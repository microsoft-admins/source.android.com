<html devsite>
  <head>
    <title>Repo Command Reference</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2019 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0 

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>Repo use takes the following form: </p>
<pre class="devsite-terminal devsite-click-to-copy">
repo <var>command options</var>
</pre>
<p>Optional elements are shown in brackets [ ]. For example, many commands take
<var>project-list</var> as an argument. You can specify <var>project-list</var> as a list of names
or a list of paths to local source directories for the projects:</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">repo sync [<var>project0 project1 ... projectn</var>]</code>
<code class="devsite-terminal">repo sync [<var>/path/to/project0 ... /path/to/projectn</var>]</code>
</pre>

<h2 id="help">help</h2>
<p>When Repo is installed, you can find the latest documentation starting with a summary of all
  commands by running:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo help
</pre>
<p>You can get information about any command by running this within a Repo tree:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo help <var>command</var>
</pre>

<p>For example, the following command yields a description and list of options
for the <code>init</code> argument of Repo, which initializes Repo in the
current directory. (See <a href="#init">init</a> for details.)</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo help init
</pre>


<h2 id="init">init</h2>
<pre class="devsite-terminal devsite-click-to-copy">repo init -u <var>url</var> [<var>options</var>]
</pre>
<p>Installs Repo in the current directory. This creates a <code>.repo/</code>
directory with Git repositories for the Repo source code and the
standard Android manifest files. The <code>.repo/</code> directory also
contains <code>manifest.xml</code>, which is a symlink to the selected manifest
in the <code>.repo/manifests/</code> directory. See <a
     href="https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md"
     class="external">manifest-format.md</a> for instructions on updating the
   manifest.</p>
<p>Options:</p>
<ul>
<li><code>-u</code>: Specify a URL from which to retrieve a manifest repository. The common manifest
  is found at <code>https://android.googlesource.com/platform/manifest</code>.</li>
<li><code>-m</code>: Select a manifest file within the repository. If no manifest name is selected,
  the default is <code>default.xml</code>. </li>
<li><code>-b</code>: Specify a revision, that is, a particular <var>manifest-branch</var>.</li>
</ul>
<p class="note"><strong>Note:</strong> For all remaining Repo commands, the current working directory
  must either be the parent directory of <code>.repo/</code> or a subdirectory of the parent directory.</p>

<h2 id="sync">sync</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo sync [<var>project-list</var>]
</pre>
<p>Downloads new changes and updates the working files in your local environment. If you run
  <code>repo sync</code> without arguments, it synchronizes the files for all projects.</p>
<p>When you run <code>repo sync</code>, this is what happens:</p>
<ul>
<li>
<p>If the project has never been synchronized, then <code>repo sync</code> is equivalent to
  <code>git clone</code>. All branches in the remote repository are copied to the local project directory.</p>
</li>
<li>
<p>If the project has been synchronized before, then <code>repo sync</code> is equivalent to:</p>
<pre><code>git remote update
git rebase origin/<var>branch</var>
</code></pre>
<p>where <code>branch</code> is the currently checked-out branch in the local project directory. If
  the local branch isn't tracking a branch in the remote repository, then no synchronization occurs
  for the project.</p>
</li>
<li>
<p>If the Git rebase operation results in merge conflicts, use the normal Git commands (for example,
  <code>git rebase --continue</code>) to resolve the conflicts.</p>
</li>
</ul>
<p>After a successful run of <code>repo sync</code>, the code in specified projects is up to date
  and synced with the code in the remote repository.</p>
<p>Options:</p>
<ul>
<li>
<p><code>-d</code>: Switch specified projects back to the manifest revision. This is helpful if the
  project is currently on a topic branch, but the manifest revision is temporarily needed.</p>
</li>
<li>
<p><code>-s</code>: Sync to a known good build as specified by the <var>manifest-server</var> element
  in the current manifest.</p>
</li>
<li>
<p><code>-f</code>: Proceed with syncing other projects even if a project fails to sync.</p>
</li>
</ul>

<h2 id="upload">upload</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo upload [<var>project-list</var>]
</pre>
<p>For the specified projects, Repo compares the local branches to the remote branches updated
  during the last Repo sync. Repo prompts you to select one or more of the branches that haven't
  been uploaded for review.</p>
<p>All commits on the selected branches
are then transmitted to Gerrit over an HTTPS connection. You need to
configure an HTTPS password to enable upload authorization. Visit the
<a href="https://android-review.googlesource.com/new-password" class="external">Password Generator</a>
to generate a new username/password pair to use over HTTPS.</p>
<p>When Gerrit receives the object data over its server, it turns each
commit into a change so that reviewers can comment on a specific commit.
To combine several <em>checkpoint</em> commits into a
single commit, use <code>git rebase -i</code> before you run the upload.</p>
<p>If you run <code>repo upload</code> without arguments, it searches all of the projects for
  changes to upload.</p>
<p>To edit changes after they've been uploaded, use a tool like <code>git rebase -i</code> or
  <code>git commit --amend</code> to update your local commits. After your edits are complete:</p>
<ul>
<li>Verify that the updated branch is the currently checked out branch.</li>
<li>For each commit in the series, enter the Gerrit change ID inside the brackets:<br />
<pre><code># Replacing from branch foo
[ 3021 ] 35f2596c Refactor part of GetUploadableBranches to lookup one specific...
[ 2829 ] ec18b4ba Update proto client to support patch set replacments
# Insert change numbers in the brackets to add a new patch set.
# To create a new change record, leave the brackets empty.
</code></pre>
</li>
</ul>
<p>After the upload is complete, the changes have an additional patch set.</p>
<p>If you want to upload only the currently checked out Git branch, use the flag
  <code>--current-branch</code> (or <code>--cbr</code> for short).</p>

<h2 id="diff">diff</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo diff [<var>project-list</var>]
</pre>
<p>Shows outstanding changes between the commit and the working tree using <code>git diff</code>.</p>

<h2 id="download">download</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo download <var>target change</var>
</pre>
<p>Downloads the specified change from the review system and makes it available in your project's
  local working directory.</p>
<p>For example, to download
<a href="https://android-review.googlesource.com/23823" class="external" >change 23823</a>
  into your platform/build directory:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo download platform/build 23823
</pre>
<p>Running <code>repo sync</code> removes any commits retrieved with
  <code>repo download</code>. Or you can check out the remote branch using
  <code>git checkout m/master</code>.</p>
<p class="note"><strong>Note:</strong> There are replication
delays to all servers worldwide, so there's a slight mirroring lag between when a change
  is visible on
the web in <a href="https://android-review.googlesource.com/">Gerrit</a> and when
<code>repo download</code> can find the change for all users.</p>

<h2 id="forall">forall</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo forall [<var>project-list</var>] -c <var>command</var>
</pre>
<p>Executes the given shell command in each project. The following additional environment variables
  are made available by <code>repo forall</code>:</p>
<ul>
<li>
<p><code>REPO_PROJECT</code> is set to the unique name of the project.</p>
</li>
<li>
<p><code>REPO_PATH</code> is the path relative to the root of the client.</p>
</li>
<li>
<p><code>REPO_REMOTE</code> is the name of the remote system from the manifest.</p>
</li>
<li>
<p><code>REPO_LREV</code> is the name of the revision from the manifest, translated to a local
  tracking branch. Use this if you need to pass the manifest revision to a locally executed Git
  command.</p>
</li>
<li>
<p><code>REPO_RREV</code> is the name of the revision from the manifest, exactly as written in the
  manifest.</p>
</li>
</ul>
<p>Options:</p>
<ul>
<li>
<p><code>-c</code>: Command and arguments to execute. The command is evaluated through
  <code>/bin/sh</code> and any arguments after it are passed through as shell positional parameters.</p>
</li>
<li>
<p><code>-p</code>: Show project headers before output of the specified command. This is achieved
  by binding pipes to the command's stdin, stdout, and sterr streams, and piping all output into a
  continuous stream that is displayed in a single pager session.</p>
</li>
<li>
<p><code>-v</code>: Show messages the command writes to stderr.</p>
</li>
</ul>

<h2 id="prune">prune</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo prune [<var>project-list</var>]
</pre>
<p>Prunes (deletes) topics that are already merged.</p>

<h2 id="start">start</h2>
<pre class="devsite-terminal devsite-click-to-copy">repo start
<var>branch-name</var> [<var>project-list</var>]</pre>
<p>Begins a new branch for development, starting from the revision specified in the manifest.</p>
<p>The <code>BRANCH_NAME</code> argument provides a short description of the change you're trying
  to make to the projects. If you don't know, consider using the name <code>default</code>.</p>
<p>The <code>project-list</code> argument specifies which projects participate in this topic
  branch. </p>
<p class="note"><strong>Note:</strong> A period (&nbsp;.&nbsp;) is shorthand for the project in
  the current working directory.</p>

<h2 id="status">status</h2>
<pre class="devsite-terminal devsite-click-to-copy">
repo status [<var>project-list</var>]
</pre>
<p>Compares the working tree to the staging area (index) and the most recent commit on this branch
  (HEAD) in each project specified. Displays a summary line for each file where there is a
  difference between these three states.</p>
<p>To see the status of just the current branch, run <code>repo status</code>. The status
  information is listed by project. For each file in the project, a two-letter code is used.</p>
<p>In the first column, an uppercase letter indicates how the staging area differs from the last
  committed state.</p>
<table>
<thead>
<tr>
<th>Letter</th>
<th>Meaning</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>-</td>
<td>No change</td>
<td>Same in HEAD and index</td>
</tr>
<tr>
<td>A</td>
<td>Added</td>
<td>Not in HEAD, in index</td>
</tr>
<tr>
<td>M</td>
<td>Modified</td>
<td>In HEAD, modified in index</td>
</tr>
<tr>
<td>D</td>
<td>Deleted</td>
<td>In HEAD, not in index</td>
</tr>
<tr>
<td>R</td>
<td>Renamed</td>
<td>Not in HEAD, path changed in index</td>
</tr>
<tr>
<td>C</td>
<td>Copied</td>
<td>Not in HEAD, copied from another in index</td>
</tr>
<tr>
<td>T</td>
<td>Mode changed</td>
<td>Same content in HEAD and index, mode changed</td>
</tr>
<tr>
<td>U</td>
<td>Unmerged</td>
<td>Conflict between HEAD and index; resolution required</td>
</tr>
</tbody>
</table>
<p>In the second column, a lowercase letter indicates how the working directory differs from
  the index.</p>
<table>
<thead>
<tr>
<th>Letter</th>
<th>Meaning</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>-</td>
<td>New/unknown</td>
<td>Not in index, in work tree</td>
</tr>
<tr>
<td>m</td>
<td>Modified</td>
<td>In index, in work tree, modified</td>
</tr>
<tr>
<td>d</td>
<td>Deleted</td>
<td>In index, not in work tree</td>
</tr>
</tbody>
</table>

  </body>
</html>
