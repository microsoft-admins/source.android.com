<html devsite>
  <head>
    <title>Frequently Asked Questions</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->



<a name="top"></a>
<p>
  This page provides answers to some frequently asked questions (FAQs).
</p>

<h2 id="open-source">Open Source</h2>

<h3 id="what-is-the-android-open-source-project">What is the Android Open Source
Project?</h3>
<p>Android Open Source Project (AOSP) refers to the
people, processes, and source code that make up Android.</p>
<p>The people oversee the project and develop the source code. The
processes are the tools and procedures that we use to manage the development
of the software. The net result is the source code, which you can use in
mobile phones and other devices.</p>

<h3 id="why-did-we-open-the-android-source-code">Why did we open the Android
source code?</h3>
<p>Google started the Android project in response to our own experiences
launching mobile apps. We wanted to make sure there would always be an
open platform available for carriers, OEMs, and developers to use to make
their innovative ideas a reality. We also wanted to avoid any
central point of failure, so no single industry player could restrict or control
the innovations of any other. Our single most important goal with
AOSP is to make sure that open source Android
software is implemented as widely and compatibly as possible, to everyone's
benefit.</p>

<h3 id="what-kind-of-open-source-project-is-android">What kind of open source
project is Android?</h3>
<p>Google oversees the development of the core Android open source platform
and works to create robust developer and user communities. For the most part,
the Android source code is licensed under the permissive Apache
License 2.0, rather than a <em>copyleft</em> license. We chose the Apache 2.0
license because we believe that it encourages widespread
Android software adoption. For details, see
<a href="licenses.html">Licenses</a>.</p>

<h3 id="why-is-google-in-charge-of-android">Why is Google in charge of
Android?</h3>
<p>Launching a software platform is complex. Openness is vital to the
long-term success of a platform, because openness attracts
investment from developers and ensures a level playing field.
The platform must also be a compelling product to users.</p>
<p>Google has committed the professional engineering resources
necessary to ensure that Android is a fully competitive software platform.
Google treats the Android project as a full-scale product development
operation and strikes the business deals necessary to make sure great
devices running Android make it to market.</p>
<p>By making sure Android is a success with users, we help ensure the
vitality of Android as a platform and as an open source project. After all,
who wants the source code to an unsuccessful product?</p>
<p>Google's goal is to ensure a successful ecosystem around Android.
We opened the Android source code so that anyone
can modify and distribute the software to meet their own needs.</p>

<h3 id="what-is-googles-overall-strategy-for-android-product-development">What
is Google's overall strategy for Android product development?</h3>
<p>We release great devices into a competitive marketplace. We
then incorporate the innovations and enhancements we made into the core
platform as the next version.</p>
<p>In practice, this means that the Android engineering team focuses
on a small number of "flagship" devices and develops the next version of
Android software to support those product launches. These flagship
devices absorb much of the product risk and blaze a trail for the broad OEM
community, who follow up with more devices that take advantage of the
new features. In this way, we make sure that the Android platform evolves
according to the needs of real-world devices.</p>

<h3 id="how-is-the-android-software-developed">How is Android software
developed?</h3>
<p>Each platform version of Android (such as 1.5 or 8.1) has a
corresponding branch in the open source tree. The most recent
branch is considered the <em>current stable</em> branch version.
This is the branch that manufacturers port to their
devices. This branch is kept suitable for release at all times.</p>
<p>Simultaneously, there's a <em>current experimental</em> branch, which is
where speculative contributions, such as large next-generation features, are
developed. Bug fixes and other contributions can be included in the current
stable branch from the experimental branch as appropriate.</p>
<p>Finally, Google works on the next version of the Android platform in tandem
with developing a flagship device. This branch pulls in changes from the
experimental and stable branches as appropriate.</p>
<p>For details, see <a href="codelines.html">Codelines, Branches, and
Releases</a>.</p>

<h3 id="why-are-parts-of-android-developed-in-private">Why are parts of Android
developed in private?</h3>
<p>It typically takes more than a year to bring a device to market. And, of
course, device manufacturers want to ship the latest software they can.
Meanwhile, developers don't want to constantly track new versions of the
platform when writing apps. Both groups experience a tension between
shipping products and not wanting to fall behind.</p>
<p>To address this, some parts of the next version of Android including the
core platform APIs are developed in a private branch. These APIs constitute
the next version of Android. Our aim is to focus attention on the current
stable version of the Android source code while we create the next version
of the platform. This allows developers
and OEMs to use a single version without tracking unfinished
future work just to keep up. Other parts of the Android system that aren't
related to application compatibility are developed in the open.
It's our intention to move more of these parts to open development over
time.</p>

<h3 id="when-are-source-code-releases-made">When are source code releases
made?</h3>
<p>When thet're ready. Releasing the source code is a fairly complex process.
Some parts of Android are developed in the open,
and that source code is always available. Other parts are developed first in
a private tree, and that source code is released when the next platform
version is ready.</p>
<p>In some releases, core platform APIs are ready far enough in advance so
that we can push the source code out for an early look prior to the
device's release. In other releases, this isn't possible. In all cases,
we release the platform source when we feel that the version is stable,
and when the development process permits.</p>

<h3 id="what-is-involved-in-releasing-the-source-code-for-a-new-android-version">What's
involved in releasing the source code for a new Android version?</h3>
<p>Releasing the source code for a new version of the Android platform is a
significant process. First, the software is built into a system image for
a device and put through various forms of certification, including
government regulatory certification for the regions the phones will be
deployed. The code also goes through operator testing. This is an important
phase of the process, because it helps detect software bugs.</p>
<p>When the release is approved by the regulators and operators, the
manufacturer begins mass producing devices, and we begin releasing the
source code.</p>
<p>Simultaneous to mass production, the Google team kicks off several efforts
to prepare the open source release. These efforts include making final API
changes, updating documentation (to reflect any modifications that were made
during qualification testing, for example), preparing an SDK for the new
version, and launching the platform compatibility information.</p>
<p>Our legal team does a final sign-off to release the code into open
source. Just as open source contributors are required to sign a Contributors
License Agreement attesting to their intellectual property ownership of their
contribution, Google must verify that the source is cleared to make contributions.</p>
<p>From the time that mass production begins, the software release process
usually takes aabout a month, so source code releases often happen at
around the same time the devices reach users.</p>

<h3 id="how-does-the-aosp-relate-to-the-android-compatibility-program">How does
AOSP relate to the Android Compatibility Program?</h3>
<p>The Android Open Source Project maintains Android software, and
develops new versions. Because it's open source, this software can be used for
any purpose, including developing devices that aren't compatible with other
devices based on the same source.</p>
<p>The function of the Android Compatibility Program is to define a baseline
implementation of Android that is compatible with third-party apps written
by developers. Devices that are <em>Android compatible</em> may participate in
the Android ecosystem, including Google Play; devices that don't meet the
compatibility requirements exist outside of that ecosystem.</p>
<p>In other words, the Android Compatibility Program is how we separate
Android-compatible devices from devices that merely run derivatives of the
source code. We welcome all uses of the Android source code, but to participate
in the Android ecosystem, a device must be identified as Android-compatible
by the program.</p>

<h3 id="how-can-i-contribute-to-android">How can I contribute to Android?</h3>
<p>You can report bugs, write apps for Android, or contribute source code
to the Android Open Source Project.</p>
<p>There are limits to the kinds of code contributions we
accept. For instance, someone might want to contribute an
alternative application API, such as a full C++-based environment. We would
decline that contribution, because Android encourages applications to be run
in the ART runtime. Similarly, we won't accept contributions such as GPL
or LGPL libraries that are incompatible with our licensing goals.</p>
<p>We encourage those interested in contributing source code to contact us
via the channels listed on the <a href="../community.html">
Android Community</a> page prior to beginning any work. For details, see
<a href="../contribute/index.html">Contributing</a>.</p>

<h3 id="how-do-i-become-an-android-committer">How do I become an Android
committer?</h3>
<p>The Android Open Source Project doesn't really have a notion of a
<em>committer</em>. All contributions (including those authored by Google
employees) go through a web-based system known as Gerrit that's part of
the Android engineering process. This system works in tandem with the git
source code management system to cleanly manage source code
contributions.</p>
<p>When submitted, changes need to be accepted by a designated approver.
Approvers are typically Google employees, but the same approvers are
responsible for all submissions, regardless of origin.</p>
<p>For details, see <a href="../contribute/submit-patches.html">Submitting
Patches</a>.</p>

<a href="#top">Back to top</a>

<h2 id="compatibility">Compatibility</h2>

<h3 id="what-does-compatibility-mean">What is Android "compatibility"?</h3>
<p>We define an <em>Android-compatible device</em> as one that can run any
application written by third-party developers using the Android SDK and NDK.
We use this as a filter to separate devices that can participate in the
Android app ecosystem and those that can't. For devices that are properly
compatible, device manufacturers can seek approval to use the Android
trademark. Devices that aren't compatible are merely derived from the
Android source code and may not use the Android trademark.</p>
<p>In other words, compatibility is a prerequisite to participate in the
Android apps ecosystem. Anyone is welcome to use the Android source code.
But if the device isn't compatible, it isn't considered part of the Android
ecosystem.</p>

<h3 id="what-is-the-role-of-google-play-in-compatibility">What's the role of
Google Play in compatibility?</h3>
<p>Device manufacturers with Android-compatible devices may seek to license
the Google Play client software. Licensed devices become part of the
Android app ecosystem, enabling their users to download developers' apps from
a catalog shared by all compatible devices. Licensing isn't available to
incompatible devices.</p>

<h3 id="what-kinds-of-devices-can-be-android-compatible">What kinds of devices
can be Android compatible?</h3>
<p>Android software can be ported to many different devices,
including some on which third-party apps won't run properly. The
<a href="/compatibility/cdd.html">Android Compatibility Definition
Document</a> (CDD) spells out the specific device configurations that are
considered compatible.</p>
<p>For example, though the Android source code could be ported to run on a
phone that doesn't have a camera, the CDD requires all phones to have a camera.
This allows developers to rely on a consistent set of capabilities when writing
their apps.</p>
<p>The CDD continues to evolve to reflect market realities. For instance,
version 1.6 of the CDD supports only cell phones. But version 2.1 allows devices
to omit telephony hardware, enabling non-phone devices such as tablet-style
music players to be compatible. As we make these changes, we'll also
augment Google Play to allow developers to retain control over where
their apps are available. To continue the telephony example, an app that
manages SMS text messages isn't useful on a media player, so Google
Play allows the developer to restrict that app exclusively to phone devices.</p>

<h3 id="if-my-device-is-compatible-does-it-automatically-have-access-to-google-play-and-branding">If
my device is compatible, does it automatically have access to Google Play and
branding?</h3>
<p>No. Access isn't automatic. Google Play is a service operated by Google.
Achieving compatibility is a prerequisite for obtaining access to the
Google Play software and branding. After a device is
<a href="/compatibility/overview#android-compatibility-is-free-and-its-easy">qualified
as an Android-compatible device</a>, the device manufacturer should complete the
contact form included in <a
href="/compatibility/contact-us#for-business-inquiries">licensing Google Mobile
Services</a> to seek access to Google Play. We'll be in contact if we can
help you.</p>

<h3 id="if-i-am-not-a-manufacturer-how-can-i-get-google-play">If I'm not a
manufacturer, how can I get Google Play?</h3>
<p>Google Play is only licensed to handset manufacturers shipping devices.
For questions about specific cases, contact <a
href="mailto:android-partnerships@google.com">android-partnerships@google.com</a>.
</p>

<h3 id="how-can-i-get-access-to-the-google-apps-for-android-such-as-maps">How
can I get access to Google apps for Android, such as Maps?</h3>
<p>Google apps for Android, such as YouTube, Google Maps, and
Gmail are Google properties that aren't part of Android and
are licensed separately. Contact <a
href="mailto:android-partnerships@google.com">android-partnerships@google.com</a>
for inquiries related to these apps.</p>

<h3 id="is-compatibility-mandatory">Is compatibility mandatory?</h3>
<p>No. The Android Compatibility Program is optional. The Android source
code is open, so anyone can use it to build any kind of device. However, if
manufacturers wish to use the Android name with their products, or want access
to Google Play, they must first
<a href="/compatibility/overview#android-compatibility-is-free-and-its-easy">demonstrate
that their devices are compatible</a>.</p>

<h3 id="how-much-does-compatibility-certification-cost">How much does
compatibility certification cost?</h3>
<p>There's no cost to obtain Android compatibility for a device. The
Compatibility Test Suite is open source and available to anyone for device
testing.</p>

<h3 id="how-long-does-compatibility-take">How long does compatibility take?</h3>
<p>The process is automated. The Compatibility Test Suite generates a report
that can be provided to Google to verify compatibility. Eventually we intend
to provide self-service tools to upload these reports to a public database.</p>

<h3 id="who-determines-what-will-be-part-of-the-compatibility-definition">Who
determines the compatibility definition?</h3>
<p>Google is responsible for the overall direction of Android as a
platform and product, so Google maintains the Compatibility Definition Document
(CDD) for each release. We draft the CDD for a new Android version in consultation
with various OEMs who provide input.</p>

<h3 id="how-long-will-each-android-version-be-supported-for-new-devices">How
long will each Android version be supported for new devices?</h3>
<p>Android's code is open source, so we can't prevent someone from using an
old version to launch a device. Instead, Google chooses not to license the
Google Play client software for use on versions that are considered
obsolete. This allows anyone to continue to ship old versions of Android,
but those devices won't use the Android name and exist outside of the
Android apps ecosystem, just as if they weren't compatible.</p>

<h3 id="can-a-device-have-a-different-user-interface-and-still-be-compatible">Can
a device have a different user interface and still be compatible?</h3>
<p>The Android Compatibility Program determines whether a device can run
third-party applications. The user interface components shipped with a
device (such as home screen, dialer, and color scheme) don't
generally have much effect on third-party apps. As such, device builders are
free to customize the user interface. The Compatibility
Definition Document restricts the degree to which OEMs may alter the
system user interface for areas that impact third-party apps.</p>

<h3 id="when-are-compatibility-definitions-released-for-new-android-versions">When
are compatibility definitions released for new Android versions?</h3>
<p>Our goal is to release a new version of the Android Compatibility Definition
Document (CDD) when the corresponding Android platform version has
converged enough to permit it. While we can't release a final draft of a CDD
for an Android software version before the first flagship device ships with
that software, final CDDs are always released after the first device.
However, wherever practical we release draft versions of CDDs.</p>

<h3 id="how-are-device-manufacturers-compatibility-claims-validated">How are
device manufacturers' compatibility claims validated?</h3>
<p>There is no validation process for Android device compatibility. However,
if the device is to include Google Play, Google typically validates
the device for compatibility before agreeing to license the Google Play client
software.</p>

<h3 id="what-happens-if-a-device-that-claims-compatibility-is-later-found-to-have-compatibility-problems">What
happens if a device that claims compatibility is later found to have
compatibility problems?</h3>
<p>Typically, Google's relationships with Google Play licensees allow us to ask
the deveice manufacturer to release updated system images that fix the problems.</p>

<a href="#top">Back to top</a>

<h2 id="compatibility-test-suite">Compatibility Test Suite</h2>

<h3 id="what-is-the-purpose-of-the-cts">What's the purpose of the CTS?</h3>
<p>The Compatibility Test Suite is a tool used by device manufacturers to help
ensure that their devices are compatible, and to report test results for
validations. The CTS is intended to be run frequently by OEMs throughout the
engineering process to catch compatibility issues early.</p>

<h3 id="what-kinds-of-things-does-the-cts-test">What kinds of things does the
CTS test?</h3>
<p>The CTS currently tests that all of the supported Android strong-typed APIs
are present and behave correctly. It also tests other non-API system
behaviors such as application lifecycle and performance. We plan to add
support in future CTS versions to test <em>soft</em> APIs such as Intents.</p>

<h3 id="will-the-cts-reports-be-made-public">Will the CTS reports be made
public?</h3>
<p>Yes. While not currently implemented, Google intends to provide web-based
self-service tools for OEMs to publish CTS reports so that anyone can
view them. Manufacturers can share CTS reports with as wide an audience
as they like.</p>

<h3 id="how-is-the-cts-licensed">How is the CTS licensed?</h3>
<p>The CTS is licensed under the same Apache Software License 2.0 that the
bulk of Android uses.</p>

<h3 id="does-the-cts-accept-contributions">Does the CTS accept
contributions?</h3>
<p>Yes, please! The Android Open Source Project accepts contributions to
improve the CTS just as for any other component. In fact,
improving the coverage and quality of the CTS test cases is one of the best
ways to help Android.</p>

<h3 id="can-anyone-use-the-cts-on-existing-devices">Can anyone use the CTS on
existing devices?</h3>
<p>The Compatibility Definition Document requires that compatible devices
implement the <code>adb</code> debugging utility. This means that any compatible
device (including those available at retail) must be able to run the CTS
tests.</p>

<h3 id="are-codecs-verified">Are codecs verified by CTS?</h3>
<p>Yes. All mandatory codecs are verified by CTS.</p>

<a href="#top">Back to top</a>

  </body>
</html>
