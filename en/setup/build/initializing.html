<html devsite>
  <head>
    <title>Establishing a Build Environment</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  This section describes how to set up your local work environment to build
  the Android source files. You must use Linux or Mac OS; building under
  Windows is not currently supported.
</p>
<p>
  For an overview of the entire code-review and code-update process, see <a
  href="../contribute/life-of-a-patch.html">Life of a Patch</a>.
</p>
<aside class="note"><strong>Note:</strong> All commands in this site are
  preceded by a dollar sign ($) to differentiate them from output or entries
  within files. You may use the <em>Click to copy</em> feature at the top right
  of each command box to copy all lines without the dollar signs or triple-click
  each line to copy it individually without the dollar sign.
</aside>

<h2 id="choosing-a-branch">Choosing a branch</h2>
<p>
  Some requirements for the build environment are determined by the version of
  the source code you plan to compile. For a full list of available branches,
  see <a href="../start/build-numbers.html">Build Numbers</a>. You can also
  choose to download and build the latest source code (called
  <code>master</code>), in which case you will simply omit
  the branch specification when you initialize the repository.
</p>
<p>
  After you have selected a branch, follow the appropriate instructions below to
  set up your build environment.
</p>

<h2 id="setting-up-a-linux-build-environment">Setting up a Linux build
environment</h2>
<p>
  These instructions apply to all branches, including <code>master</code>.
</p>
<p>The Android build is routinely tested in house on recent versions of
  Ubuntu LTS (14.04) and Debian testing. Most other distributions should
  have the required build tools available.
</p>
<p>
  For Gingerbread (2.3.x) and newer versions, including the <code>master</code>
  branch, a 64-bit environment is required. Older versions can be
  compiled on 32-bit systems.
</p>
<aside class="note"><strong>Note:</strong> See
  <a href="requirements.html">Requirements</a> for the complete list of hardware
  and software requirements, then follow the detailed instructions for Ubuntu
  and Mac OS below.
</aside>

<h3 id="installing-required-packages-ubuntu-1404">Installing required packages
(Ubuntu 14.04)</h3>
<p>
  You will need a 64-bit version of Ubuntu. Ubuntu 14.04 is recommended.
</p>
<pre class="devsite-terminal devsite-click-to-copy">
sudo apt-get install git-core gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev libgl1-mesa-dev libxml2-utils xsltproc unzip
</pre>
<aside class="note"><strong>Note:</strong> To use SELinux tools for policy
  analysis, also install the <code>python-networkx</code> package.
</aside>
<aside class="note"><strong>Note:</strong> If you are using LDAP and want
  to run ART host tests, also install the <code>libnss-sss:i386</code>
  package.
</aside>

<h3 id="installing-required-packages-ubuntu-1204">Installing required packages
  (Ubuntu 12.04)
</h3>
<p>
  You may use Ubuntu 12.04 to build older versions of Android. Version 12.04
  is not supported on master or recent releases.
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">sudo apt-get install git gnupg flex bison gperf build-essential zip curl libc6-dev libncurses5-dev:i386 x11proto-core-dev libx11-dev:i386 libreadline6-dev:i386 libgl1-mesa-glx:i386 libgl1-mesa-dev g++-multilib mingw32 tofrodos python-markdown libxml2-utils xsltproc zlib1g-dev:i386</code>
<code class="devsite-terminal">sudo ln -s /usr/lib/i386-linux-gnu/mesa/libGL.so.1 /usr/lib/i386-linux-gnu/libGL.so</code>
</pre>

<h3 id="installing-required-packages-ubuntu-1004-1110">Installing required
  packages (Ubuntu 10.04 -- 11.10)
</h3>
<p>
  Building on Ubuntu 10.04-11.10 is no longer supported, but may be useful for
  building older releases of AOSP.
</p>
<pre class="devsite-terminal devsite-click-to-copy">
sudo apt-get install git gnupg flex bison gperf build-essential zip curl zlib1g-dev libc6-dev lib32ncurses5-dev ia32-libs x11proto-core-dev libx11-dev lib32readline5-dev lib32z-dev libgl1-mesa-dev g++-multilib mingw32 tofrodos python-markdown libxml2-utils xsltproc
</pre>
<p>
  On Ubuntu 10.10:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
sudo ln -s /usr/lib32/mesa/libGL.so.1 /usr/lib32/mesa/libGL.so
</pre>
<p>
  On Ubuntu 11.10:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
sudo apt-get install libx11-dev:i386
</pre>

<h3 id="configuring-usb-access">Configuring USB access</h3>
<p>
  Install a community-maintained default set of <code>udev</code> rules for
  all Android devices by following the instructions to <a
    href="https://developer.android.com/studio/run/device.html#setting-up"
    class="external">Set up a device for development</a>.
</p>

<h3 id="using-a-separate-output-directory">Using a separate output
directory</h3>
<p>
  By default, the output of each build is stored in the <code>out/</code>
  subdirectory of the matching source tree.
</p>
<p>
  On some machines with multiple storage devices, builds are
  faster when storing the source files and the output on
  separate volumes. For additional performance, the output
  can be stored on a filesystem optimized for speed instead
  of crash robustness, since all files can be re-generated
  in case of filesystem corruption.
</p>
<p>
  To set this up, export the <code>OUT_DIR_COMMON_BASE</code> variable
  to point to the location where your output directories
  will be stored.
</p>
<pre class="devsite-terminal devsite-click-to-copy">
export OUT_DIR_COMMON_BASE=&lt;path-to-your-out-directory&gt;
</pre>
<p>
  The output directory for each separate source tree will be named after the
  directory holding the source tree. For instance, if you have source trees as
  <code>/source/master1</code> and <code>/source/master2</code> and
  <code>OUT_DIR_COMMON_BASE</code> is set to <code>/output</code>, the output
  directories will be <code>/output/master1</code> and
  <code>/output/master2</code>.
</p>
<p>
  It's important in that case to not have multiple source
  trees stored in directories that have the same name,
  as those would end up sharing an output directory, with
  unpredictable results. This is only supported on Jelly Bean (4.1) and newer,
  including the <code>master</code> branch.
</p>
<h2 id="setting-up-a-mac-os-x-build-environment">Setting up a Mac OS build
  environment
</h2>
<p>
  In a default installation, Mac OS runs on a case-preserving but
  case-insensitive filesystem. This type of filesystem is not supported by git
  and will cause some git commands (such as <code>git status</code>) to behave
  abnormally. Because of this, we recommend that you always work with the AOSP
  source files on a case-sensitive filesystem. This can be done fairly easily
  using a disk image, discussed below.
</p>
<p>
  Once the proper filesystem is available, building the <code>master</code>
  branch in a modern Mac OS environment is very straightforward. Earlier
  branches require some additional tools and SDKs.
</p>

<h3 id="creating-a-case-sensitive-disk-image">Creating a case-sensitive disk
image</h3>
<p>
  You can create a case-sensitive filesystem within your existing Mac OS
  environment using a disk image. To create the image, launch Disk
  Utility and select <strong>New Image</strong>. A size of 25GB is the minimum to
  complete the build; larger numbers are more future-proof. Using sparse images
  saves space while allowing to grow later as the need arises. Be sure to select
  <strong>case sensitive, journaled</strong> as the volume format.
</p>
<p>
  You can also create it from a shell with the following command:
</p>
<pre class="devsite-click-to-copy devsite-terminal" data-terminal-prefix="# ">
hdiutil create -type SPARSE -fs 'Case-sensitive Journaled HFS+' -size 40g ~/android.dmg
</pre>
<p>
  This will create a <code>.dmg</code> (or possibly a
  <code>.dmg.sparseimage</code>) file which, once mounted, acts as a drive with
  the required formatting for Android development.
</p>
<p>
  If you need a larger volume later, you can also resize the sparse image with
  the following command:
</p>
<pre class="devsite-click-to-copy devsite-terminal" data-terminal-prefix="# ">hdiutil resize -size &lt;new-size-you-want&gt;g ~/android.dmg.sparseimage
</pre>
<p>
  For a disk image named <code>android.dmg</code> stored in your home
  directory, you can add helper functions to your <code>~/.bash_profile</code>:
</p>
<ul>
  <li>To mount the image when you execute <code>mountAndroid</code>:
    <pre class="devsite-click-to-copy">
    # mount the android file image
    mountAndroid() { hdiutil attach ~/android.dmg -mountpoint /Volumes/android; }</pre>
    <aside class="note"><strong>Note:</strong> If your system created a
      <code>.dmg.sparseimage</code> file, replace <code>~/android.dmg</code> with
      <code>~/android.dmg.sparseimage</code>.
    </aside>
  </li>
  <li>To unmount it when you execute <code>umountAndroid</code>:
    <pre class="devsite-click-to-copy">
     # unmount the android file image
     umountAndroid() { hdiutil detach /Volumes/android; }</pre>
  </li>
</ul>
<p>
  After you've mounted the <code>android</code> volume, you'll do all your work
  there. You can eject it (unmount it) just as you would an external drive.
</p>

<h3 id="xcode-other-packages">Installing Xcode and other packages</h3>
<ol>
  <li>Install Xcode command line tools with:
    <pre class="devsite-terminal devsite-click-to-copy">
    xcode-select --install</pre>
    For older versions of Mac OS (10.8 or earlier), you must install Xcode from
    the <a href="http://developer.apple.com/" class="external">Apple developer
    site</a>. If you are not already registered as an Apple developer, you must
    create an Apple ID to download.
  </li>
  <li>Install either <a href="http://www.macports.org/install.php">MacPorts</a> or <a href="https://brew.sh/">Homebrew</a> for package management.
  </li>
  <li>Ensure the associated directory is in your path within your <code>~/.bash_profile</code> file:
    <ol>
      <li>MacPorts - <code>/opt/local/bin</code> must appear <strong>before</strong>
        <code>/usr/bin</code>:
        <pre class="devsite-click-to-copy">
        export PATH=/opt/local/bin:$PATH</pre>
      </li>
      <li>Homebrew - <code>/usr/local/bin</code>:
        <pre class="devsite-click-to-copy">
        export PATH=/usr/local/bin:$PATH</pre>
      </li>
  </li>
  <li>For MacPorts, issue:
    <pre class="devsite-terminal devsite-click-to-copy">
    POSIXLY_CORRECT=1 sudo port install git gnupg</pre>
  </li>
  <li>For Homebrew, issue:
    <pre class="devsite-terminal devsite-click-to-copy">
    brew install git gnupg2</pre>
  </li>
</ol>

<h3 id="setting-a-file-descriptor-limit">Setting a file descriptor limit</h3>
<p>
  On Mac OS, the default limit on the number of simultaneous file descriptors
  open is too low and a highly parallel build process may exceed this limit. To
  increase the cap, add the following lines to your
  <code>~/.bash_profile</code>:
</p>
<pre class="devsite-click-to-copy">
# set the number of open files to be 1024
ulimit -S -n 1024</pre>

<h2 id="next-download-the-source">Next: Download the source</h2>
<p>
  Your build environment is good to go! Proceed to
  <a href="downloading.html">downloading the source</a>.
</p>

  </body>
</html>
