<html devsite>
  <head>
    <title>Supporting Older Versions</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  These requirements apply to older versions of Android. For the Android Open Source Project (AOSP)
  <code>master</code> branch, see the standard <a href="requirements">Requirements</a> and
  <a href="initializing">Establishing a Build Environment</a>. For Android
  versions 8.0 (Oreo or O) through 5.0 (Lollipop or L), consider using the included
  <a href="https://android.googlesource.com/platform/build/+/master/tools/docker">Dockerfile</a>
  to ease installation of all required packages.
</p>

<h2 id=operating-systems>Operating systems</h2>
<p>
  Android is typically built with a GNU/Linux or Mac OS operating system. It is
  also possible to build Android in a virtual machine on unsupported systems
  such as Windows.
</p>

<p>We recommend building on GNU/Linux instead of another operating system. The
  Android build system normally uses ART, running on the build machine, to
  pre-compile system dex files. Since ART is able to run only on Linux, the
  build system skips this pre-compilation step on non-Linux operating systems,
  resulting in an Android build with reduced performance.
</p>

<h3 id=linux>GNU/Linux</h3>
<ul>
  <li>Android 6.0 (Marshmallow) - AOSP master: Ubuntu 14.04 (Trusty)</li>
  <li>Android 2.3.x (Gingerbread) - Android 5.x (Lollipop): Ubuntu 12.04
  (Precise)</li>
  <li>Android 1.5 (Cupcake) - Android 2.2.x (Froyo): Ubuntu 10.04 (Lucid)</li>
</ul>
<h3 id=mac>Mac OS (Intel/x86)</h3>
<ul>
  <li>Android 6.0 (Marshmallow) - AOSP master: Mac OS v10.10 (Yosemite) or
    higher with Xcode 4.5.2 and Command Line Tools
  </li>
  <li>Android 5.x (Lollipop): Mac OS v10.8 (Mountain Lion) with Xcode 4.5.2
    and Command Line Tools
  </li>
  <li>Android 4.1.x-4.3.x (Jelly Bean) - Android 4.4.x (KitKat): Mac OS v10.6
    (Snow Leopard) or Mac OS X v10.7 (Lion) and Xcode 4.2 (Apple's Developer
    Tools)
  </li>
  <li>Android 1.5 (Cupcake) - Android 4.0.x (Ice Cream Sandwich): Mac OS
    v10.5 (Leopard) or Mac OS X v10.6 (Snow Leopard) and the Mac OS X v10.5
    SDK
  </li>
</ul>

<h2 id=make>Make</h2>
<p>
  To avoid build errors, Android 4.0.x (Ice Cream Sandwich) and earlier must
  <a href="initializing.html#reverting-from-make-382">revert from make 3.82</a>.
</p>

<h3 id="xcode-other-packages">Xcode and other packages</h3>
<p>
    For older versions of Mac OS (10.8 or earlier), you must install Xcode from
    the <a href="http://developer.apple.com/" class="external">Apple developer
    site</a>. If you are not already registered as an Apple developer, you must
    create an Apple ID to download.
</p>
<p>
    If using Mac OS X v10.4, also install bison:
    <pre class="devsite-terminal devsite-click-to-copy">
    POSIXLY_CORRECT=1 sudo port install bison</pre>
</p>

<p>
    For MacPorts, issue:
    <pre class="devsite-terminal devsite-click-to-copy">
    POSIXLY_CORRECT=1 sudo port install gmake libsdl git gnupg</pre>
</p>

<p>
    For Homebrew, issue:
    <pre class="devsite-terminal devsite-click-to-copy">
    brew install gmake libsdl git gnupg2</pre>
</p>

<h3 id="reverting-from-make-382">Reverting from make 3.82</h3>
<p>
  In Android 4.0.x (Ice Cream Sandwich) and earlier, a bug exists in gmake 3.82
  that prevents Android from building. You can install version 3.81 using
  MacPorts with these steps:
</p>
<ol>
  <li>Edit <code>/opt/local/etc/macports/sources.conf</code> and add a line that
    says:
    <pre class="devsite-click-to-copy">
    file:///Users/Shared/dports</pre>
    above the rsync line. Then create this directory:
    <pre class="devsite-terminal devsite-click-to-copy">
    mkdir /Users/Shared/dports</pre>
  </li>
  <li>In the new <code>dports</code> directory, run:
    <pre class="devsite-terminal devsite-click-to-copy">
    svn co --revision 50980 http://svn.macports.org/repository/macports/trunk/dports/devel/gmake/ devel/gmake/</pre>
  </li>
  <li>Create a port index for your new local repository:
    <pre class="devsite-terminal devsite-click-to-copy">
    portindex /Users/Shared/dports</pre>
  </li>
  <li>Install the old version of gmake with:
    <pre class="devsite-terminal devsite-click-to-copy">
    sudo port install gmake @3.81</pre>
  </li>
</ol>

<h2 id=jdk>JDK</h2>

<ul>
  <li>Android 7.0 (Nougat) - Android 8.0 (Oreo): Ubuntu -
    <a href="http://openjdk.java.net/install/" class="external">OpenJDK 8</a>,
    Mac OS -
    <a href="http://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html#jdk-8u45-oth-JPR" class="external">jdk
    8u45 or newer</a>
  </li>
  <li>Android 5.x (Lollipop) - Android 6.0 (Marshmallow): Ubuntu -
    <a href="http://openjdk.java.net/install/" class="external">OpenJDK 7</a>,
    Mac OS -
    <a href="https://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html#jdk-7u71-oth-JPR" class="external">jdk-7u71-macosx-x64.dmg</a>
  </li>
  <li>Android 2.3.x (Gingerbread) - Android 4.4.x (KitKat): Ubuntu -
    <a href="http://www.oracle.com/technetwork/java/javase/archive-139210.html" class="external">Java
    JDK 6</a>, Mac OS - <a href="http://support.apple.com/kb/dl1572" class="external">Java JDK
    6</a>
  </li>
  <li>Android 1.5 (Cupcake) - Android 2.2.x (Froyo): Ubuntu -
    <a href="http://www.oracle.com/technetwork/java/javase/archive-139210.html" class="external">Java
    JDK 5</a>
  </li>
</ul>

  <h3 id=jdk-linux>JDK for Linux</h3>

<p>
  The <code>master</code> branch of Android in the
  <a href="https://android.googlesource.com/" class="external">Android Open
  Source Project (AOSP)</a> comes with prebuilt versions of OpenJDK below
  <code>prebuilts/jdk/</code> so no additional installation is required.
</p>
<p>
  Older versions of Android require a separate installation of the JDK. On
  Ubuntu, use
  <a href="http://openjdk.java.net/install/" class="external">OpenJDK</a>. See
  <a href="requirements.html#jdk">JDK Requirements</a> for precise versions and
  the sections below for instructions.
</p>
<h4 id="for-ubuntu-15-04">For Ubuntu &gt;= 15.04</h4>
<p>
  Run the following:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">sudo apt-get update</code>
<code class="devsite-terminal">sudo apt-get install openjdk-8-jdk</code>
</pre>

<h4 id="for-ubuntu-14-04">For Ubuntu LTS 14.04</h4>
<p>
  There are no available supported OpenJDK 8 packages for Ubuntu 14.04. The
  <strong>Ubuntu 15.04 OpenJDK 8</strong> packages have been used successfully
  with Ubuntu 14.04. <em>Newer package versions (e.g. those for 15.10, 16.04) were
  found not to work on 14.04 using the instructions below.</em>
</p>
<ol>
  <li>Download the <code>.deb</code> packages for 64-bit architecture from
    <a href="http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/" class="external">old-releases.ubuntu.com</a>:
    <ul>
      <li><a
        href="http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre-headless_8u45-b14-1_amd64.deb" class="external">openjdk-8-jre-headless_8u45-b14-1_amd64.deb</a>
        with SHA256 <code>0f5aba8db39088283b51e00054813063173a4d8809f70033976f83e214ab56c0</code>
      </li>
      <li><a
        href="http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre_8u45-b14-1_amd64.deb" class="external">openjdk-8-jre_8u45-b14-1_amd64.deb</a>
        with SHA256 <code>9ef76c4562d39432b69baf6c18f199707c5c56a5b4566847df908b7d74e15849</code>
      </li>
      <li><a
        href="http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jdk_8u45-b14-1_amd64.deb" class="external">openjdk-8-jdk_8u45-b14-1_amd64.deb</a>
        with SHA256 <code>6e47215cf6205aa829e6a0a64985075bd29d1f428a4006a80c9db371c2fc3c4c</code>
      </li>
    </ul>
  </li>
  <li>Optionally, confirm the checksums of the downloaded files against the
    SHA256 string listed with each package above. For example, with the
    <code>sha256sum</code> tool:
    <pre class="devsite-terminal devsite-click-to-copy">
    sha256sum {downloaded.deb file}</pre>
  </li>
  <li>Install the packages:
    <pre class="devsite-terminal devsite-click-to-copy">
    sudo apt-get update</pre>
    Run <code>dpkg</code> for each of the .deb files you downloaded. It may
    produce errors due to missing dependencies:
    <pre class="devsite-terminal devsite-click-to-copy">
    sudo dpkg -i {downloaded.deb file}</pre>
    To fix missing dependencies:
    <pre class="devsite-terminal devsite-click-to-copy">
    sudo apt-get -f install</pre>
  </li>
</ol>

<h4 id="default-java-version">Update the default Java version - optional</h4>
<p>
  Optionally, for the Ubuntu versions above update the default Java version by
  running:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">sudo update-alternatives --config java</code>
<code class="devsite-terminal">sudo update-alternatives --config javac</code>
</pre>
<aside class="note">
  <b>Note:</b> If, during a build, you encounter version errors for Java, see
  <a href="building.html#wrong-java-version">Wrong Java version</a> for likely
  causes and solutions.
 </aside>

  </body>
</html>
