<html devsite>
  <head>
    <title>Pixel Update Bulletin—March 2019</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2019 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p><em>Published March 4, 2019</em></p>

<p>
The Pixel Update Bulletin contains details of security
vulnerabilities and functional improvements affecting <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
class="external">supported Google Pixel devices</a> (Google devices).
For Google devices, security patch levels of 2019-03-05 or later address all
issues in this bulletin and all issues in the March 2019 Android Security
Bulletin. To learn how to check a device's security patch level, see <a
href="https://support.google.com/pixelphone/answer/4457705"
class="external">Check & update your Android version</a>.
</p>
<p>
All supported Google devices will receive an update to the 2019-03-05 patch
level. We encourage all customers to accept these updates to their devices.
</p>
<p class="note">
<strong>Note:</strong> The Google device firmware images are available on the
<a href="https://developers.google.com/android/images" class="external">Google
Developer site</a>.
</p>

<h2 id="announcements">Announcements</h2>

<p>There are no Pixel security patches in the March 2019 Pixel Update Bulletin.
</p>

<h3 id="functional-patches">Functional patches</h3>

<p>The functional patches are included for affected Pixel
  devices to address functionality issues not related to
  the security of Pixel devices. The patches listed in
  the table below include associated references, the
  affected category, and the affected devices.
</p>

<table>
  <tr>
    <th><strong>References</strong></th>
    <th><strong>Category</strong></th>
    <th><strong>Improvements</strong></th>
    <th><strong>Devices</strong></th>
  </tr>
  <tr>
    <td>A-122471935</td>
    <td>Camera</td>
    <td>Improves Startup and responsiveness of Camera app</td>
    <td>Pixel 3, Pixel 3 XL</td>
  </tr>
  <tr>
    <td>A-112046316<br>
    A-118317435<br>
    A-120212581</td>
    <td>Performance</td>
    <td >Improves recovery in the instance of OTA update failure</td>
    <td>Pixel 3, Pixel 3XL</td>
  </tr>
  <tr>
    <td>A-120920537</td>
    <td>Performance</td>
    <td>Improves storage performance on Pixel 3 devices</td>
    <td>Pixel 3, Pixel3 XL</td>
  </tr>
  <tr>
    <td>A-119776006</td>
    <td>Bluetooth</td>
    <td>Improves bluetooth reliability on Pixel 3 devices</td>
    <td>Pixel 3, Pixel3 XL</td>
  </tr>
  <tr>
    <td>A-111260263<br>
    A-116848259</td>
    <td>Media</td>
    <td>Improves playback of encrypted media on some video apps</td>
    <td>Pixel 3, Pixel3 XL</td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>
This section answers common questions that may occur after reading this
bulletin.
</p>
<p>
<strong>1. How do I determine if my device is updated to address these issues?
</strong>
</p>
<p>
Security patch levels of 2019-03-05 or later address all issues associated with
the 2019-03-05 security patch level and all previous patch levels. To learn how
to check a device's security patch level, read the instructions on the <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
class="external">Pixel update schedule</a>.
</p>
<p id="type">
<strong>2. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>3. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>4. What does an * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have an * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Pixel devices available
from the <a href="https://developers.google.com/android/drivers"
class="external">Google Developer site</a>.
</p>
<p>
<strong>5. Why are security vulnerabilities split between this bulletin and the
Android Security Bulletins?</strong>
</p>
<p>
Security vulnerabilities that are documented in the Android Security Bulletins
are required to declare the latest security patch level on Android
devices. Additional security vulnerabilities, such as those documented in this
bulletin are not required for declaring a security patch level.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>March 4, 2019</td>
   <td>Bulletin published.</td>
  </tr>
</table>
</body>
</html>
