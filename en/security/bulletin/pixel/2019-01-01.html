<html devsite>
  <head>
    <title>Pixel Update Bulletin—January 2019</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2019 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p><em>Published January 7, 2019</em></p>

<p>
The Pixel Update Bulletin contains details of security
vulnerabilities and functional improvements affecting <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
class="external">supported Google Pixel devices</a> (Google devices).
For Google devices, security patch levels of 2019-01-05 or later address all
issues in this bulletin and all issues in the January 2019 Android Security
Bulletin. To learn how to check a device's security patch level, see <a
href="https://support.google.com/pixelphone/answer/4457705"
class="external">Check & update your Android version</a>.
</p>
<p>
All supported Google devices will receive an update to the 2019-01-05 patch
level. We encourage all customers to accept these updates to their devices.
</p>
<p class="note">
<strong>Note:</strong> The Google device firmware images are available on the
<a href="https://developers.google.com/android/images" class="external">Google
Developer site</a>.
</p>

<h2 id="announcements">Announcements</h2>

<p>In addition to the security vulnerabilities described in the
<a href="/security/bulletin/2019-01-01">January 2019 Android Security
Bulletin</a>, Google devices also contain patches for the security
vulnerabilities described below. Partners were notified of these issues at
least a month ago and may choose to incorporate them as part of their device
updates.
</p>

<h2 id="security-patches">Security patches</h2>
<p>
Vulnerabilities are grouped under the component they affect. There is a
description of the issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources#severity">severity</a>,
and updated Android Open Source Project (AOSP) versions (where applicable).
When available, we link the public change that addressed the issue to the bug
ID, such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-13098</td>
    <td>A-113148387<br />
        <a
href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=76d56d4ab4f2a9e4f085c7d77172194ddaccf7d2">
Upstream kernel</a></td>
    <td>ID</td>
    <td>Moderate</td>
    <td>Filesystem</td>
  </tr>
  <tr>
    <td>CVE-2018-13099</td>
    <td>A-113148515<br />
        <a
href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=4dbe38dc386910c668c75ae616b99b823b59f3eb">
Upstream kernel</a></td>
    <td>ID</td>
    <td>Moderate</td>
    <td>Filesystem</td>
  </tr>
</table>


<h3 id="functional-patches">Functional patches</h3>
<p>These updates are included for affected Pixel devices to address
functionality issues not related to the security of Pixel devices. The table
includes associated references, the affected category (such as, Bluetooth and
mobile data), improvements, and affected devices.
</p>

<table>
  <tr>
    <col width="15%">
    <col width="15%">
    <col width="40%">
    <col width="30%">
   <th>References</th>
   <th>Category</th>
   <th>Improvements</th>
   <th>Devices</th>
  </tr>
  <tr>
   <td>A-113776612, A-118022272</td>
   <td>Audio</td>
   <td>Improved audio quality when recording videos</td>
   <td>Pixel 3, Pixel 3 XL</td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>
This section answers common questions that may occur after reading this
bulletin.
</p>
<p>
<strong>1. How do I determine if my device is updated to address these issues?
</strong>
</p>
<p>
Security patch levels of 2019-01-05 or later address all issues associated with
the 2019-01-05 security patch level and all previous patch levels. To learn how
to check a device's security patch level, read the instructions on the <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
class="external">Pixel update schedule</a>.
</p>
<p id="type">
<strong>2. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>3. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>4. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Pixel devices available
from the <a href="https://developers.google.com/android/drivers"
class="external">Google Developer site</a>.
</p>
<p>
<strong>5. Why are security vulnerabilities split between this bulletin and the
Android Security Bulletins?</strong>
</p>
<p>
Security vulnerabilities that are documented in the Android Security Bulletins
are required to declare the latest security patch level on Android
devices. Additional security vulnerabilities, such as those documented in this
bulletin are not required for declaring a security patch level.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>January 7, 2019</td>
   <td>Bulletin published.</td>
  </tr>
</table>
</body>
</html>
