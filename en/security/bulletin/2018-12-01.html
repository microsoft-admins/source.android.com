<html devsite>
  <head>
    <title>Android Security Bulletin—December 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published December 3, 2018 | Updated December 5, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-12-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-12-01">December 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>

<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a>. These capabilities reduce the likelihood that security
vulnerabilities could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2018-12-01-details">2018-12-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-12-01 patch level. Vulnerabilities are
grouped under the component they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, such as the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>

<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9547</td>
    <td><a
      href="https://android.googlesource.com/platform/frameworks/native/+/e6eb42cb2e57747e52e488d54da314bc6eabb546"
      class="external">A-114223584</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9548</td>
    <td><a
      href="https://android.googlesource.com/platform/frameworks/base/+/c97efaa05124e020d7cc8c6e08be9c3b55ac4ea7" class="external">A-112555574</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>

<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9549</td>
    <td><a
      href="https://android.googlesource.com/platform/external/aac/+/6f6d220a3255e7cbd31bcd1220ffb83af0a2779a"
      class="external">A-112160868</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9550</td>
    <td><a
      href="https://android.googlesource.com/platform/external/aac/+/ce97e7d55e1f69683b5bc8f19cc8da8c85bc2cd4"
      class="external">A-112660981</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9551</td>
    <td><a
      href="https://android.googlesource.com/platform/external/aac/+/0e5db9fee912d367a572b88f0d86f9a33006fa29"
      class="external">A-112891548</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9552</td>
    <td><a
      href="https://android.googlesource.com/platform/external/libhevc/+/d15da6f960dd2d5b77faced4e799f8bf53785d9c"
      class="external">A-113260892</a></td>
    <td>ID</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9553</td>
    <td><a
      href="https://android.googlesource.com/platform/external/libvpx/+/c4c92b2c6ed72a78ea430c3cdce564ec11866a24"
      class="external">A-116615297</a></td>
    <td>RCE</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9538</td>
    <td><a
      href="https://android.googlesource.com/platform/external/v4l2_codec2/+/0a7d252adb774338c2c69a17651aceca3aec1b23"
      class="external">A-112181526</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9554</td>
    <td><a
      href="https://android.googlesource.com/platform/frameworks/av/+/16f9b39c69626093ae9225b458739707c9a3b4e7"
      class="external">A-114770654</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>

<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted transmission to execute arbitrary code within the
context of a privileged process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9555</td>
    <td><a
      href="https://android.googlesource.com/platform/system/bt/+/02fc52878d8dba16b860fbdf415b6e4425922b2c"
      class="external">A-112321180</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9556</td>
    <td><a
      href="https://android.googlesource.com/platform/system/update_engine/+/840a7eae5a6d8250490e8ea430193531f0c4ccd6"
      class="external">A-113118184</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9557</td>
    <td>A-35385357<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
    <td>CVE-2018-9558</td>
    <td><a
      href="https://android.googlesource.com/platform/system/nfc/+/ce7fcb95d5111ad8c554e7ec8ff02f9b40196cdc"
      class="external">A-112161557</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9559</td>
    <td><a
      href="https://android.googlesource.com/platform/system/vold/+/c2e37da22aadcdb4a5b7f61a61f824ab8e9b8af9"
      class="external">A-112731440</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9560</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/9009da96e00434501d9398bbfbc636902c757632"
      class="external">A-79946737</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9562</td>
    <td><a
      href="https://android.googlesource.com/platform/system/bt/+/1bb14c41a72978c6075c5753a8301ddcbb10d409"
      class="external">A-113164621</a></td>
    <td>ID</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9566</td>
    <td><a
      href="https://android.googlesource.com/platform/system/bt/+/314336a22d781f54ed7394645a50f74d6743267d"
      class="external">A-74249842</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>

<h2 id="2018-12-05-details">2018-12-05 security patch level vulnerability details</h2>

<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-12-05 patch level. Vulnerabilities are
grouped under the component they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="system-05">System</h3>

<p>The most severe vulnerability in this section could lead to remote
information disclosure with no additional execution privileges needed.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-9565</td>
    <td>A-16680558<a href="#asterisk">*</a></td>
    <td>ID</td>
    <td>High</td>
    <td>OMA-DM</td>
  </tr>
</table>

<h3 id="htc-components">HTC components</h3>

<p>The most severe vulnerability in this section could enable a local attacker
to bypass user interaction requirements in order to gain access to additional
permissions.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-9567</td>
    <td>A-65543936<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
</table>

<h3 id="kernel-components">Kernel components</h3>

<p>The most severe vulnerability in this section could enable a local attacker
to execute arbitrary code within the context of a privileged process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-10840</td>
    <td>A-116406508<br />
<a href="https://bugzilla.redhat.com/show_bug.cgi?id=CVE-2018-10840">Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>ext4 filesystem</td>
  </tr>
  <tr>
    <td>CVE-2018-9568</td>
    <td>A-113509306<br />
<a href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/net/core/sock.c?id=9d538fa60bad4f7b23193c89e843797a1cf71ef3">Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>network</td>
  </tr>
</table>

<h3 id="qualcomm-components">Qualcomm components</h3>

<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm security bulletin or security alert.
The severity assessment of these issues is provided directly by Qualcomm.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-11960</td>
    <td>A-114042002<br />
<a href="https://source.codeaurora.org/quic/la/kernel/msm-3.18/commit/?id=18ce15db603e19cfac9a2f4076f255e879100495">QC-CR#2264832</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>HWEngines</td>
  </tr>
  <tr>
    <td>CVE-2018-11961</td>
    <td>A-114040881<br />
<a href="https://source.codeaurora.org/quic/le/platform/hardware/qcom/gps/commit/?id=c57ee0a5d3261ab20c35b451d1b3ae2b02a21591">QC-CR#2261813</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>GPS_AP_LINUX</td>
  </tr>
  <tr>
    <td>CVE-2018-11963</td>
    <td>A-114041685<br />
<a href="https://source.codeaurora.org/quic/la/kernel/msm-4.4/commit/?id=c9ac3476a91c384a3f2760fabaecef0ad8698d7b">QC-CR#2220770</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Automotive Multimedia</td>
  </tr>
</table>

<h3 id="qualcomm-closed-source-components">Qualcomm closed-source components</h3>

<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm security bulletin or security
alert. The severity assessment of these issues is provided directly by
Qualcomm.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-8248</td>
    <td>A-78135902<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-11004</td>
    <td>A-66913713<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18141</td>
    <td>A-67712316<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5913</td>
    <td>A-79419833<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11279</td>
    <td>A-109678200<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18319</td>
    <td>A-78284753<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18321</td>
    <td>A-78283451<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18322</td>
    <td>A-78285196<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18323</td>
    <td>A-78284194<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18324</td>
    <td>A-78284517<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18327</td>
    <td>A-78240177<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18331</td>
    <td>A-78239686<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18332</td>
    <td>A-78284545<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18160</td>
    <td>A-109660689<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18326</td>
    <td>A-78240324<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-8276</td>
    <td>A-68141338<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18328</td>
    <td>A-78286046<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18329</td>
    <td>A-73539037<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18330</td>
    <td>A-73539235<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-3595</td>
    <td>A-71501115<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18320</td>
    <td>A-33757308<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11999</td>
    <td>A-74236942<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5867</td>
    <td>A-77485184<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5868</td>
    <td>A-77484529<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5869</td>
    <td>A-33385206<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-5754</td>
    <td>A-79419639<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5915</td>
    <td>A-79420511<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11267</td>
    <td>A-109678338<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11922</td>
    <td>A-112279564<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>

<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-12-01 or later address all issues associated
with the 2018-12-01 security patch level.</li>
<li>Security patch levels of 2018-12-05 or later address all issues associated
with the 2018-12-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-12-01]</li>
 <li>[ro.build.version.security_patch]:[2018-12-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-12-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-12-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb"
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>

<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
    <td>1.0</td>
    <td>December 3, 2018</td>
    <td>Bulletin published</td>
  </tr>
  <tr>
    <td>1.1</td>
    <td>December 5, 2018</td>
    <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body></html>
