<html devsite>
  <head>
    <title>Android Security Bulletin — February 2019</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published February 4, 2019</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2019-02-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical security vulnerability in
Framework that could allow a remote attacker using a specially crafted
PNG file to execute arbitrary code within the context of a privileged process.
The <a href="/security/overview/updates-resources.html#severity">
severity assessment</a> is based on the effect that exploiting the
vulnerability would possibly have on an affected device, assuming
the platform and service mitigations are turned off for development
purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2019-02-01">February 2019
Pixel Update Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>

<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a>. These capabilities reduce the likelihood that security
vulnerabilities could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2019-02-01-details">2019-02-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2019-02-01 patch level. Vulnerabilities are
grouped under the component they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, such as the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted PNG file to execute arbitrary code within the context
of a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2019-1986</td>
    <td><a
        href="https://android.googlesource.com/platform/external/skia/+/15c377e6230b060d54c43247a0a261ff7c73553b"
        class="external">A-117838472</a> [<a
        href="https://android.googlesource.com/platform/external/skia/+/8157d73fb620bc463eb26e974fe92d3259e7545a"
        >2</a>]</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2019-1987</td>
    <td><a
        href="https://android.googlesource.com/platform/external/skia/+/15c377e6230b060d54c43247a0a261ff7c73553b"
        class="external">A-118143775</a> [<a
        href="https://android.googlesource.com/platform/external/skia/+/654579dbab7cf3a566636bcc7d6617d5ac938f38"
        >2</a>]</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1988</td>
    <td><a
        href="https://android.googlesource.com/platform/external/skia/+/0056606ac49688dcf3c08a51ca98fd94d9bf1897"
        class="external">A-118372692</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>8.0, 8.1, 9</td>
  </tr>
</table>

<h3 id="library">Library</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
an unprivileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-17760</td>
    <td>A-78029030<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-5268</td>
    <td>A-78029634<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-5269</td>
    <td>A-78029727<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2017-18009</td>
    <td>A-78026242<a href="#asterisk">*</a></td>
    <td>ID</td>
    <td>Moderate</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>

<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted transmission to execute arbitrary code within the
context of a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2019-1991</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/2d21e75aa8c1e0c4adf178a1330f9f5c573ca045"
        class="external">A-110166268</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1992</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/c365ae6444b86c3ddd19197fd2c787581ebb31df"
        class="external">A-116222069</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1993</td>
    <td><a
        href="https://android.googlesource.com/platform/frameworks/base/+/b5dee1bafd5648fd9210b4cba5e23b0665f9add5"
        class="external">A-119819889</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1994</td>
    <td><a
        href="https://android.googlesource.com/platform/packages/apps/Settings/+/bd363f4925206b6256bb60d70b998f0d54efe7cc"
        class="external">A-117770924</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1995</td>
    <td><a
        href="https://android.googlesource.com/platform/packages/apps/Email/+/b541ef378df154f30cd1d18408354eff6004b9ef"
        class="external">A-32589229</a> [<a
        href="https://android.googlesource.com/platform/packages/apps/UnifiedEmail/+/5a0b253c8797bf1c0b8dca73128e60cd1f823a6d"
        >2</a>]</td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1996</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/525bdbd6e1295ed8a081d2ae87105c64d6f1ac4f"
        class="external">A-111451066</a></td>
    <td>ID</td>
    <td>High</td>
    <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1997</td>
    <td><a
        href="https://android.googlesource.com/platform/external/wpa_supplicant_8/+/d87989c5020e346322f2f9037e2ef58f41e4969a"
        class="external">A-117508900</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2019-1998</td>
    <td><a
        href="https://android.googlesource.com/platform/external/nos/host/android/+/877a6e05bb08353a4f806ad4d3c9cf6f1c3fb097"
        class="external">A-116055338</a> [<a
        href="https://android.googlesource.com/platform/system/security/+/f8feed620bd607427ded702cce91bb0eb749bc6a"
        >2</a>]</td>
    <td>DoS</td>
    <td>High</td>
    <td>9</td>
  </tr>
</table>


<h2 id="2019-02-05-details">2019-02-05 security patch level vulnerability details</h2>

<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2019-02-05 patch level. Vulnerabilities are
grouped under the component they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-10879</td>
    <td>A-116406063<br />
        <a
href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=513f86d73855ce556ea9522b6bfd79f87356dc3a"
class="external">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>ext4 filesystem</td>
  </tr>
  <tr>
    <td>CVE-2019-1999</td>
    <td>A-120025196<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Binder driver</td>
  </tr>
  <tr>
    <td>CVE-2019-2000</td>
    <td>A-120025789<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Binder driver</td>
  </tr>
  <tr>
    <td>CVE-2019-2001</td>
    <td>A-117422211<a href="#asterisk">*</a></td>
    <td>ID</td>
    <td>High</td>
    <td>iomem</td>
  </tr>
</table>


<h3 id="nvidia-components">NVIDIA components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-6271</td>
    <td>A-80198474<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>libnvomx</td>
  </tr>
  <tr>
    <td>CVE-2018-6267</td>
    <td>A-70857947<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>libnvomx</td>
  </tr>
  <tr>
    <td>CVE-2018-6268</td>
    <td>A-80433161<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>libnvomx</td>
  </tr>
  <tr>
    <td>CVE-2016-6684</td>
    <td>A-117423758<a href="#asterisk">*</a></td>
    <td>ID</td>
    <td>High</td>
    <td>kernel log</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm security bulletin or security alert.
The severity assessment of these issues is provided directly by Qualcomm.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-11262</td>
    <td>A-76424945<br />
        <a
href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=29ab5eb75bc9ed01466ab1a98e932e59fe27ad42">
QC-CR#2221192</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-11280</td>
    <td>A-109741776<br />
        <a
href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=bd3627dae5f1a34e0284cfe167f61273ecc2f386">
QC-CR#2185061</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Modem</td>
  </tr>
  <tr>
    <td>CVE-2018-11275</td>
    <td>A-74409078<br />
        <a
href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=648fdd3ddcc01161abbf7a21fcd11eda13cc5226">
QC-CR#2221256</a> [<a
href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=bf0261ab128f28763258c620bc95ca379a286b59">2</a>]</td>
    <td>N/A</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-13900</td>
    <td>A-119052051<br />
        <a
href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=c8ca0610474488ddff578a8338818fe69e7d4a14">
QC-CR#2287499</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Modem</td>
  </tr>
  <tr>
    <td>CVE-2018-13905</td>
    <td>A-119052050<br />
        <a
href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=d87585164cef055f8b220f77c58cf7159e176e29">
QC-CR#2225202</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Graphics</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm security bulletin or security alert.
The severity assessment of these issues is provided directly by Qualcomm.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-11289</td>
    <td>A-109678453<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11820</td>
    <td>A-111089815<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11938</td>
    <td>A-112279482<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11945</td>
    <td>A-112278875<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11268</td>
    <td>A-109678259<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11845</td>
    <td>A-111088838<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11864</td>
    <td>A-111092944<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11921</td>
    <td>A-112278972<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11931</td>
    <td>A-112279521<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11932</td>
    <td>A-112279426<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11935</td>
    <td>A-112279483<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11948</td>
    <td>A-112279144<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5839</td>
    <td>A-112279544<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-13904</td>
    <td>A-119050566<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>


<h2 id="common-questions-and-answers">Common questions and answers</h2>

<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2019-02-01 or later address all issues associated
with the 2019-02-01 security patch level.</li>
<li>Security patch levels of 2019-02-05 or later address all issues associated
with the 2019-02-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2019-02-01]</li>
 <li>[ro.build.version.security_patch]:[2019-02-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2019-02-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2019-02-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb"
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>

<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
    <td>1.0</td>
    <td>February 4, 2019</td>
    <td>Bulletin published</td>
  </tr>
</table>
</body>
</html>
