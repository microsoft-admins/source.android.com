<html devsite>
  <head>
    <title>Android Security Bulletin—September 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published September 4, 2018 | Updated September 5, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-09-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file
to execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-09-01">September 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a>. These capabilities reduce the likelihood that security
vulnerabilities could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2018-09-01-details">2018-09-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-09-01 patch level. Vulnerabilities are
grouped under the component they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, such as the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>


<h3 id="android-runtime">Android Runtime</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
an application that uses the library.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2018-9466</td>
   <td><a
href="https://android.googlesource.com/platform/external/libxml2/+/b730f8d3c15da4ac439f1184bf17a13021963ea9"
class="external">A-62151041</a></td>
   <td>RCE</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2018-9467</td>
   <td><a
href="https://android.googlesource.com/platform/libcore/+/518e8d27de9f32eb86bc3090ee2759ea93b9fb93"
class="external">A-110955991</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>


<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
an unprivileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2018-9469</td>
   <td><a
href="https://android.googlesource.com/platform/frameworks/base/+/623b2b604c4ffcd48f137379d6934537510665bf"
class="external">A-109824443</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9470</td>
   <td><a
href="https://android.googlesource.com/platform/external/neven/+/86a561f79f97baa38e240f6296fe1192fa4a5c9c"
class="external">A-78290481</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9471</td>
   <td><a
href="https://android.googlesource.com/platform/frameworks/base/+/eabaff1c7f02906e568997bdd7dc43006655387e"
class="external">A-77599679</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>

<h3 id="library">Library</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
an application that uses the library.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9472</td>
   <td><a
href="https://android.googlesource.com/platform/external/libxml2/+/b730f8d3c15da4ac439f1184bf17a13021963ea9"
class="external">A-79662501</a></td>
    <td>RCE</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="media-framework">Media Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements to gain access to
additional permissions.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2018-9474</td>
   <td><a
href="https://android.googlesource.com/platform/frameworks/base/+/586b9102f322731d604e6280143e16cb6f1c9f76"
class="external">A-77600398</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9440</td>
   <td><a
href="https://android.googlesource.com/platform/frameworks/av/+/8033f4a227e03f97a0f1d9975dc24bcb4ca61f74"
class="external">A-77823362</a>
[<a
href="https://android.googlesource.com/platform/frameworks/av/+/2870acaa4c58cf59758a74b6390615a421f14268"
class="external">2</a>]</td>
   <td>DoS</td>
   <td>Moderate</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a local attacker
to bypass user interaction requirements to gain access to additional
permissions.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
 <tr>
   <td>CVE-2018-9475</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/43cd528a444d0cc5bbf3beb22cd583289bcf7334"
class="external">A-79266386</a></td>
   <td>EoP</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9478</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/68688194eade113ad31687a730e8d4102ada58d5"
class="external">A-79217522</a></td>
   <td>EoP</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9479</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/68688194eade113ad31687a730e8d4102ada58d5"
class="external">A-79217770</a></td>
   <td>EoP</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9456</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/04be7ae5771ee1edc6cbe2af26998755d7be5a68"
class="external">A-78136869</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2018-9477</td>
   <td><a
href="https://android.googlesource.com/platform/packages/apps/Settings/+/3eec10e4a8daf8f07127341fbc45bef539c8d790"
class="external">A-92497653</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2018-9480</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/75c22982624fb530bc1d57aba6c1e46e7881d6ba"
class="external">A-109757168</a></td>
   <td>ID</td>
   <td>High</td>
   <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9481</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/75c22982624fb530bc1d57aba6c1e46e7881d6ba"
class="external">A-109757435</a></td>
   <td>ID</td>
   <td>High</td>
   <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9482</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/75c22982624fb530bc1d57aba6c1e46e7881d6ba"
class="external">A-109757986</a></td>
   <td>ID</td>
   <td>High</td>
   <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9483</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/d3689fb0ddcdede16c13250a7a30ca76b113c9c1"
class="external">A-110216173</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9484</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/d5b44f6522c3294d6f5fd71bc6670f625f716460"
class="external">A-79488381</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9485</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/bdbabb2ca4ebb4dc5971d3d42cb12f8048e23a23"
class="external">A-80261585</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9486</td>
   <td><a
href="https://android.googlesource.com/platform/system/bt/+/bc6aef4f29387d07e0c638c9db810c6c1193f75b"
class="external">A-80493272</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9487</td>
   <td><a
href="https://android.googlesource.com/platform/frameworks/base/+/cf6784bfbf713aaa54d8da77e9481b3f02784246"
class="external">A-69873852</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9488</td>
   <td><a
href="https://android.googlesource.com/platform/system/sepolicy/+/d4e094e2b1a47c1fea1799d9fade19e953a7ca1b"
class="external">A-110107376</a></td>
   <td>EoP</td>
   <td>Moderate</td>
   <td>8.0, 8.1, 9</td>
  </tr>
</table>


<h2 id="2018-09-05-details">2018-09-05 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-09-05 patch level. Vulnerabilities are
grouped under the component they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass operating system protections that isolate application data
from other applications.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9468</td>
    <td><a href="https://android.googlesource.com/platform/packages/providers/DownloadProvider/+/544294737dfc3b585465302f1f784a311659a37c#"
           class="external">A-111084083</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
to access data normally accessible only to locally installed applications with
permissions.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-5754</td>
    <td>A-69856074<a href="#asterisk">*</a><br />
        Upstream kernel</td>
    <td>ID</td>
    <td>High</td>
    <td>Kernel Memory</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm APSS security bulletin or security
alert. Android partners can check applicability of their issues to their
devices through Createpoint. The severity assessment of these issues is
provided directly by Qualcomm.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-11816</td>
    <td>A-63527106 <br />
QC-CR#2119840<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Video</td>
  </tr>
  <tr>
    <td>CVE-2018-11261</td>
    <td>A-64340487 <br />
QC-CR#2119840<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Video</td>
  </tr>
  <tr>
    <td>CVE-2018-11836</td>
    <td>A-111128620 <br />
	<a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=9d703c0815b2b260592bc8b91d907aeef7962eb7">QC-CR#2214158</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>WLAN HOST</td>
  </tr>
  <tr>
    <td>CVE-2018-11842</td>
    <td>A-111124974 <br />
	<a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=5eea70b9d5852e468467c1565927dbe0c76d8674">QC-CR#2216741</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>WLAN HOST</td>
  </tr>
  <tr>
    <td>CVE-2018-11898</td>
    <td>A-111128799 <br />
	<a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=dc657f502adb3038784b7488d2f183ed31b6aac3">QC-CR#2233036</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>WLAN HOST</td>
  </tr>
  <tr>
    <td>CVE-2017-15825</td>
    <td>A-68992460 <br />
	<a href="https://source.codeaurora.org/quic/la/kernel/lk/commit/?id=252e22c9adb9b59c36e59e00d8b43013facec4d6">QC-CR#2096455</a></td>
    <td>N/A</td>
    <td>Moderate</td>
    <td>Boot</td>
  </tr>
  <tr>
    <td>CVE-2018-11270</td>
    <td>A-109741697 <br />
	<a href="https://source.codeaurora.org/quic/la/kernel/msm-3.10/commit/?id=d475e1aba3f8be3b135199014549ff9d5c315e1d">QC-CR#2205728</a></td>
    <td>N/A</td>
    <td>Moderate</td>
    <td>WiredConnectivity</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm AMSS security bulletin or security
alert. Android partners can check applicability of their issues to their
devices through Createpoint. The severity assessment of these issues is
provided directly by Qualcomm.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2016-10394</td>
    <td>A-68326803<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18314</td>
    <td>A-62213176<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18311</td>
    <td>A-73539234<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11950</td>
    <td>A-72950814<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5866</td>
    <td>A-77484228<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11824</td>
    <td>A-111090697<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2016-10408</td>
    <td>A-68326811<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18313</td>
    <td>A-78240387<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18312</td>
    <td>A-78239234<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18124</td>
    <td>A-68326819<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-3588</td>
    <td>A-71501117<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11951</td>
    <td>A-72950958<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11952</td>
    <td>A-74236425<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5871</td>
    <td>A-77484229<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5914</td>
    <td>A-79419793<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11288</td>
    <td>A-109677940<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11285</td>
    <td>A-109677982<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11290</td>
    <td>A-109677964<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11292</td>
    <td>A-109678202<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11287</td>
    <td>A-109678380<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11846</td>
    <td>A-111091377<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11855</td>
    <td>A-111090533<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11857</td>
    <td>A-111093202<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11858</td>
    <td>A-111090698<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11866</td>
    <td>A-111093021<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11865</td>
    <td>A-111093167<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>


<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-09-01 or later address all issues associated
with the 2018-09-01 security patch level.</li>
<li>Security patch levels of 2018-09-05 or later address all issues associated
with the 2018-09-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-09-01]</li>
 <li>[ro.build.version.security_patch]:[2018-09-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-09-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-09-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb"
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>September 4, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>September 5, 2018</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body></html>



