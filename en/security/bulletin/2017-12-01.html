<html devsite>
  <head>
    <title>Android Security Bulletin—December 2017</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p><em>Published December 4, 2017 | Updated December 6, 2017</em>
</p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2017-12-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705">Check & update
your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before publication.
Source code patches for these issues have been released to the Android Open
Source Project (AOSP) repository and linked from this bulletin. This bulletin
also includes links to patches outside of AOSP.</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The <a
href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the <a href="#mitigations">Android
and Google Play Protect mitigations</a> section for details on the
<a href="/security/enhancements/index.html">Android
security platform protections</a> and Google Play Protect, which improve the
security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2017-12-01">December 2017 Pixel&hairsp;/&hairsp;Nexus
Security Bulletin</a>.
</p>
<h2 id="mitigations">Android and Google Service
Mitigations</h2>
<p>
This is a summary of the mitigations provided by the <a
href="/security/enhancements/index.html">Android
security platform</a> and service protections such as <a
href="https://www.android.com/play-protect">Google Play Protect</a>. These
capabilities reduce the likelihood that security vulnerabilities could be
successfully exploited on Android.
</p>
<ul>
  <li>Exploitation for many issues on Android is made more difficult by
  enhancements in newer versions of the Android platform. We encourage all users
  to update to the latest version of Android where possible.</li>
  <li>The Android security team actively monitors for abuse through <a
  href="https://www.android.com/play-protect">Google Play Protect</a> and warns
  users about <a
  href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
  Harmful Applications</a>. Google Play Protect is enabled by default on devices
  with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
  especially important for users who install apps from outside of Google
  Play.</li>
</ul>
<h2 id="2017-12-01-details">2017-12-01 security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-12-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references, <a href="#type">type
of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>
<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0807</td>
    <td>A-35056974<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
    <td>CVE-2017-0870</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/minikin/+/22758c3312ada2cf9579c9c379875e3c7eb4b1f7">A-62134807</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0871</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/8e151bf8999345399208d54663f103921ae5e1c6">A-65281159</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0872</td>
    <td><a href="https://android.googlesource.com/platform/external/skia/+/7a3ba537f7456b4870a983cd9e0a09bb3d478efc">A-65290323</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0876</td>
    <td>A-64964675<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0877</td>
    <td>A-66372937<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0878</td>
    <td><a href="https://android.googlesource.com/platform/external/libhevc/+/a963ba6ac200ee4222ba4faa7137a69144ba668a">A-65186291</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13151</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/4262d8eeee23d169ab0a141f103592f7172d95bc">A-63874456</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13153</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/969f2c97f04a0570a23d4d94b6f0a0642d2224cb">A-65280854</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0837</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/f759b8c4bcce2d3b3d45551be461f04297fa2bd3">A-64340921</a>
    [<a href="https://android.googlesource.com/platform/frameworks/av/+/0957621867279da792808e43144f0c2b670d4c6c">2</a>]</td>
    <td>EoP</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0873</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/d1a1d7b88203a240488633e3a9b4cde231c3c4e3">A-63316255</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0874</td>
    <td><a href="https://android.googlesource.com/platform/external/libavc/+/252628cffba8702e36b98c193bcd2fe67d8237ee">A-63315932</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0880</td>
    <td><a href="https://android.googlesource.com/platform/external/skia/+/67f9bd2acfd17f64a33ae8ad14806a0c93b921d8">A-65646012</a>
    [<a href="https://android.googlesource.com/platform/frameworks/base/+/adb5e0ba6d532c0d52b3bf89a1dbec4e3e7a6fd6">2</a>]</td>
    <td>DoS</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
    <td>CVE-2017-13148</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/60c4d957db5e18da39ec943f15171547b53305d6">A-65717533</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a proximate
attacker to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-13160</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/68a1cf1a9de115b66bececf892588075595b263f">A-37160362</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13156</td>
    <td><a href="https://android.googlesource.com/platform/system/core/+/9dced1626219d47c75a9d37156ed7baeef8f6403">A-64211847</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13157</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/dba1bb07e04b51b1bd0a1251711781e731ce9524">A-32990341</a></td>
    <td>ID</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13158</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/dba1bb07e04b51b1bd0a1251711781e731ce9524">A-32879915</a></td>
    <td>ID</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-13159</td>
    <td><a href="https://android.googlesource.com/platform/packages/apps/Settings/+/b5e93969a5e0c3a3f07e068dbc763cdd995a0e21">A-32879772</a></td>
    <td>ID</td>
    <td>High</td>
    <td>5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>

<h2 id="2017-12-05-details">2017-12-05 security patch level—Vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-12-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-13162</td>
    <td>A-64216036<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Binder</td>
  </tr>
  <tr>
    <td>CVE-2017-0564</td>
    <td>A-34276203<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>ION</td>
  </tr>
  <tr>
    <td>CVE-2017-7533</td>
    <td>A-63689921<br />
        <a href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=49d31c2f389acfe83417083e1208422b4091cd9">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>File handling</td>
  </tr>
  <tr>
    <td>CVE-2017-13174</td>
    <td>A-63100473<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>EDL</td>
  </tr>
</table>


<h3 id="mediatek-components">MediaTek components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-13170</td>
    <td>A-36102397<a href="#asterisk">*</a><br />
        M-ALPS03359280</td>
    <td>EoP</td>
    <td>High</td>
    <td>Display driver</td>
  </tr>
  <tr>
    <td>CVE-2017-13171</td>
    <td>A-64316572<a href="#asterisk">*</a><br />
        M-ALPS03479086</td>
    <td>EoP</td>
    <td>High</td>
    <td>Performance service</td>
  </tr>
  <tr>
    <td>CVE-2017-13173</td>
    <td>A-28067350<a href="#asterisk">*</a><br />
        M-ALPS02672361</td>
    <td>EoP</td>
    <td>High</td>
    <td>System server</td>
  </tr>
</table>


<h3 id="nvidia-components">NVIDIA components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-6262</td>
    <td>A-38045794<a href="#asterisk">*</a><br />
        N-CVE-2017-6262</td>
    <td>EoP</td>
    <td>High</td>
    <td>NVIDIA driver</td>
  </tr>
  <tr>
    <td>CVE-2017-6263</td>
    <td>A-38046353<a href="#asterisk">*</a><br />
        N-CVE-2017-6263</td>
    <td>EoP</td>
    <td>High</td>
    <td>NVIDIA driver</td>
  </tr>
  <tr>
    <td>CVE-2017-6276</td>
    <td>A-63802421<a href="#asterisk">*</a><br />
        N-CVE-2017-6276</td>
    <td>EoP</td>
    <td>High</td>
    <td>Mediaserver</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="21%">
  <col width="9%">
  <col width="14%">
  <col width="37%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-11043</td>
    <td>A-64728953<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=613f91ebcd0838c2c2bec3657e36dd57fcc6a7ea">
QC-CR#2067820</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2016-3706</td>
    <td>A-34499281<br />
        <a href="https://source.codeaurora.org/quic/le/oe/recipes/commit/?h=LNX.LE.5.3&id=6cfcc1c582a565f5360f7a3977f4a8f42d5245cd">
QC-CR#1058691</a>
       [<a href="https://source.codeaurora.org/quic/le/oe/recipes/commit/?h=LNX.LE.5.3&id=0f70f82655ceac279f9f0c76d7995294189096f9">2</a>]</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>UDP RPC</td>
  </tr>
  <tr>
    <td>CVE-2016-4429</td>
    <td>A-68946906<br />
        <a href="https://source.codeaurora.org/quic/le/oe/recipes/commit/?h=LNX.LE.5.3&id=6cfcc1c582a565f5360f7a3977f4a8f42d5245cd">
QC-CR#1058691</a>
	   [<a href="https://source.codeaurora.org/quic/le/oe/recipes/commit/?h=LNX.LE.5.3&id=0f70f82655ceac279f9f0c76d7995294189096f9">2</a>]</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>UDP RPC</td>
  </tr>
  <tr>
    <td>CVE-2017-11007</td>
    <td>A-66913719<br />
        <a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=5c710156bb55b0a085da7c4142b124f3cd986d25">
QC-CR#2068824</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Fastboot</td>
  </tr>
  <tr>
    <td>CVE-2017-14904</td>
    <td>A-63662821<a href="#asterisk">*</a><br />
        QC-CR#2109325</td>
    <td>EoP</td>
    <td>High</td>
    <td>Gralloc</td>
  </tr>
  <tr>
    <td>CVE-2017-9716</td>
    <td>A-63868627<br />
        <a href="https://source.codeaurora.org/quic/la//kernel/msm-4.4/commit/?id=aab2cc06db7cb6c7589bef71e65b5acfa58adc33">
QC-CR#2006695</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Qbt1000 driver</td>
  </tr>
  <tr>
    <td>CVE-2017-14897</td>
    <td>A-65468973<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-3.18/commit/?id=11e7de77bd5ab0a7706a013598f845ad0c4a8b4c">
QC-CR#2054091</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>RPMB driver</td>
  </tr>
  <tr>
    <td>CVE-2017-14902</td>
    <td>A-65468970<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-3.18/commit/?id=8c4901802968b8c8356860ee689b1ef9cd2cbfe4">
QC-CR#2061287</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>MProc</td>
  </tr>
  <tr>
    <td>CVE-2017-14895</td>
    <td>A-65468977<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=b15f0ff7351eb6b6a8f6694b4cd5ad27145bd439">
QC-CR#2009308</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm AMSS security bulletin or security
alert. The severity assessment of these issues is provided directly by
Qualcomm.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-6211 </td>
    <td>A-36217326<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14908</td>
    <td>A-62212840<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14909</td>
    <td>A-62212839<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14914</td>
    <td>A-62212297<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14916</td>
    <td>A-62212841<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14917</td>
    <td>A-62212740<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-14918</td>
    <td>A-65946406<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-11005</td>
    <td>A-66913715<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-11006</td>
    <td>A-66913717<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>


<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>
This section answers common questions that may occur after reading this
bulletin.
</p>
<p>
<strong>1. How do I determine if my device is updated to address these issues?
</strong>
</p>
<p>
To learn how to check a device's security patch level, see <a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
& update your Android version</a>.
</p>
<ul>
  <li>Security patch levels of 2017-12-01 or later address all issues associated
  with the 2017-12-01 security patch level.</li>
  <li>Security patch levels of 2017-12-05 or later address all issues associated
  with the 2017-12-05 security patch level and all previous patch levels.</li>
</ul>
<p>
Device manufacturers that include these updates should set the patch string
level to:
</p>
<ul>
  <li>[ro.build.version.security_patch]:[2017-12-01]</li>
  <li>[ro.build.version.security_patch]:[2017-12-05]</li>
</ul>
<p>
<strong>2. Why does this bulletin have two security patch levels?</strong>
</p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
  <li>Devices that use the 2017-12-01 security patch level must include all issues
  associated with that security patch level, as well as fixes for all issues
  reported in previous security bulletins.</li>
  <li>Devices that use the security patch level of 2017-12-05 or newer must
  include all applicable patches in this (and previous) security
  bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Nexus devices available from the <a
href="https://developers.google.com/android/nexus/drivers">Google Developer
site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel&hairsp;/&hairsp;Nexus
bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner
security bulletins are not required for declaring a security patch level.
Android device and chipset manufacturers are encouraged to document the presence
of other fixes on their devices through their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html">LGE</a>, or
<a href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>December 4, 2017</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>December 6, 2017</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body></html>
