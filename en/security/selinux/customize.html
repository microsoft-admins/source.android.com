<html devsite>
  <head>
    <title>Customizing SELinux</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>After you've integrated the base level of SELinux functionality and
thoroughly analyzed the results, you may add your own policy settings to cover
your customizations to the Android operating system. These policies must still
meet the <a href="/compatibility/index.html">Android Compatibility program</a>
requirements and must not remove the default SELinux settings.</p>

<p>Manufacturers should not remove existing SELinux policy. Otherwise, they
risk breaking the Android SELinux implementation and the applications it
governs. This includes third-party applications that will likely need to be
improved to be compliant and operational. Applications must require no
modification to continue functioning on SELinux-enabled devices.</p>

<p>When embarking upon customizing SELinux, remember to:</p>

<ul>
  <li>Write SELinux policy for all new daemons</li>
  <li>Use predefined domains whenever appropriate</li>
  <li>Assign a domain to any process spawned as an <code>init</code> service</li>
  <li>Become familiar with the macros before writing policy</li>
  <li>Submit changes to core policy to AOSP</li>
</ul>

<p>And remember not to:</p>

<ul>
  <li>Create incompatible policy</li>
  <li>Allow end user policy customization</li>
  <li>Allow MDM policy customizations</li>
  <li>Scare users with policy violations</li>
  <li>Add backdoors</li>
</ul>

<p>See the <em>Kernel Security Features</em> section of the
<a href="/compatibility/android-cdd#9_7_kernel_security_features">Android
Compatibility Definition document</a> for specific requirements.</p>

<p>SELinux uses a whitelist approach, meaning all access must be explicitly
allowed in policy in order to be granted. Since Android's default SELinux
policy already supports the Android Open Source Project, you are not required
to modify SELinux settings in any way. If you do customize SELinux settings,
take great care not to break existing applications. To get started:</p>

<ol>
  <li>Use the
    <a href="https://android.googlesource.com/kernel/common/">latest Android
      kernel</a>.</li>
  <li>Adopt the
    <a href="http://en.wikipedia.org/wiki/Principle_of_least_privilege">principle
      of least privilege</a>.</li>
  <li>Address only your own additions to Android. The default policy works with
    the <a href="https://android.googlesource.com/">Android Open Source
      Project</a> codebase automatically.</li>
  <li>Compartmentalize software components into modules that conduct singular
    tasks.</li>
  <li>Create SELinux policies that isolate those tasks from unrelated
    functions.</li>
  <li>Put those policies in <code>*.te</code> files (the extension for SELinux
    policy source files) within the
    <code>/device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>
    directory and use <code>BOARD_SEPOLICY</code> variables to include them in
    your build.</li>
  <li>Make new domains permissive initially. This is done by using a permissive
    declaration in the domain's <code>.te</code> file.</li>
  <li>Analyze results and refine your domain definitions.</li>
  <li>Remove the permissive declaration when no further denials appear in userdebug
    builds.</li>
</ol>

<p>After you've integrated your SELinux policy change, add a step to your
development workflow to ensure SELinux compatibility going forward. In an ideal
software development process, SELinux policy changes only when the software
model changes and not the actual implementation.</p>

<p>As you start customizing SELinux, first audit your additions to Android. If
you've added a component that conducts a new function, ensure the component
meets Android's security policy, as well as any associated policy crafted by
the OEM, before turning on enforcing mode.</p>

<p>To prevent unnecessary issues, it is better to be overbroad and
over-compatible than too restrictive and incompatible, which results in broken
device functions. Conversely, if your changes will benefit others, you should
submit the modifications to the default SELinux policy as a
<a href="/setup/contribute/submit-patches.html">patch</a>. If the patch is
applied to the default security policy, you won't need to make this change with
each new Android release.</p>

<h2 id="example_policy_statements">Example policy statements</h2>

<p>SELinux is based upon the
<a href="https://www.gnu.org/software/m4/manual/index.html" class="external">M4</a>
computer language and therefore supports a variety of macros to save time.</p>

<p>In the following example, all domains are granted access to read from or
write to <code>/dev/null</code> and read from <code>/dev/zero</code>.</p>

<pre class="devsite-click-to-copy">
# Allow read / write access to /dev/null
allow domain null_device:chr_file { getattr open read ioctl lock append write};

# Allow read-only access to /dev/zero
allow domain zero_device:chr_file { getattr open read ioctl lock };
</pre>


<p>This same statement can be written with SELinux <code>*_file_perms</code>
macros (shorthand):</p>

<pre class="devsite-click-to-copy">
# Allow read / write access to /dev/null
allow domain null_device:chr_file rw_file_perms;

# Allow read-only access to /dev/zero
allow domain zero_device:chr_file r_file_perms;
</pre>

<h2 id="example_policy">Example policy</h2>

<p>Here is a complete example policy for DHCP, which we examine below:</p>

<pre class="devsite-click-to-copy">
type dhcp, domain;
permissive dhcp;
type dhcp_exec, exec_type, file_type;
type dhcp_data_file, file_type, data_file_type;

init_daemon_domain(dhcp)
net_domain(dhcp)

allow dhcp self:capability { setgid setuid net_admin net_raw net_bind_service
};
allow dhcp self:packet_socket create_socket_perms;
allow dhcp self:netlink_route_socket { create_socket_perms nlmsg_write };
allow dhcp shell_exec:file rx_file_perms;
allow dhcp system_file:file rx_file_perms;
# For /proc/sys/net/ipv4/conf/*/promote_secondaries
allow dhcp proc_net:file write;
allow dhcp system_prop:property_service set ;
unix_socket_connect(dhcp, property, init)

type_transition dhcp system_data_file:{ dir file } dhcp_data_file;
allow dhcp dhcp_data_file:dir create_dir_perms;
allow dhcp dhcp_data_file:file create_file_perms;

allow dhcp netd:fd use;
allow dhcp netd:fifo_file rw_file_perms;
allow dhcp netd:{ dgram_socket_class_set unix_stream_socket } { read write };
allow dhcp netd:{ netlink_kobject_uevent_socket netlink_route_socket
netlink_nflog_socket } { read write };
</pre>

<p>Let’s dissect the example:</p>

<p>In the first line, the type declaration, the DHCP daemon inherits from the
base security policy (<code>domain</code>). From the previous statement
examples, DHCP can read from and write to <code>/dev/null</code>.</p>

<p>In the second line, DHCP is identified as a permissive domain.</p>

<p>In the <code>init_daemon_domain(dhcp)</code> line, the policy states DHCP is
spawned from <code>init</code> and is allowed to communicate with it.</p>

<p>In the <code>net_domain(dhcp)</code> line, the policy allows DHCP to use
common network functionality from the <code>net</code> domain such as reading
and writing TCP packets, communicating over sockets, and conducting DNS
requests.</p>

<p>In the line <code>allow dhcp proc_net:file write;</code>, the policy states
DHCP can write to specific files in <code>/proc</code>. This line demonstrates
SELinux’s fine-grained file labeling. It uses the <code>proc_net</code> label
to limit write access to only the files under <code>/proc/sys/net</code>.</p>

<p>The final block of the example starting with
<code>allow dhcp netd:fd use;</code> depicts how applications may be allowed to
interact with one another. The policy says DHCP and netd may communicate with
one another via file descriptors, FIFO files, datagram sockets, and UNIX stream
sockets. DHCP may only read to and write from the datagram sockets and UNIX
stream sockets and not create or open them.</p>

<h2 id="available_controls">Available controls</h2>

<table>
 <tr>
    <th>Class</th>
    <th>Permission</th>
 </tr>
 <tr>
    <td>file</td>
    <td>
<pre>
ioctl read write create getattr setattr lock relabelfrom relabelto append
unlink link rename execute swapon quotaon mounton</pre>
</td>
 </tr>
 <tr>
 <td>directory</td>
 <td>
<pre>
add_name remove_name reparent search rmdir open audit_access execmod</pre>
</td>
 </tr>
 <tr>
 <td>socket</td>
 <td>
<pre>
ioctl read write create getattr setattr lock relabelfrom relabelto append bind
connect listen accept getopt setopt shutdown recvfrom sendto recv_msg send_msg
name_bind</pre>
</td>
 </tr>
 <tr>
 <td>filesystem</td>
 <td>
<pre>
mount remount unmount getattr relabelfrom relabelto transition associate
quotamod quotaget</pre>
 </td>
 </tr>
 <tr>
 <td>process</td>
 <td>
<pre>
fork transition sigchld sigkill sigstop signull signal ptrace getsched setsched
getsession getpgid setpgid getcap setcap share getattr setexec setfscreate
noatsecure siginh setrlimit rlimitinh dyntransition setcurrent execmem
execstack execheap setkeycreate setsockcreate</pre>
</td>
 </tr>
 <tr>
 <td>security</td>
 <td>
<pre>
compute_av compute_create compute_member check_context load_policy
compute_relabel compute_user setenforce setbool setsecparam setcheckreqprot
read_policy</pre>
</td>
 </tr>
 <tr>
 <td>capability</td>
 <td>
<pre>
chown dac_override dac_read_search fowner fsetid kill setgid setuid setpcap
linux_immutable net_bind_service net_broadcast net_admin net_raw ipc_lock
ipc_owner sys_module sys_rawio sys_chroot sys_ptrace sys_pacct sys_admin
sys_boot sys_nice sys_resource sys_time sys_tty_config mknod lease audit_write
audit_control setfcap</pre>
</td>
 </tr>
 <tr>
 <td>
<p><strong>MORE</strong></p>
</td>
 <td>
<p><strong>AND MORE</strong></p>
</td>
 </tr>
</table>

<h2 id=neverallow>neverallow rules</h2>

<p>SELinux <code>neverallow</code> rules prohibit behavior that should never occur.
With <a href="/compatibility/cts/">compatibility</a> testing,
SELinux <code>neverallow</code> rules are now enforced across devices.</p>

<p>The following guidelines are intended to help manufacturers avoid errors
related to <code>neverallow</code> rules during customization. The rule numbers
used here correspond to Android 5.1 and are subject to change by release.</p>

<p>Rule 48: <code>neverallow { domain -debuggerd -vold -dumpstate
-system_server } self:capability sys_ptrace;</code><br>
See the man page for <code>ptrace</code>.  The <code>sys_ptrace</code>
capability grants the ability to <code>ptrace</code> any process, which allows a great deal
of control over other processes and should belong only to designated system
components, outlined in the rule.  The need for this capability often indicates
the presence of something that is not meant for user-facing builds or
functionality that isn’t needed. Remove the unnecessary component.</p>

<p>Rule 76: <code>neverallow { domain -appdomain -dumpstate -shell -system_server -zygote } { file_type -system_file -exec_type }:file execute;</code><br>
This rule is intended to prevent the execution of arbitrary code on the system.
Specifically, it asserts that only code on <code>/system</code> gets executed,
which allows security guarantees thanks to mechanisms such as verified boot.
Often, the best solution when encountering a problem with this
<code>neverallow</code> rule is to move the offending code to the
<code>/system</code> partition.</p>

<h2 id="android-o">Customizing SEPolicy in Android 8.0+</h2>
<p>
This section provides guidelines for vendor SELinux policy in Android 8.0 and
higher, including details on Android Open Source Project (AOSP) SEPolicy and
SEPolicy extensions. For more information about how SELinux policy is kept
compatible across partitions and Android versions, see
<a href="/security/selinux/compatibility">Compatibility</a>.
</p>
<h3 id="policy-placement">Policy placement</h3>
<p>
In Android 7.0 and earlier, device manufacturers could add policy to
<code>BOARD_SEPOLICY_DIRS</code>, including policy meant to augment AOSP policy
across different device types. In Android 8.0 and higher, adding a policy to
<code>BOARD_SEPOLICY_DIRS</code> places the policy only in the vendor
image.
</p>
<p>
In Android 8.0 and higher, policy exists in the following locations in AOSP:
</p>
<ul>
  <li><strong>system/sepolicy/public</strong>. Includes policy exported for use
    in vendor-specific policy. Everything goes into the Android 8.0
    <a href="/security/selinux/compatibility">compatibility infrastructure</a>.
    Public policy is meant to persist across releases so you can include
    anything <code>/public</code> in your customized policy. Because of this,
    the type of policy that can be placed in <code>/public</code> is more
    restricted. Consider this the platform's exported policy API: Anything that
    deals with the interface between <code>/system</code> and
    <code>/vendor</code> belongs here.</li>
  <li><strong>system/sepolicy/private</strong>. Includes policy necessary for
    the functioning of the system image, but of which vendor image policy should
    have no knowledge.</li>
  <li><strong>system/sepolicy/vendor</strong>. Includes policy for components that
    go in <code>/vendor</code> but exist in the core platform tree (not
    device-specific directories). This is an artifact of build system's
    distinction between devices and global components; conceptually this is a
    part of the device-specific policy described below.</li>
  <li><strong>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</strong>.
    Includes device-specific policy. Also includes device customizations to
    policy, which in Android 8.0 and higher corresponds to policy for components
    on the vendor image.</li>
</ul>
<h3 id="supported-policy-scenarios">Supported policy scenarios</h3>
<p>
On devices launching with Android 8.0 and higher, the vendor image must work
with the OEM system image and the reference AOSP system image provided by Google
(and pass CTS on this reference image). These requirements ensure a clean
separation between the framework and the vendor code. Such devices support the
following scenarios.
</p>
<h4 id="vendor-image-only-extensions">vendor-image-only extensions</h4>
<p>
<strong>Example</strong>: Adding a new service to <code>vndservicemanager</code>
from the vendor image that supports processes from the vendor image.
</p>
<p>
As with devices launching with previous Android versions, add device-specific
customization in
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>.
New policy governing how vendor components interact with (only) other vendor
components <strong>should involve types present only in
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code></strong>.
Policy written here allows code on vendor to work, will not be updated as part
of a framework-only OTA, and will be present in the combined policy on a device
with the reference AOSP system image.

<h4 id="vendor-image-support-to-work-with-aosp">vendor-image support to work
with AOSP</h4>
<p>
<strong>Example</strong>: Adding a new process (registered with
<code>hwservicemanager</code> from the vendor image) that implements an
AOSP-defined HAL.
</p>
<p>
As with devices launching with previous Android versions, perform
device-specific customization in
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>.
The policy exported as part of <code>system/sepolicy/public/</code> is available
for use, and is shipped as part of the vendor policy. Types and attributes from
the public policy may be used in new rules dictating interactions with the new
vendor-specific bits, subject to the provided <code>neverallow</code>
restrictions. As with the vendor-only case, new policy here will not be updated
as part of a framework-only OTA and will be present in the combined policy on a
device with the reference AOSP system image.
</p>
<h4 id="system-image-only-extensions">system-image-only extensions</h4>
<p>
<strong>Example</strong>: Adding a new service (registered with servicemanager)
that is accessed only by other processes from the system image.
</p>
<p>
Add this policy to <code>system/sepolicy/private</code>. You can add extra
processes or objects to enable functionality in a partner system image, provided
those new bits don't need to interact with new components on the vendor image
(specifically, such processes or objects must fully function without policy from
the vendor image). The policy exported by <code>system/sepolicy/public</code> is
available here just as it is for vendor-image-only extensions.  This policy is
part of the system image and could be updated in a framework-only OTA, but will
not be present when using the reference AOSP system image.
</p>
<h4
id="vendor-image-extensions-that-serve-extended-aosp-components">vendor-image
extensions that serve extended AOSP components</h4>
<p>
<strong>Example:</strong> A new, non-AOSP HAL for use by extended clients that
also exist in the AOSP system image (such as an extended system_server).
</p>
<p>
Policy for interaction between system and vendor must be included in the
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>
directory shipped on the vendor partition.
This is similar to the above scenario of adding vendor-image support to work
with the reference AOSP image, except the modified AOSP components may also
require additional policy to properly operate with the rest of the system
partition (which is fine as long as they still have the public AOSP type
labels).
</p>
<p>
Policy for interaction of public AOSP components with system-image-only
extensions should be in <code>system/sepolicy/private</code>.
</p>

<h4 id="system-image-extensions-that-access-only-AOSP-interfaces">system-image
extensions that access only AOSP interfaces</h4>
<p>
<strong>Example:</strong> A new, non-AOSP system process must access a HAL on
which AOSP relies.
</p>
<p>
This is similar to the <a href="#system-image-only-extensions">system-image-only
extension example</a>, except new system components may interact across the
<code>system/vendor</code> interface. Policy for the new system component must
go in <code>system/sepolicy/private</code>, which is acceptable provided it is
through an interface already established by AOSP in
<code>system/sepolicy/public</code> (i.e. the types and attributes required for
functionality are there). While policy could be included in the device-specific
policy, it would be unable to use other <code>system/sepolicy/private</code>
types or change (in any policy-affecting way) as a result of a framework-only
update. The policy may be changed in a framework-only OTA, but will not be
present when using an AOSP system image (which won't have the new system
component either).
</p>
<h4 id="vendor-image-extensions-that-serve-new-system-components">vendor-image
extensions that serve new system components</h4>
<p>
<strong>Example:</strong> Adding a new, non-AOSP HAL for use by a client process
without an AOSP analogue (and thus requires its own domain).
</p>
<p>
Similar to the <a
href="#vendor-image-extensions-that-serve-extended-aosp-components">AOSP-extensions
example</a>, policy for interactions between system and vendor must go in the
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>
directory shipped on the vendor partition
(to ensure the system policy has no knowledge of vendor-specific details). You
can add new public types that extend the policy in
<code>system/sepolicy/public</code>; this should be done only in addition to the
existing AOSP policy, i.e. do not remove AOSP public policy. The new public
types can then be used for policy in <code>system/sepolicy/private</code> and in
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>.
</p>
<p>
Keep in mind that every addition to <code>system/sepolicy/public</code> adds
complexity by exposing a new compatibility guarantee that must be tracked in a
mapping file and which is subject to other restrictions. Only new types and
corresponding allow rules may be added in <code>system/sepolicy/public</code>;
attributes and other policy statements are not supported. In addition, new
public types cannot be used to directly label objects in the
<code>/vendor</code> policy.
</p>
<h3 id="unsupported-policy-scenarios">Unsupported policy scenarios</h3>
<p>
Devices launching with Android 8.0 and higher do not support the following
policy scenario and examples.
</p>
<h4
id="additional-extensions-to-system-image-that-need-permission-to-new-vendor-image-components-after-a-framework-only-ota">Additional
extensions to system-image that need permission
to new vendor-image components
after a framework-only OTA</h4>
<p>
<strong>Example: </strong>A new non-AOSP system process, requiring its own
domain, is added in the next Android release and needs access to a new,
non-AOSP HAL.
</p>
<p>
Similar to
<a href="#vendor-image-extensions-that-serve-extended-aosp-components">new
(non-AOSP) system and vendor components</a> interaction, except the new system
type is introduced in a
framework-only OTA. Although the new type could be added to the policy in
<code>system/sepolicy/public</code>, the existing vendor policy has no knowledge
of the new type as it is tracking only the Android 8.0 system public policy.
AOSP handles this by exposing vendor-provided resources via an attribute (e.g.
<code>hal_foo</code> attribute) but as attribute partner extensions are not
supported in <code>system/sepolicy/public</code>, this method is unavailable to
vendor policy. Access must be provided by a previously-existing public type.
</p>
<p>
<strong>Example: </strong>A change to a system process (AOSP or non-AOSP) must
change how it interacts with new, non-AOSP vendor component.
</p>
<p>
The policy on the system image must be written without knowledge of specific
vendor customizations. Policy concerning specific interfaces in AOSP is thus
exposed via attributes in system/sepolicy/public so that vendor policy can
opt-in to future system policy which uses these attributes. However,
<strong>attribute extensions in <code>system/sepolicy/public</code> are not
supported</strong>, so all policy dictating how the system components interact
with new vendor components (and which is not handled by attributes already
present in AOSP <code>system/sepolicy/public</code>) must be in
<code>device/<var>manufacturer</var>/<var>device-name</var>/sepolicy</code>.
This means that system types cannot change
the access allowed to vendor types as part of a framework-only OTA.</p>


  </body>
</html>
