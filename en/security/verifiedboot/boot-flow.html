<html devsite>
  <head>
    <title>Boot Flow</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p>
The recommended boot flow for a device is as follows:
</p>
<figure>
  <img src="/security/images/verified-boot-flow.png"
       alt="Verified boot flow">
  <figcaption><b>Figure 1</b>. Verified boot flow</figcaption>
</figure>

<h2 id="flow-for-a-b-devices">Flow for A/B devices</h2>
<p>
If the device is using A/B, the boot flow is slightly different. The slot to
boot must first be marked as <code>SUCCESSFUL</code> using the <a
href="https://android.googlesource.com/platform/hardware/interfaces/+/master/boot/1.0/IBootControl.hal"
class="external">Boot Control HAL</a> <strong>before</strong> updating the
Rollback Protection metadata.
</p>
<p>
If there's a platform update that fails (is not marked
<code>SUCCESSFUL</code>), the A/B stack falls back to the other slot,
which still has the previous version of Android in it. However, if
the Rollback Protection-metadata had been set, the previous version can't
boot because of Rollback Protection.
</p>

<h2 id="communicating-verified-boot-state-to-users">Communicating Verified Boot
  state to users</h2>
<p>
After determining the boot state of a device, you need to communicate that
state to the user. If the device doesn't have any issues, then proceed without
displaying anything. Verified Boot issues fall into these categories:
</p>
<ul>
 <li>YELLOW: Warning screen for LOCKED devices with custom root of trust set</li>
 <li>ORANGE: Warning screen for UNLOCKED devices</li>
 <li>RED (eio): Warning screen for dm-verity corruption</li>
 <li>RED (no os found): No valid OS found</li>
</ul>
<table class="columns">
  <col width="50%">
  <col width="50%">
  <tr>
    <td>
<h3 id="locked-devices-with-custom-root-of-trust">LOCKED devices with custom
root of trust</h3>

<p>
Show a YELLOW screen on every boot if the device is LOCKED, a custom root of
trust has been set, and the image was signed with this custom root of trust.
The YELLOW screen is dismissed after ten seconds and the device continues
booting. If the user presses the power button, "Press power button to pause"
text changes to "Press power button to continue" and the screen is never
dismissed, though the device may dim or turn off the screen to protect against
burn-in). If pressed again, the screen is dismissed and the phone continues
booting.
</p>
<p>
For the <var>hex-number</var>, use the first 8 digits of the sha256 of
the libavb representation of the public key used for verification, for example
<code>f7a24de1</code>.
</p>
<p>
<strong>Suggested text:</strong>
</p>
<p>
Your device has loaded a different operating system.
</p>
<p>
Visit this link on another device to learn more:
</p>
<p>
g.co/ABH
</p>
<p>
ID: <var>hex-number</var>
</p>
<p>
<span class="material-icons">power_settings_new</span> Press power button to pause
</p>
<p>&nbsp;</p>
    </td>
    <td>

<figure>
 <p><strong>Example YELLOW screen:</strong></p>
  <img src="/security/images/boot_yellow1.png"
       alt="Yellow device warning screen">
</figure>
    </td>
  </tr>
  <tr>
    <td>
<h3 id="unlocked-devices">UNLOCKED devices</h3>
<p>
Show an ORANGE screen on every boot if the device is UNLOCKED. The ORANGE screen
is dismissed after ten seconds and the device continues booting. If the user
presses the power button, "Press power button to pause" text changes to "Press
power button to continue" and the screen is never dismissed (the device may dim
and/or turn off the screen if needed to protect against burn-in or similar). If
pressed again, the screen is dismissed and the phone continues booting.
</p>
<p>
For the <var>hex-number</var>, use the first 8 digits of the sha256 of the libavb
representation of the public key used for verification, for example
<code>f7a24de1</code>.
</p>
<p>
<strong>Suggested text:</strong>
</p>
<p>
The boot loader is unlocked and software integrity cannot be guaranteed. Any
data stored on the device may be available to attackers. Do not store any
sensitive data on the device.
</p>
<p>
Visit this link on another device to learn more:
</p>
<p>
g.co/ABH
</p>
<p>
ID: <var>hex-number</var>
</p>
<p>
<span class="material-icons">power_settings_new</span> Press power button to pause.
</p>
<p>&nbsp;</p>
    </td>
    <td>

<figure>
 <p><strong>Example ORANGE screen:</strong></p>
  <img src="/security/images/boot_orange.png"
       alt="Orange device warning screen">
</figure>
    </td>
  </tr>
  <tr>
    <td>

<h3 id="dm-verity-corruption">dm-verity corruption</h3>
<p>
Show a RED <code>eio</code> screen if a valid version of Android is found and
the device is currently in the <code>eio</code> dm-verity mode. The user needs
to click the power button to continue. If the user hasn't acknowledged the
warning screen within 30 seconds, the device powers off (to protect the screen
against burn-in and save power).
</p>

<aside class="note">
<strong>Note:</strong> Other warning screens may follow this screen. For example, if the
device is <code>UNLOCKED</code> the ORANGE screen shows after.
</aside>

<p>
<strong>Suggested text:</strong>
</p>
<p>
Your device is corrupt. It can't be trusted and may not work properly.
</p>
<p>
Visit this link on another device to learn more:
</p>
<p>
g.co/ABH
</p>
<p>
<span class="material-icons">power_settings_new</span> Press power button to continue.
</p>
<p>&nbsp;</p>
    </td>
    <td>
<figure>
 <p><strong>Example RED eio screen:</strong></p>
  <img src="/security/images/boot_red1.png"
       alt="Red eio device warning screen">
</figure>
    </td>
  </tr>
  <tr>
    <td>
    <h3 id="no-valid-os-found">No valid OS found</h3>
<p>
Show a RED screen is shown if no valid version of Android can be found. The
device cannot continue booting. If the user hasn't acknowledged the warning
screen within 30 seconds, the device powers off to protect the screen against
burn-in and save power).
</p>
<p>
For the <var>hex-number</var>, use the first 8 digits of the sha256 of
the libavb representation of the public key used for verification, for example
<code>f7a24de1</code>.
</p>
<p>
<strong>Suggested text:</strong>
</p>
<p>
No valid operating system could be found. The device will not boot.
</p>
<p>
Visit this link on another device to learn more:
</p>
<p>
g.co/ABH
</p>
<p>
ID: <var>hex-number</var>
</p>
<p>
<span class="material-icons">power_settings_new</span> Press power button to power off.
</p>
<p>&nbsp;</p>
    </td>
    <td>
<figure>
 <p><strong>Example RED screen:</strong></p>
  <img src="/security/images/boot_red2.png"
       alt="Red corrupt device warning screen">
</figure>
    </td>
  </tr>
  <tr>
    <td>
<h3 id="unlock-confirmation">Unlock confirmation</h3>
<p>
Show an unlock confirmation screen in response to the
<code>fastboot flashing unlock</code> command being executed via the fastboot
interface. Focus is initially on <em>Don't unlock</em>. If the user hasn't
interacted with the warning screen within 30 seconds, the screen disappears and
the command fails.
</p>
<p>
<strong>Suggested text:</strong>
</p>
<p>
If you unlock the bootloader, you will be able to install custom operating
system software on this phone. A custom OS is not subject to the same level of
testing as the original OS, and can cause your phone and installed applications
to stop working properly. Software integrity cannot be guaranteed with a custom
OS so any data stored on the phone while the bootloader is unlocked may be at
risk.
</p>
<p>
To prevent unauthorized access to your personal data, unlocking the bootloader
will also delete all personal data on your phone.
</p>
<p>
Press the Volume Up/Volume Down to select whether to unlock the bootloader, then
the power button to continue.
</p>
<p>
Unlock
</p>
<p>
Unlock bootloader.
</p>
<p>
Don't unlock
</p>
<p>
Do not unlock bootloader and restart phone.
</p>
<p>&nbsp;</p>
    </td>
    <td>
<figure>
 <p><strong>Example screen:</strong></p>
  <img src="/security/images/unlock-confirmation.png"
       alt="UNLOCK device warning screen">
</figure>
    </td>
  </tr>
  <tr>
    <td>
<h3 id="lock-confirmation">Lock confirmation</h3>
<p>
Show a lock confirmation screen in response to the <code>fastboot flashing
lock</code> command being executed via the fastboot interface. Focus is
initially on <em>Don't lock</em>. If the user hasn't interacted with the
warning screen within 30 seconds, the screen disappears and the command fails.
</p>
<p>
<strong>Text:</strong>
</p>
<p>
If you lock the bootloader, you will not be able to install custom operating
system software on this phone. To prevent unauthorized access to your personal
data, locking the bootloader will also delete all personal data on your phone.
</p>
<p>
Press the Volume Up/Volume Down to select whether to lock the bootloader, then
the power button to continue.
</p>
<p>
Lock
</p>
<p>
Lock bootloader.
</p>
<p>
Don't lock
</p>
<p>
Do not lock bootloader and restart phone.
</p>
<p>&nbsp;</p>
    </td>
    <td>
<figure>
 <p><strong>Example screen:</strong></p>
  <img src="/security/images/lock-confirmation.png"
       alt="LOCK confirmation device warning screen">
</figure>
    </td>
  </tr>
</table>
<h2 id="communicating-verified-boot-state-to-android">Communicating Verified
Boot state to Android</h2>
<p>
The bootloader communicates Verified Boot state to Android through
kernel-command options. It sets the <code>androidboot.verifiedstate</code>
option to one of the following values:
</p>
<ul>
 <li><code>green</code>: if device is <code>LOCKED</code> and user-settable
     root of trust is not used</li>
 <li><code>yellow</code>: if device is <code>LOCKED</code> and user-settable
     root of trust is used</li>
 <li><code>orange</code>: if device is <code>UNLOCKED</code></li>
</ul>
<p>
The <code>androidboot.veritymode</code> option is set to <code>eio</code> or
<code>restart</code> depending on which state the boot loader is in with respect
to handling dm-verity errors. For more details, see <a
href="/security/verifiedboot/verified-boot#handling-verification-errors">Handling
verification errors</a>.
</p>
</body>
</html>
