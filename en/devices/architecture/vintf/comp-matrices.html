<html devsite>
  <head>
    <title>Compatibility Matrices</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>This section describes the framework and device compatibility matrices and
the <a href="#compatibility-matrix-schema">compatibility matrix schema</a>. For
match rules, see <a href="/devices/architecture/vintf/match-rules.html">Matching
Rules</a>.</p>

<h2 id="framework-compatibility-matrix">Framework compatibility matrix
(FCM)</h2>
<p>The framework compatibility matrix describes the requirements of the
framework on the device it runs on. The matrix file is associated with the
Android framework image (on <code>system.img</code>). It is expected the
requirements of the FCM will be satisfied by the device manifest (requirements
enforced at launch and OTA time).</p>

<p>Example FCM file:</p>

<pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!-- Comments, Legal notices, etc. here -->
&lt;compatibility-matrix version="1.0" type="framework" level="3">
    &lt;hal>
        &lt;name>android.hardware.camera&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;version>3.1-4&lt;/version>
        &lt;interface>
            &lt;name>ICameraProvider&lt;/name>
            &lt;instance>default&lt;/instance>
            &lt;regex-instance>[a-z_]+/[0-9]+&lt;/regex-instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hardware.nfc&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>INfc&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal optional="true">
        &lt;name>android.hardware.graphics.composer&lt;/name>
        &lt;version>2.1&lt;/version>
    &lt;/hal>
    &lt;hal format="native">
        &lt;name>GL&lt;/name>
        &lt;version>1.1&lt;/version>
        &lt;version>3.0&lt;/version>
    &lt;/hal>
    &lt;hal format="native">
        &lt;name>EGL&lt;/name>
        &lt;version>1.1&lt;/version>
    &lt;/hal>
    &lt;kernel version="3.18.51">
        &lt;!-- common configs -->
    &lt;/kernel>
    &lt;kernel version="3.18.51">
        &lt;!-- arm specific configs -->
        &lt;condition>
            &lt;config>
                &lt;key>CONFIG_ARM&lt;/key>
                &lt;value type="tristate">y&lt;/value>
            &lt;/config>
        &lt;condition>
        &lt;config>
            &lt;key>CONFIG_A&lt;/key>
            &lt;value type="string">&lt;/value>
        &lt;/config>
        &lt;config>
            &lt;key>CONFIG_B&lt;/key>
            &lt;value type="tristate">y&lt;/value>
        &lt;/config>
    &lt;/kernel>
    &lt;kernel version="4.1.22">
        &lt;!-- common configs -->
        &lt;config>
            &lt;key>CONFIG_A&lt;/key>
            &lt;value type="string">foo&lt;/value>
        &lt;/config>
        &lt;config>
            &lt;key>CONFIG_B2&lt;/key>
            &lt;value type="int">1024&lt;/value>
        &lt;/config>
    &lt;/kernel>
    &lt;sepolicy>
        &lt;kernel-sepolicy-version>30&lt;/kernel-sepolicy-version>
        &lt;sepolicy-version>25.0&lt;/sepolicy-version>
        &lt;sepolicy-version>26.0-3&lt;/sepolicy-version>
    &lt;/sepolicy>
    &lt;avb>
        &lt;vbmeta-version>2.1&lt;/vbmeta-version>
    &lt;/avb>
    &lt;xmlfile format="dtd">
        &lt;name>media_profile&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;path>/system/etc/media_profile_V1_0.dtd&lt;/path>
    &lt;/xmlfile>
&lt;/compatibility-matrix>
</pre>

<p>For more details, see <a href="/devices/architecture/vintf/fcm">FCM
Lifecycle</a>.</p>

<h2 id="device-compatibility-matrix">Device compatibility matrix (DCM)</h2>
<p>The device compatibility matrix describes a set of requirements the device
expects from the framework (requirements enforced at launch and OTA time).
</p>
<p>Example DCM file:</p>

<pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!-- Comments, Legal notices, etc. here -->
&lt;compatibility-matrix version="1.0" type="device">
    &lt;hal>
        &lt;name>android.hidl.manager&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>IServiceManager&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hidl.memory&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>IMemory&lt;/name>
            &lt;instance>ashmem&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.hidl.allocator&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>IAllocator&lt;/name>
            &lt;instance>ashmem&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;hal>
        &lt;name>android.framework.sensor&lt;/name>
        &lt;version>1.0&lt;/version>
        &lt;interface>
            &lt;name>ISensorManager&lt;/name>
            &lt;instance>default&lt;/instance>
        &lt;/interface>
    &lt;/hal>
    &lt;vendor-ndk>
        &lt;version>27&lt;/version>
    &lt;/vendor-ndk>
    &lt;system-sdk>
        &lt;version>27&lt;/version>
    &lt;/system-sdk>
&lt;/compatibility-matrix>
</pre>

<h2 id="compatibility-matrix-schema">Compatibility matrix schema</h2>
<p>This section describes the meaning of these XML tags. Some "required" tags
can be missing from the source file in Android source tree and written by
<code><a href="/devices/architecture/vintf/resources#assemble_vintf">assemble_vintf</a></code>
at build time. "Required" tags must be present in the corresponding files on the
device.</p>

<dl>
<dt><code>?xml</code></dt>
<dd>Optional. It only provides information to the XML parser.</dd>
<dt><code>compatibility-matrix.version</code></dt>
<dd>Required. Meta-version of this compatibility matrix. Describes the elements
expected in the compatibility matrix. Unrelated to XML version.</dd>
<dt><code>compatibility-matrix.type</code></dt>
<dd>Required. Type of this compatibility matrix:
 <ul>
 <li><code>"device"</code>: Device compatibility matrix.</li>
 <li><code>"framework"</code>: Framework compatibility matrix.</li>
 </ul>
</dd>
<dt><code>manifest.level</code></dt>
<dd>Required for framework compatibility matrix. Specifies the Framework
Compatibility Matrix Version (FCM Version) of this file. Should not be
declared in device-specific framework compatibility matrix (i.e.
<code>DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE</code>).</dd>
<dt><code>compatibility-matrix.hal</code></dt>
<dd>Optional and can repeat. Lists a single HAL (HIDL or native) that is
required by owner of the compatibility matrix (framework or device) to be
present. HAL entries are distinguished by a <code>&lt;name&gt;</code> element;
there can be several HAL entries with the same name (implies "and" condition).
</dd>
<dt><code>compatibility-matrix.hal.format</code></dt>
<dd>Optional. Value can be one of:
 <ul>
 <li><code>"hidl"</code>: HIDL HALs. This is the default.</li>
 <li><code>"native"</code>: native HALs.</li>
 </ul>
</dd>
<dt><code>compatibility-matrix.hal.optional</code></dt>
<dd>Attribute is optional and defaults to false. States whether this HAL is
optional to the owner of the compatibility matrix (framework or device). If a
<code>&lt;hal&gt;</code> entry is marked as optional, it means the owner can
work with this HAL, if present, but does not require it to be present.</dd>
<dt><code>compatibility-matrix.hal.name</code></dt>
<dd>Required. Full package name of this HAL. Examples:
 <ul>
 <li><code>android.hardware.camera</code> (HIDL HAL)</li>
 <li><code>GLES</code> (native HAL, requires name only)</li>
 </ul>
</dd>
<dt><code>compatibility-matrix.hal.version</code></dt>
<dd>Required, can repeat without duplicates. A list of version ranges (see
<a href="/devices/architecture/vintf/match-rules.html#hals">HAL matches</a>)
that defines what versions the owner of the compatibility matrix (framework or
device) expects.</dd>
<dt><code>compatibility-matrix.hal.interface</code></dt>
<dd>Optional, can repeat. A list of required interfaces of this HAL.</dd>
<dt><code>compatibility-matrix.hal.interface.name</code></dt>
<dd>Required. Name of the interface.</dd>
<dt><code>compatibility-matrix.hal.interface.instance</code></dt>
<dd>Optional, can repeat. A list of required instances of this interface.</dd>
<dt><code>compatibility-matrix.hal.interface.regex-instance</code></dt>
<dd>Optional, can repeat. A list of required instance name patterns on this
interface. Use
<a href="http://man7.org/linux/man-pages/man7/regex.7.html" class="external">Extended
Regular Expression</a> format.</dd>
<dt><code>compatibility-matrix.kernel</code></dt>
<dd>Optional, can repeat. Specify a list of kernel configs that the framework
requires on each kernel version.<br>
Multiple <code>&lt;kernel></code> with the same <code>&lt;version></code> can
exist to imply "and" relationship. Each <code>&lt;kernel></code> is a "fragment"
of the requirements that are enabled only when <code>&lt;conditions></code> are
met.</dd>
<dt><code>compatibility-matrix.kernel.version</code></dt>
<dd>Required. Kernel version. Format is
<code><var>VERSION</var>.<var>MAJOR_REVISION</var>.<var>MINOR_REVISION</var></code>.
Version and major revision must match exactly. Minor revision defines the
minimum LTS version of the kernel the framework expects.</dd>
<dt><code>compatibility-matrix.kernel.condition</code></dt>
<dd>Optional. Must not exist for the first <code>&lt;kernel></code> of each
version. Specifies a list of conditions. Only when the conditions are met are
the requirements stated in this <code>&lt;kernel></code> fragment is enabled.
</dd>
<dt><code>compatibility-matrix.kernel.config</code></dt>
<dd>Optional, can repeat. Lists <code>CONFIG</code> items that must be
matched for this kernel version. Each <code>CONFIG</code> item is a key-value
pair; config items are distinguished by key.</dd>
<dt><code>compatibility-matrix.kernel.config.key</code></dt>
<dd>Required. Key name of the <code>CONFIG</code> item. Starts with
<code>CONFIG_</code>.</dd>
<dt><code>compatibility-matrix.kernel.config.value</code></dt>
<dd>Required. Value of the <code>CONFIG</code> item. Format depends on type:
 <ul>
 <li><code>string</code>. Quotes are omitted.</li>
 <li><code>int</code>. Decimal and hexadecimal (must start with <code>0x</code>
 or <code>0X)</code>values are accepted. Interpreted as an 64-bit integer;
 overflows result in truncation. (Parser accepts values from -2<sup>64</sup> + 1
 to 2<sup>64</sup> - 1, 65th bit is truncated; for details refer to the
 <a href="http://man7.org/linux/man-pages/man3/strtoul.3.html" class="external">strtoull
 man page</a>.)</li>
 <li><code>range</code>. Format is <code>[int]-[int]</code>, e.g.
 <code>10-20</code>. Hexadecimal values are accepted and must start with
 <code>0x</code> or <code>0X</code>. Two boundaries must be an unsigned 64-bit
 integer.</li>
 <li><code>tristate</code>. Valid values are <code>y</code>, <code>m</code> and
 <code>n</code>.</li>
 </ul>
</dd>
<dt><code>compatibility-matrix.kernel.config.value.type</code></dt>
<dd>Required. Type of the value of the <code>CONFIG</code> item, one of:
 <ul>
 <li><code>string</code></li>
 <li><code>int</code></li>
 <li><code>range</code></li>
 <li><code>tristate</code></li>
 </ul>
</dd>
<dt><code>compatibility-matrix.sepolicy</code></dt>
<dd>Required. Contains all sepolicy-related entries. Used only by the
framework compatibility matrix.</dd>
<dt><code>compatibility-matrix.sepolicy.sepolicy-version</code></dt>
<dd>Required, can repeat. Describes the requirement on sepolicy version.
Corresponds to <code>manifest.sepolicy.version</code>. Each instance of an
element defines a range of sepolicy versions.</dd>
<dt><code>compatibility-matrix.sepolicy.kernel-sepolicy-version</code></dt>
<dd>Required. Declares the <code>policydb</code> version the framework works
with.</dd>
<dt><code>compatibility-matrix.avb.vbmeta-version</code></dt>
<dd>Optional; used only by the framework compatibility matrix. Declares the
<a href="/devices/architecture/vintf/match-rules.html#avb-version">AVB
version</a> used to sign <code>system.img</code>.</dd>
<dt><code>compatibility-matrix.vendor-ndk</code></dt>
<dd>Optional; used only by the device compatibility matrix. Declares the
requirement of the VNDK vendor snapshot. If missing, no VNDK requirement is made
on the system image.</dd>
<dt><code>compatibility-matrix.vendor-ndk.version</code></dt>
<dd>Required. A positive integer that declares a VNDK version required by the
vendor image.</dd>
<dt><code>compatibility-matrix.vendor-ndk.library</code></dt>
<dd>Optional, can repeat. Declares a set of VNDK libraries required by the
vendor image. Same semantics as <code>manifest.vendor-ndk.library</code>.</dd>
<dt><code>compatibility-matrix.system-sdk.version</code></dt>
<dd>Optional, can repeat; used only by the device compatibility matrix. Declares
the requirement by vendor apps on System SDK versions. If missing, no System SDK
requirement is made on the system image.</dd>
</dl>

  </body>
</html>

