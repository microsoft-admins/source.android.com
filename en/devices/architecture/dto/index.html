<html devsite>
  <head>
    <title>Device Tree Overlays</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  {% include "_versions.html" %}
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>A device tree (DT) is a data structure of named nodes and properties that
describe non-discoverable hardware. Operating systems, such as the Linux kernel
used in Android, use DTs to support a wide range of hardware configurations used
by Android-powered devices. Hardware vendors supply their own DT source files,
which Linux then compiles into the Device Tree Blob (DTB) file used by the
bootloader.</p>

<p>A <a href="https://lkml.org/lkml/2012/11/5/615" class="external">device tree
overlay</a> (DTO) enables a central device tree blob (DTB) to be overlaid on
the device tree. A bootloader using DTO can maintain the system-on-chip (SoC)
DT and dynamically overlay a device-specific DT, adding nodes to the tree and
making changes to properties in the existing tree.</p>

<p>This page details a typical bootloader workflow for loading a DT and provides
a list of common DT terms. Other pages in this section describe how to
<a href="/devices/architecture/dto/implement.html">implement bootloader support
for DTO</a>, how to
<a href="/devices/architecture/dto/compile.html">compile</a>, verify, and
<a href="/devices/architecture/dto/optimize.html">optimize your DTO
implementation</a>, and how to
<a href="/devices/architecture/dto/multiple.html">use multiple DTs</a>. You can
also get details on <a href="/devices/architecture/dto/syntax.html">DTO
syntax</a> and required
<a href="/devices/architecture/dto/partitions.html">DTO/DTBO partition
formatting</a>.</p>

<h2 id="p-update">Updates in Android {{ androidPVersionNumber }} Release</h2>
<p>In Android {{ androidPVersionNumber }}, the bootloader must not modify the
properties defined in the device tree overlays before passing the unified
device tree blob to the kernel.</p>

<h2 id=load-dt>Loading a device tree</h2>
<p>Loading a device tree in bootloader involves building, partitioning, and
running.</p>

<figure id="treble_dto_bootloader">
<img src="../images/treble_dto_bootloader.png">
<figcaption><strong>Figure 1.</strong> Typical implementation for loading device
tree in bootloader.</figcaption>
</figure>

<ol>
<li>To build:
<ul>
<li>Use the device tree compiler (<code>dtc</code>) to compile device tree
source (<code>.dts</code>) into a device tree blob (<code>.dtb</code>),
formatted as a flattened device tree.</li>
<li>Flash the <code>.dtb</code> file into a bootloader runtime-accessible
location (detailed below).</li>
</ul>
</li>
<li>To partition, determine a bootloader runtime-accessible and trusted location
in flash memory to put <code>.dtb</code>. Example locations:

<div style="width:75%">
<div class="attempt-left">
<table><tr><th style="text-align: center;">Boot Partition</th></tr></table>
<figure id="treble_dto_partition_1">
<img src="../images/treble_dto_partition_1.png" style="display:block; margin:0 auto">
<figcaption>
<strong>Figure 2.</strong> Put <code>.dtb</code> in boot partition by appending
to <code>image.gz</code> and passing as "<code>kernel</code>" to
<code>mkbootimg</code>.
</figcaption>
</figure>
</div>
<div class="attempt-right">
<table><tr><th style="text-align: center;">Unique Partition</th></tr></table>
<figure id="treble_dto_partition_2">
<img src="../images/treble_dto_partition_2.png" style="display:block; margin:0 auto">
<figcaption>
<strong>Figure 3.</strong> Put <code>.dtb</code> in an unique partition (e.g.
<code>dtb</code> partition).
</figcaption>
</figure>
</div>
</div>
</li>

<li style="clear:both">To run:
<ul>
<li>Load <code>.dtb</code> from storage into memory.</li>
<li>Start kernel given the memory address of the loaded DT.</li>
</ul>
</li>
</ol>

<h2 id=terms>Terminology</h2>
<p>This section uses the following device tree terms:</p>
<table>
<tbody>
<tr>
<th>DT</th>
<td>Device Tree</td>
</tr>
<tr>
<th>DTB</th>
<td>Device Tree Blob</td>
</tr>
<tr>
<th>DTBO</th>
<td>Device Tree Blob for Overlay</td>
</tr>
<tr>
<th>DTC</th>
<td>Device Tree Compiler</td>
</tr>
<tr>
<th>DTO</th>
<td>Device Tree Overlay</td>
</tr>
<tr>
<th>DTS</th>
<td>Device Tree Source</td>
</tr>
<tr>
<th>FDT</th>
<td>Flattened Device Tree, a binary format contained in a <code>.dtb</code> blob
file</td>
</tr>
</tbody>
</table>

  </body>
</html>
