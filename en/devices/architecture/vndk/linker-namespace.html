<html devsite>
  <head>
    <title>Linker Namespace</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>The dynamic linker tackles two challenges in Treble VNDK design:</p>

<ul>
 <li>SP-HAL shared libraries and their dependencies, including VNDK-SP
 libraries, are loaded into framework processes. There should be some
 mechanisms to prevent symbol conflicts.</li>

 <li><code>dlopen()</code> and <code>android_dlopen_ext()</code> may introduce
 some run-time dependencies that are not visible at build-time and can be
 difficult to detect using static analysis.</li>
</ul>

<p>These two challenges can be resolved by the <em>linker namespace</em>
mechanism. The linker namespace mechanism is provided by the dynamic linker. It
can isolate the shared libraries in different linker namespaces so that
libraries with same library name but with different symbols won't conflict.</p>

<p>On the other hand, the linker namespace mechanism provides the flexibility
so that some shared libraries may be exported by a linker namespace and used by
another linker namespace. These exported shared libraries may become the
application programming interfaces that are public to other programs while
hiding their implementation details within their linker namespaces.</p>

<p>For example, <code>/system/lib[64]/libcutils.so</code> and
<code>/system/lib[64]/vndk-sp-${VER}/libcutils.so</code> are two shared
libraries. These two libraries may have different symbols. They are loaded
into different linker namespaces so that framework modules can depend on
<code>/system/lib[64]/libcutils.so</code> and SP-HAL shared libraries can
depend on <code>/system/lib[64]/vndk-sp-${VER}/libcutils.so</code>.</p>

<p>On the other hand, <code>/system/lib[64]/libc.so</code> is an example of
public libraries that is exported by a linker namespace and imported into
many linker namespaces. The dependencies of
<code>/system/lib[64]/libc.so</code>, such as <code>libnetd_client.so</code>,
is loaded into the namespace in which <code>/system/lib[64]/libc.so</code>
resides. Other namespaces won't have accesses to those dependencies. This
mechanism encapsulates the implementation details while providing the public
interfaces.</p>





<h2 id="how-does-it-work">How does it work?</h2>

<p>The dynamic linker is responsible for loading the shared libraries specified
in <code>DT_NEEDED</code> entries or the shared libraries specified by the
argument of <code>dlopen()</code> or <code>android_dlopen_ext()</code>. In both
cases, the dynamic linker finds the linker namespace where the caller
resides and try to load the dependencies into the same linker namespace. If
the dynamic linker cannot load the shared library into the specified linker
namespace, it asks the <em>linked linker namespace</em> for exported shared
libraries.</p>





<h2 id="configuration-file-format">Configuration file format</h2>

<p>The configuration file format is based on the INI file format. A typical
configuration file looks like:</p>

<pre class="prettyprint">
dir.system = /system/bin
dir.system = /system/xbin
dir.vendor = /vendor/bin

[system]
additional.namespaces = sphal,vndk

namespace.default.isolated = true
namespace.default.search.paths = /system/${LIB}
namespace.default.permitted.paths = /system/${LIB}/hw
namespace.default.asan.search.paths = /data/asan/system/${LIB}:/system/${LIB}
namespace.default.asan.permitted.paths = /data/asan/system/${LIB}/hw:/system/${LIB}/hw

namespace.sphal.isolated = true
namespace.sphal.visible = true
namespace.sphal.search.paths = /odm/${LIB}:/vendor/${LIB}
namespace.sphal.permitted.paths = /odm/${LIB}:/vendor/${LIB}
namespace.sphal.asan.search.paths  = /data/asan/odm/${LIB}:/odm/${LIB}
namespace.sphal.asan.search.paths += /data/asan/vendor/${LIB}:/vendor/${LIB}
namespace.sphal.asan.permitted.paths  = /data/asan/odm/${LIB}:/odm/${LIB}
namespace.sphal.asan.permitted.paths += /data/asan/vendor/${LIB}:/vendor/${LIB}
namespace.sphal.links = default,vndk
namespace.sphal.link.default.shared_libs = libc.so:libm.so
namespace.sphal.link.vndk.shared_libs = libbase.so:libcutils.so

namespace.vndk.isolated = true
namespace.vndk.search.paths = /system/${LIB}/vndk-sp-29
namespace.vndk.permitted.paths = /system/${LIB}/vndk-sp-29
namespace.vndk.links = default
namespace.vndk.link.default.shared_libs = libc.so:libm.so

[vendor]
namespace.default.isolated = false
namespace.default.search.paths = /vendor/${LIB}:/system/${LIB}
</pre>

<p>The configuration file includes:</p>

<ul>
 <li>Several directory-section mapping properties at the beginning for the
  dynamic linker to select the effective section.</li>

 <li>
  Several linker namespaces configuration sections:

  <ul>
   <li>Each section contains several namespaces (graph vertices) and several
   fallback links between namespaces (graph arcs).</li>

   <li>Each namespace has its own isolation, search paths, permitted paths,
   and visibility settings.</li>
  </ul>
 </li>
</ul>

<p>The tables below describe the meaning of each property in detail.</p>



<h3 id="directory-section-mapping-property">Directory-section mapping property</h3>

<table>
 <tr>
  <th>Property</th>
  <th>Description</th>
  <th>Example</th>
 </tr>

 <tr>
  <td><p><code>dir.<var>name</var></code></p></td>

  <td>
   <p>A path to a directory that the <code>[<var>name</var>]</code> section
   applies to.</p>

   <p>Each property maps the executables under the directory to a linker
   namespaces configuration section. There might be two (or more) properties
   with the same <code><var>name</var></code> but point to different
   directories.</p>
  </td>

  <td>
   <p>
    <code>dir.system = /system/bin</code><br/>
    <code>dir.system = /system/xbin</code><br/>
    <code>dir.vendor = /vendor/bin</code><br/>
   </p>

   <p>This indicates that the configuration specified in the
   <code>[system]</code> section applies to the executables that are loaded
   from either <code>/system/bin</code> or <code>/system/xbin</code>.</p>

   <p>The configuration specified in the <code>[vendor]</code> section applies
   to the executables that are loaded from <code>/vendor/bin</code>.</p>
  </td>
 </tr>
</table>



<h3 id="relation-properties">Relation properties</h3>

<table>
 <tr>
  <th>Property</th>
  <th>Description</th>
  <th>Example</th>
 </tr>

 <tr>
  <td><code>additional.<wbr/>namespaces</code></td>

  <td>
   <p>A comma-separated list of additional namespaces (in addition to the
   <code>default</code> namespace) for the section.</p>
  </td>

  <td>
   <p><code>additional.<wbr/>namespaces = sphal,<wbr/>vndk</code></p>

   <p>This indicates there are three namespaces (<code>default</code>,
   <code>sphal</code>, and <code>vndk</code>) in the <code>[system]</code>
   configuration.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>links</code></td>

  <td>
   <p>A comma-separated list of fallback namespaces.</p>

   <p>If a shared library cannot be found in the current namespace, the dynamic
   linker tries to load the shared library from the fallback namespaces. The
   namespace specified at the beginning of the list has higher priority.</p>

   <aside class="note">Note: Fallback namespaces are not transitive. For
   example, namespace <em>A</em> links to namespace <em>B</em> and namespace
   <em>B</em> links to namespace <em>C</em>. If the dynamic linker can not
   find the library in namespace <em>A</em>, it ONLY searches namespace
   <em>B</em>. It doesn't search namespace <em>C</em>.</aside>
  </td>

  <td>
   <p><code>namespace.<wbr/>sphal.<wbr/>links = default,<wbr/>vndk</code></p>

   <p>If a shared library or an executable requests a shared library that
   cannot be loaded into the <code>sphal</code> namespace, the dynamic linker
   tries to load the shared library from the <code>default</code>
   namespace.</p>

   <p>And then, if the shared library cannot be loaded from the
   <code>default</code> namespace either, the dynamic linker tries to load the
   shared library from the <code>vndk</code> namespace.</p>

   <p>Finally, if all attempts fail, the dynamic linker returns an error.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>link.<wbr/><var>other</var>.<wbr/>shared_libs</code></td>

  <td>
   <p>A colon-separated list of shared libraries that can be searched in the
   <code>other</code> namespaces when those libraries cannot be found in the
   <code>name</code> namespace.</p>

   <p>This property cannot be used with
   <code>namespace.<wbr/><var>name</var>.<wbr/>link.<wbr/><var>other</var>.<wbr/>allow_all_shared_libs</code>.</p>

   <aside class="note"><p>Note: This property shares the same underlying
   implementation with
   <a href="/devices/tech/config/namespaces_libraries">public.libraries.txt</a>
   files. Both mechanisms control the imported shared libraries by specifying a
   link with a library name filter.</p>

   <p>The difference is that <code>ld.config.txt</code> is loaded by the
   dynamic linker and all namespaces are created when a program starts. On the
   contrary, <code>libnativeloader</code> creates a linker namespace when the
   Zygote process is forked and specialized for an application. The namespace
   for the application native libraries has a link that only allows the library
   names specified in <code>public.libraries.txt</code>.</p></aside>
  </td>

  <td>
   <p><code>namespace.<wbr/>sphal.<wbr/>link.<wbr/>default.<wbr/>shared_libs = libc.so:<wbr/>libm.so</code></p>

   <p>This indicates that the fallback link only accepts <code>libc.so</code>
   or <code>libm.so</code> as the requested library name. The dynamic linker
   ignores the fallback link from <code>sphal</code> to
   <code>default</code> namespace if the requested library name is not
   <code>libc.so</code> nor <code>libm.so</code>.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>link.<wbr/><var>other</var>.<wbr/>allow_all_shared_libs</code></td>

  <td>
   <p>A boolean value that indicates whether all shared libraries can be
   searched in the <code><var>other</var></code> namespace when those libraries cannot
   be found in the <code><var>name</var></code> namespace.</p>

   <p>This property cannot be used with
   <code>namespace.<wbr/><var>name</var>.<wbr/>link.<wbr/><var>other</var>.<wbr/>shared_libs</code>.</p>
  </td>

  <td>
   <p><code>namespace.<wbr/>vndk.<wbr/>link.<wbr/>sphal.<wbr/>allow_all_shared_libs = true</code></p>

   <p>This indicates all library names can walk through the fallback link
   from <code>vndk</code> to <code>sphal</code> namespace.</p>
  </td>
 </tr>
</table>



<h3 id="namespace-properties">Namespace properties</h3>

<table>
 <tr>
  <th>Property</th>
  <th>Description</th>
  <th>Example</th>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>isolated</code></td>

  <td>
   <p>A boolean value that indicates whether the dynamic linker should check
   where the shared library resides.</p>

   <p>If <code>isolated</code> is <code>true</code>, only the shared libraries
   that are <em>in</em> one of the <code>search.paths</code> directories
   (excluding sub-directories) or are <em>under</em> one of the
   <code>permitted.paths</code> directories (including sub-directories) can be
   loaded.</p>

   <p>If <code>isolated</code> is <code>false</code> (default), the dynamic
   linker doesn't check the path of shared libraries.</p>
  </td>

  <td>
   <p><code>namespace.<wbr/>sphal.<wbr/>isolated = true</code></p>

   <p>This indicates that only the shared libraries in
   <code>search.paths</code> or under <code>permitted.paths</code> can be
   loaded into the <code>sphal</code> namespace.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>search.paths</code></td>

  <td>
   <p>A colon-separated list of directories to search for shared
   libraries.</p>

   <p>The directories specified in <code>search.paths</code> are prepended
   to the requested library name if function calls to <code>dlopen()</code> or
   <code>DT_NEEDED</code> entries do not specify the full path. The directory
   specified at the beginning of the list has higher priority.</p>

   <p>When <code>isolated</code> is <code>true</code>, shared libraries that
   are <em>in</em> one of the <code>search.paths</code> directories (excluding
   sub-directories) can be loaded regardless the <code>permitted.paths</code>
   property.</p>

   <p>For example, if <code>search.paths</code> is
   <code>/system/${LIB}</code> and <code>permitted.paths</code> is empty,
   <code>/system/${LIB}/libc.so</code> can be loaded but
   <code>/system/${LIB}/vndk/libutils.so</code> cannot be loaded.</p>

   <aside class="note">Note: <code>${LIB}</code> is a built-in
   placeholder. It is replaced by <code>lib</code> for 32-bit processes and
   <code>lib64</code> for 64-bit processes.</aside>
  </td>

  <td>
   <p><code>namespace.<wbr/>default.<wbr/>search.paths = /system/${LIB}</code></p>

   <p>This indicates that the dynamic linker searches
   <code>/system/${LIB}</code> for shared libraries.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>asan.search.paths</code></td>

  <td>
   <p>A colon-separated list of directories to search for shared libraries when
   <a href="/devices/tech/debug/asan">Address Sanitizer</a> is enabled.</p>

   <p><code>namespace.<wbr/><var>name</var>.<wbr/>search.paths</code> is
   ignored when <a href="/devices/tech/debug/asan">Address Sanitizer</a> is
   enabled.</p>
  </td>

  <td>
   <p><code>namespace.<wbr/>default.<wbr/>asan.search.paths = /data/asan/system/${LIB}:<wbr/>/system/${LIB}</code></p>

   <p>This indicates that when
   <a href="/devices/tech/debug/asan">Address Sanitizer</a> is enabled the
   dynamic linker searches <code>/data/asan/system/${LIB}</code> first and
   then searches <code>/system/${LIB}</code>.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>permitted.paths</code></td>

  <td>
   <p>A colon-separated list of directories (including sub-directories) where
   the dynamic linker can load the shared libraries (in addition to
   <code>search.paths</code>) when <code>isolated</code> is
   <code>true</code>.</p>

   <p>The shared libraries that are under the sub-directories of
   <code>permitted.paths</code> can be loaded too. For example, if
   <code>permitted.paths</code> is <code>/system/${LIB}</code>,
   both <code>/system/${LIB}/libc.so</code> and
   <code>/system/${LIB}/vndk/libutils.so</code> can be loaded.</p>

   <p>If <code>isolated</code> is <code>false</code>,
   <code>permitted.paths</code> are ignored and a warning is emitted.</p>

   <aside class="note">Note: <code>permitted.paths</code> are NOT
   prepended to the requested library names when the dynamic linker is
   searching for a shared library. Its main purpose is to allow programers to
   load shared libraries into an isolated namespace by specifying the full path
   of the shared library.</aside>
  </td>

  <td>
   <p><code>namespace.<wbr/>default.<wbr/>permitted.paths = /system/${LIB}/hw</code></p>

   <p>This indicates that the shared libraries under
   <code>/system/${LIB}/hw</code> can be loaded into the isolated
   <code>default</code> namespace.</p>

   <p>For example, without <code>permitted.paths</code>,
   <code>libaudiohal.so</code> won't be able to load
   <code>/system/${LIB}/hw/audio.a2dp.default.so</code> into the
   <code>default</code> namespace.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>asan.permitted.paths</code></td>

  <td>
   <p>A colon-separated list of directories where the dynamic linker can load
   the shared libraries when <a href="/devices/tech/debug/asan">Address
   Sanitizer</a> is enabled.</p>

   <p><code>namespace.<wbr/><var>name</var>.<wbr/>permitted.paths</code> is
   ignored when <a href="/devices/tech/debug/asan">Address Sanitizer</a> is
   enabled.</p>
  </td>

  <td>
   <p><code>namespace.<wbr/>default.<wbr/>asan.permitted.paths = /data/asan/system/${LIB}/hw:<wbr/>/system/${LIB}/hw</code></p>

   <p>This indicates that when
   <a href="/devices/tech/debug/asan">Address Sanitizer</a> is enabled shared
   libraries under <code>/data/asan/system/${LIB}/hw</code> or
   <code>/system/${LIB}/hw</code> can be loaded to the isolated
   <code>default</code> namespace.</p>
  </td>
 </tr>

 <tr>
  <td><code>namespace.<wbr/><var>name</var>.<wbr/>visible</code></td>

  <td>
   <p>A boolean value that indicates whether the program (other than
   <code>libc</code>) can obtain a linker namespace handle with
   <code>android_get_exported_namespace()</code> and open a shared library in
   the linker namespace by passing the handle to
   <code>android_dlopen_ext()</code>.</p>

   <p>If <code>visible</code> is <code>true</code>,
   <code>android_get_exported_namespace()</code> always returns the handle if
   the namespace exists.</p>

   <p>If <code>visible</code> is <code>false</code> (default),
   <code>android_get_exported_namespace()</code> always returns
   <code>NULL</code> regardless the presence of the namespace. Shared libraries
   can only be loaded into this namespace if (1) they are requested by another
   linker namespace that has a fallback link to this namespace or (2) they are
   requested by other shared libraries or executables in this namespace.</p>
  </td>

  <td>
   <p><code>namespace.<wbr/>sphal.<wbr/>visible = true</code></p>

   <p>This indicates that <code>android_get_exported_namespace("sphal")</code>
   can return a valid linker namespace handle.</p>
  </td>
 </tr>
</table>





<h2 id="linker-namespace-isolation">Linker namespace isolation</h2>

<p>There are three configuration files in
<code>${android-src}/system/core/rootdir/etc</code>. Depending on the value of
<code>PRODUCT_TREBLE_LINKER_NAMESPACES</code>, <code>BOARD_VNDK_VERSION</code>,
and <code>BOARD_VNDK_RUNTIME_DISABLE</code> in <code>BoardConfig.mk</code>,
different configurations are selected:</p>

<table>
 <tr>
  <th><code>PRODUCT_TREBLE_</code><br/><code>LINKER_NAMESPACES</code></th>
  <th><code>BOARD_VNDK_</code><br/><code>VERSION</code></th>
  <th><code>BOARD_VNDK_</code><br/><code>RUNTIME_DISABLE</code></th>
  <th>Selected configuration</th>
  <th>VTS Requirement</th>
 </tr>

 <tr>
  <td rowspan="3"><code>true</code></td>
  <td rowspan="2"><code>current</code></td>
  <td><em>empty</em></td>
  <td><code>ld.config.txt</code></td>
  <td>Mandatory for devices launched with Android P.</td>
 </tr>

 <tr>
  <td><code>true</code></td>
  <td rowspan="2"><code>ld.config.vndk_lite.txt</code></td>
  <td rowspan="2">Mandatory for devices launched with Android 8.x.</td>
 </tr>

 <tr>
  <td><em>empty</em></td>
  <td><em>any</em></td>
 </tr>

 <tr>
  <td><code>false</code></td>
  <td><em>any</em></td>
  <td><em>any</em></td>
  <td><code>ld.config.legacy.txt</code></td>
  <td>For non-Treble devices</td>
 </tr>
</table>

<p><code>${android-src}/system/core/rootdir/etc/ld.config.vndk_lite.txt</code>
isolates SP-HAL and VNDK-SP shared libraries. In Android 8.0 and higher, this
must be the configuration file for dynamic linker when
<code>PRODUCT_TREBLE_LINKER_NAMESPACES</code> is <code>true</code>.</p>

<p><code>${android-src}/system/core/rootdir/etc/ld.config.txt</code> isolates
SP-HAL and VNDK-SP shared libraries as well. In addition,
<code>ld.config.txt</code> also provides the full dynamic linker isolation.
It makes sure that modules in the system partition won't depend on the shared
libraries in the vendor partitions and vice versa.</p>

<p>In Android 8.1, <code>ld.config.txt</code> is the default configuration file
and it is highly recommended to enable full dynamic linker isolation. However,
if there are too many dependencies to be cleaned up in Android 8.1, you may add
<code>BOARD_VNDK_RUNTIME_DISABLE</code> to <code>BoardConfig.mk</code>:</p>

<pre class="prettyprint">
BOARD_VNDK_RUNTIME_DISABLE := true
</pre>

<p>If <code>BOARD_VNDK_RUNTIME_DISABLE</code> is <code>true</code>,
<code>${android-src}/system/core/rootdir/etc/ld.config.vndk_lite.txt</code>
is installed.</p>



<h3 id="ld.config.txt">ld.config.txt</h3>

<p><code>ld.config.txt</code> isolates the shared library dependencies
between the system partition and vendor partitions. Compared to
<code>ld.config.txt</code> mentioned in previous subsection, the differences
are outlined as following items:</p>

<ul>
 <li>
  <p>Framework Processes</p>

  <ul>
   <li>Four namespaces (<code>default</code>, <code>vndk</code>,
   <code>sphal</code>, and <code>rs</code>) are created.</li>

   <li>All namespaces are isolated.</li>

   <li>System shared libraries are loaded into the <code>default</code>
   namespace.</li>

   <li>SP-HALs are loaded into the <code>sphal</code> namespace.</li>

   <li>VNDK-SP shared libraries loaded into the <code>vndk</code>
   namespace.</li>
  </ul>
 </li>

 <li>
  <p>Vendor Processes</p>

  <ul>
   <li>Three namespaces (<code>default</code>, <code>vndk</code>, and
   <code>system</code>) are created.</li>

   <li>The <code>default</code> namespace is isolated.</li>

   <li>Vendor shared libraries are loaded into the <code>default</code>
   namespace.</li>

   <li>VNDK and VNDK-SP shared libraries are loaded into the <code>vndk</code>
   namespace.</li>

   <li>LL-NDK and their dependencies are loaded into the <code>system</code>
   namespace.</li>
  </ul>
 </li>
</ul>

<p>The relationship between the linker namespaces is depicted in the figure
below:</p>

<img src="../images/treble_vndk_linker_namespace3.png"
     alt="Linker namespace graph described in ld.config.txt" />
<figcaption>
 <strong>Figure 1.</strong> Linker namespace isolation
 (<code>ld.config.txt</code>)
</figcaption>


<p>In the graph above, <em>LL-NDK</em> and <em>VNDK-SP</em> stand for following
shared libraries:</p>

<ul>
 <li>
  <em>LL-NDK</em>

  <ul>
   <li><code>libEGL.so</code></li>
   <li><code>libGLESv1_CM.so</code></li>
   <li><code>libGLESv2.so</code></li>
   <li><code>libGLESv3.so</code></li>
   <li><code>libandroid_net.so</code></li>
   <li><code>libc.so</code></li>
   <li><code>libdl.so</code></li>
   <li><code>liblog.so</code></li>
   <li><code>libm.so</code></li>
   <li><code>libnativewindow.so</code></li>
   <li><code>libneuralnetworks.so</code></li>
   <li><code>libsync.so</code></li>
   <li><code>libvndksupport.so</code></li>
   <li><code>libvulkan.so</code></li>
  </ul>
 </li>

 <li>
  <em>VNDK-SP</em>

  <ul>
   <li><code>android.hardware.graphics.common@1.0.so</code></li>
   <li><code>android.hardware.graphics.mapper@2.0.so</code></li>
   <li><code>android.hardware.renderscript@1.0.so</code></li>
   <li><code>android.hidl.memory@1.0.so</code></li>
   <li><code>libRSCpuRef.so</code></li>
   <li><code>libRSDriver.so</code></li>
   <li><code>libRS_internal.so</code></li>
   <li><code>libbase.so</code></li>
   <li><code>libbcinfo.so</code></li>
   <li><code>libc++.so</code></li>
   <li><code>libcutils.so</code></li>
   <li><code>libhardware.so</code></li>
   <li><code>libhidlbase.so</code></li>
   <li><code>libhidlmemory.so</code></li>
   <li><code>libhidltransport.so</code></li>
   <li><code>libhwbinder.so</code></li>
   <li><code>libion.so</code></li>
   <li><code>libutils.so</code></li>
   <li><code>libz.so</code></li>
  </ul>
 </li>
</ul>

<p>The table below presents the namespaces configuration for framework
processes, which is excerpted from the <code>[system]</code> section in
<code>ld.config.txt</code>:</p>

<table>
 <tr>
  <th>Namespace</th>
  <th>Property</th>
  <th>Value</th>
 </tr>

 <tr>
  <td rowspan="3"><code>default</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/system/${LIB}</code><br/>
   <code>/product/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/system/${LIB}/drm</code><br/>
   <code>/system/${LIB}/extractors</code><br/>
   <code>/system/${LIB}/hw</code><br/>
   <code>/product/${LIB}</code><br/>
   <code>/system/framework</code><br/>
   <code>/system/app</code><br/>
   <code>/system/priv-app</code><br/>
   <code>/vendor/app</code><br/>
   <code>/vendor/priv-app</code><br/>
   <code>/oem/app</code><br/>
   <code>/odm/priv-app</code><br/>
   <code>/oem/app</code><br/>
   <code>/product/framework</code><br/>
   <code>/product/app</code><br/>
   <code>/product/priv-app</code><br/>
   <code>/data</code><br/>
   <code>/mnt/expand</code>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td rowspan="8"><code>sphal</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>default,vndk,rs</code></td>
 </tr>

 <tr>
  <td><code>link.default.shared_libs</code></td>
  <td><em>LL-NDK</em></td>
 </tr>

 <tr>
  <td><code>link.vndk.shared_libs</code></td>
  <td><em>VNDK-SP</em></td>
 </tr>

 <tr>
  <td><code>link.rs.shared_libs</code></td>
  <td><code>libRS_internal.so</code></td>
 </tr>

 <tr>
  <td rowspan="7"><code>vndk</code> (For VNDK-SP)</td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}/vndk-sp</code><br/>
   <code>/vendor/${LIB}/vndk-sp</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm/${LIB}/hw</code><br/>
   <code>/odm/${LIB}/egl</code><br/>
   <code>/vendor/${LIB}/hw</code><br/>
   <code>/vendor/${LIB}/egl</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}/hw</code>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>default</code>, <code>sphal</code></td>
 </tr>

 <tr>
  <td><code>link.default.shared_libs</code></td>
  <td><em>LL-NDK</em></td>
 </tr>

 <tr>
  <td><code>link.default.allow_all_shared_libs</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td rowspan="7"><code>rs</code> (For Renderscript)</td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}/vndk-sp</code><br/>
   <code>/vendor/${LIB}/vndk-sp</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}</code><br/>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code><br/>
   <code>/data</code>  (For compiled RS kernel)
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>default,vndk</code></td>
 </tr>

 <tr>
  <td><code>link.default.shared_libs</code></td>
  <td>
   <em>LL-NDK</em><br/>
   <code>libmediandk.so</code><br/>
   <code>libft2.so</code>
  </td>
 </tr>

 <tr>
  <td><code>link.vndk.shared_libs</code></td>
  <td><em>VNDK-SP</em></td>
 </tr>
</table>

<p>The table below presents the namespaces configuration for vendor processes,
which is excerpted from the <code>[vendor]</code> section in
<code>ld.config.txt</code>:</p>

<table>
 <tr>
  <th>Namespace</th>
  <th>Property</th>
  <th>Value</th>
 </tr>

 <tr>
  <td rowspan="7"><code>default</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm</code><br/>
   <code>/vendor</code><br/>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>system</code>, <code>vndk</code></td>
 </tr>

 <tr>
  <td><code>link.system.shared_libs</code></td>
  <td><em>LL-NDK</em></td>
 </tr>

 <tr>
  <td><code>link.vndk.shared_libs</code></td>
  <td><em>VNDK</em>, <em>VNDK-SP</em> (vendor available)</td>
 </tr>

 <tr>
  <td rowspan="5"><code>vndk</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}/vndk</code><br/>
   <code>/odm/${LIB}/vndk-sp</code><br/>
   <code>/vendor/${LIB}/vndk</code><br/>
   <code>/vendor/${LIB}/vndk-sp</code><br/>
   <code>/system/${LIB}/vndk-${VER}</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}</code>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>system</code>, <code>default</code></td>
 </tr>

 <tr>
  <td><code>link.system.shared_libs</code></td>
  <td><em>LL-NDK</em></td>
 </tr>

 <tr>
  <td><code>link.default.allow_all_shared_libs</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td rowspan="2"><code>system</code></td>
  <td><code>search.paths</code></td>
  <td><code>/system/${LIB}</code></td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>false</code></td>
 </tr>
</table>


<p>More details can be found in
<code>${android-src}/system/core/rootdir/etc/ld.config.txt</code>.</p>



<h3 id="ld.config.vndk_lite.txt">ld.config.vndk_lite.txt</h3>

<p>As of Android 8.0, the dynamic linker is configured to isolate SP-HAL and
VNDK-SP shared libraries such that their symbols do not conflict with other
framework shared libraries. The relationship between the linker namespaces is
shown below:</p>

<img src="../images/treble_vndk_linker_namespace1.png"
     alt="Linker namespace graph described in ld.config.vndk_lite.txt" />
<figcaption>
 <strong>Figure 2.</strong> Linker namespace isolation
 (<code>ld.config.vndk_lite.txt</code>)
</figcaption>

<p><em>LL-NDK</em> and <em>VNDK-SP</em> stand for following shared libraries:
</p>

<ul>
 <li>
  <em>LL-NDK</em>

  <ul>
   <li><code>libEGL.so</code></li>
   <li><code>libGLESv1_CM.so</code></li>
   <li><code>libGLESv2.so</code></li>
   <li><code>libc.so</code></li>
   <li><code>libdl.so</code></li>
   <li><code>liblog.so</code></li>
   <li><code>libm.so</code></li>
   <li><code>libnativewindow.so</code></li>
   <li><code>libstdc++.so</code> (Not in <code>ld.config.txt</code>)</li>
   <li><code>libsync.so</code></li>
   <li><code>libvndksupport.so</code></li>
   <li><code>libz.so</code> (Moved to <em>VNDK-SP</em> in
   <code>ld.config.txt</code>)</li>
  </ul>
 </li>

 <li>
  <em>VNDK-SP</em>

  <ul>
   <li><code>android.hardware.graphics.common@1.0.so</code></li>
   <li><code>android.hardware.graphics.mapper@2.0.so</code></li>
   <li><code>android.hardware.renderscript@1.0.so</code></li>
   <li><code>android.hidl.memory@1.0.so</code></li>
   <li><code>libbase.so</code></li>
   <li><code>libc++.so</code></li>
   <li><code>libcutils.so</code></li>
   <li><code>libhardware.so</code></li>
   <li><code>libhidlbase.so</code></li>
   <li><code>libhidlmemory.so</code></li>
   <li><code>libhidltransport.so</code></li>
   <li><code>libhwbinder.so</code></li>
   <li><code>libion.so</code></li>
   <li><code>libutils.so</code></li>
  </ul>
 </li>
</ul>

<p>The table below presents the namespaces configuration for framework
processes, which is excerpted from the <code>[system]</code> section in
<code>ld.config.vndk_lite.txt</code>:</p>

<table>
 <tr>
  <th>Namespace</th>
  <th>Property</th>
  <th>Value</th>
 </tr>

 <tr>
  <td rowspan="2"><code>default</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/system/${LIB}</code><br/>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code><br/>
   <code>/product/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>false</code></td>
 </tr>

 <tr>
  <td rowspan="8"><code>sphal</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>default,vndk,rs</code></td>
 </tr>
 <tr>
  <td><code>link.default.shared_libs</code></td>
  <td><em>LL-NDK</em></td>
 </tr>
 <tr>
  <td><code>link.vndk.shared_libs</code></td>
  <td><em>VNDK-SP</em></td>
 </tr>
 <tr>
  <td><code>link.rs.shared_libs</code></td>
  <td><code>libRS_internal.so</code></td>
 </tr>

 <tr>
  <td rowspan="6"><code>vndk</code> (For VNDK-SP)</td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}/vndk-sp</code><br/>
   <code>/vendor/${LIB}/vndk-sp</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm/${LIB}/hw</code><br/>
   <code>/odm/${LIB}/egl</code><br/>
   <code>/vendor/${LIB}/hw</code><br/>
   <code>/vendor/${LIB}/egl</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}/hw</code><br/>
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>default</code></td>
 </tr>

 <tr>
  <td><code>link.default.shared_libs</code></td>
  <td><em>LL-NDK</em></td>
 </tr>

 <tr>
  <td rowspan="7"><code>rs</code> (For Renderscript)</td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}/vndk-sp</code><br/>
   <code>/vendor/${LIB}/vndk-sp</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}</code><br/>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code>
  </td>
 </tr>

 <tr>
  <td><code>permitted.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/vendor/${LIB}</code><br/>
   <code>/data</code>  (For compiled RS kernel)
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>visible</code></td>
  <td><code>true</code></td>
 </tr>

 <tr>
  <td><code>links</code></td>
  <td><code>default,vndk</code></td>
 </tr>

 <tr>
  <td><code>link.default.shared_libs</code></td>
  <td>
   <em>LL-NDK</em><br/>
   <code>libmediandk.so</code><br/>
   <code>libft2.so</code>
  </td>
 </tr>

 <tr>
  <td><code>link.vndk.shared_libs</code></td>
  <td><em>VNDK-SP</em></td>
 </tr>
</table>

<p>The table below presents the namespaces configuration for vendor processes,
which is excerpted from the <code>[vendor]</code> section in
<code>ld.config.vndk_lite.txt</code>:</p>

<table>
 <tr>
  <th>Namespace</th>
  <th>Property</th>
  <th>Value</th>
 </tr>

 <tr>
  <td rowspan="2"><code>default</code></td>
  <td><code>search.paths</code></td>
  <td>
   <code>/odm/${LIB}</code><br/>
   <code>/odm/${LIB}/vndk</code><br/>
   <code>/odm/${LIB}/vndk-sp</code><br/>
   <code>/vendor/${LIB}</code><br/>
   <code>/vendor/${LIB}/vndk</code><br/>
   <code>/vendor/${LIB}/vndk-sp</code><br/>
   <code>/system/${LIB}/vndk-${VER}</code><br/>
   <code>/system/${LIB}/vndk-sp-${VER}</code><br/>
   <code>/system/${LIB}</code>  (Deprecated)<br/>
   <code>/product/${LIB}</code>  (Deprecated)
  </td>
 </tr>

 <tr>
  <td><code>isolated</code></td>
  <td><code>false</code></td>
 </tr>
</table>

<p>More details can be found in
<code>${android-src}/system/core/rootdir/etc/ld.config.vndk_lite.txt</code>.</p>





<h2 id="document-history">Document history</h2>

<h3 id="changes-p">Android P changes</h3>

<ul>
 <li><p>In Android P, the <code>vndk</code> linker namespace is added to vendor
  processes and VNDK shared libraries are isolated from the default linker
  namespace.</p></li>

 <li><p>Replace <code>PRODUCT_FULL_TREBLE</code> with more specific
  <code>PRODUCT_TREBLE_LINKER_NAMESPACES</code>.</p></li>

 <li>
  <p>Android P changes the names of the following dynamic linker configuration
  files:</p>

  <table>
   <tr>
    <th>Android 8.x</th>
    <th>Android P</th>
    <th>Description</th>
   </tr>

   <tr>
    <td>ld.config.txt.in</td>
    <td>ld.config.txt</td>
    <td>For devices with runtime linker namespace isolation</td>
   </tr>

   <tr>
    <td>ld.config.txt</td>
    <td>ld.config.vndk_lite.txt</td>
    <td>For devices with VNDK-SP linker namespace isolation</td>
   </tr>

   <tr>
    <td>ld.config.legacy.txt</td>
    <td>ld.config.legacy.txt</td>
    <td>For legacy devices running Android 7.x and earlier</td>
   </tr>
  </table>
 </li>

 <li><p>Remove <code>android.hardware.graphics.allocator@2.0.so</code>.</p></li>

 <li><p><code>product</code> and <code>odm</code> partitions are added.</p></li>
</ul>

  </body>
</html>
