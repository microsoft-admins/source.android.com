<html devsite>
  <head>
    <title>VNDK Snapshot Design</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  VNDK snapshots can be used by a system image to provide the correct VNDK
  libraries to vendor images even when system and vendor images are built from
  different versions of Android. Creating a VNDK snapshot requires capturing
  VNDK libraries as a snapshot and marking them with a version number. The
  vendor image may link with a specific VNDK version that provides required ABIs
  for the modules in the vendor image. However, within the same VNDK version,
  the VNDK libraries must be
  <a href="/devices/architecture/hidl/hashing#abi-stability">ABI-stable</a>.
</p>

<p>
  VNDK snapshot design includes methods for
  <a href="/devices/architecture/vndk/snapshot-generate.html">generating the
  pre-builds of a VNDK snapshot</a> from the current system image and
  <a href="/devices/architecture/vndk/snapshot-generate.html#install-vndk-snapshot">installing
  those pre-built libs</a> to the system partition of a newer Android version.
</p>

<h2 id=about-vndk-libs>About VNDK libraries</h2>

<p>
  <a href="/devices/architecture/index.html#hidl">HIDL-HALs</a>, introduced in
  Android 8.0, enables separate upgrades for system and vendor partitions. VNDK
  defines sets of libraries (VNDK-core, VNDK-SP and LL-NDK) that vendor code can
  link with and blocks the vendors from using libraries that are not in a VNDK
  set. As a result, the vendor image can be built and run if the proper VNDK
  sets on the system image are provided to the vendor image.
</p>

<aside class="note"><strong>Note:</strong> For details on these libraries, refer
  to <a href="/devices/architecture/vndk/index.html#concepts">VNDK concepts</a>.
</aside>

<h3 id="vndk-core">VNDK-core</h3>

<p>
  The VNDK-core set of libraries is installed in
  <code>/system/lib[64]/vndk-${VER}</code> and is available
  <strong>only</strong> for vendor processes with the API level equal to
  <code>${VER}</code>. System processes may not use these libraries and must
  instead use the libraries installed in <code>/system/lib[64]</code>. Because
  of the strict namespace restriction for each process, the VNDK-core libraries
  are safe from dual-loading.
</p>

<p>To include a library in VNDK-core, add the following to
  <code>Android.bp</code>:
</p>

<pre class="prettyprint">
vendor_available: true,
vndk: {
    enabled: true,
},
</pre>

<aside class="note"><strong>Note:</strong> If a system process loads library
  <code>foo.so</code> from <code>system/lib</code> and loads another
  <code>foo.so</code> from <code>system/lib/vndk</code>, <code>foo.so</code> is
  dual-loaded. Normally it is unsafe to load the same library twice in a
  process.
</aside>

<h3 id="vndk-sp">VNDK-SP</h3>

<p>
  VNDK-SP libraries are installed in <code>/system/lib[64]/vndk-sp-${VER}</code>
  and are available to vendor processes and system processes (through the SP-HAL
  libraries installed in vendor partition). VNDK-SP libraries may be
  dual-loaded.
</p>

<p>
  To include a library in VNDK-SP, add the following to <code>Android.bp</code>:
</p>

<pre class="prettyprint">
vendor_available: true,
vndk: {
    enabled: true,
    support_system_process: true,
},
</pre>

<h3 id="ll-ndk">LL-NDK</h3>

<p>
  LL-NDK libraries are installed in <code>/system/lib[64]</code>. Vendor modules
  can use LL-NDK stub libraries to access pre-selected symbols of LL-NDK
  libraries. LL-NDK libraries must be backward-compatible and ABI-stable to
  enable old versions of vendor modules to use new versions of LL-NDK libraries.
  Because of the ABI-stable characteristics of LL-NDK, the VNDK snapshot does
  not need to include LL-NDK libraries for old vendor images.
</p>

<h2 id="about-vndk-snapshots">About VNDK snapshots</h2>

<p>
  Android 8.1 included <a href="/devices/architecture/vndk/build-system">VNDK
  libraries built from the source code</a>. However, for later versions of
  Android, each VNDK version must be captured as a snapshot and provided as a
  pre-build to enabling linking to an older vendor image.
</p>

<p>
  Starting in Android {{ androidPVersionNumber }}, new versions of Android will
  include at least one snapshot of VNDK-core and VNDK-SP directories for older
  versions in the Android source code. At build time, required snapshots will be
  installed to <code>/system/lib[64]/vndk-${VER}</code> and
  <code>/system/lib[64]/vndk-sp-${VER}</code> (directories that can be used by
  the vendor partition), where <code>${VER}</code> is the string variable that
  represents the version name of the VNDK snapshot.
</p>

<p>
  As the VNDK snapshot libraries may differ for each VNDK version, the VNDK
  snapshot also includes the linker namespace configurations, installed as 
  <code>etc/ld.config.${VER}.txt</code>,
  <code>/etc/llndk.libraries.${VER}.txt</code>, and
  <code>/etc/vndksp.libraries.${VER}.txt</code>.
</p>

<h3 id="example-upgrade-system-vendor">Example: Upgrading system and vendor
images</h3>

<p>
  No snapshot required; build without additional configurations for VNDK
  snapshots.
</p>

<h3 id="example-upgrade-system-only">Example: Upgrading system image only</h3>

<p>
  Must include the VNDK snapshot and linker namespace configuration files for
  the vendor image in the system image. The linker namespace configuration files
  are automatically configured to search for VNDK libraries in
  <code>/system/lib[64]/vndk-${VER}</code> and
  <code>/system/lib[64]/vndk-sp-${VER}</code>.
</p>

<img src="/devices/architecture/images/vndk_snapshot_system_only.png">
<figcaption><strong>Figure 1.</strong> Upgrading system only</figcaption>

<h3 id="example-upgrade-system-minor-vendor">Example: Upgrading system image,
minor vendor image change</h3>

<p>
  Building a vendor image against a VNDK snapshot is not yet supported, so you
  must build the vendor image separately with its original source code, then
  upgrade the system image as described in the previous example.
</p>

<h2 id="vndk-snapshot-arch">VNDK snapshot architecture</h2>

<p>
  To make an Android {{ androidPVersionNumber }} system image compatible with an
  Android 8.1 vendor image, the VNDK snapshot that matches the Android 8.1
  vendor image must be provided with the Android {{ androidPVersionNumber }}
  system image, as shown below:
</p>

<img src="/devices/architecture/images/vndk_snapshot_arch.png">
<figcaption><strong>Figure 2.</strong> VNDK snapshot architecture</figcaption>

<p>
  The VNDK snapshot design includes the following methods:
</p>

<ul>
  <li><strong>Generating a snapshot for VNDK-core and VNDK-SP
  libraries</strong>. Android {{ androidPVersionNumber }} includes a script you
  can use to make a snapshot of the current VNDK build. This script bundles all
  libraries in <code>/system/lib[64]/vndk-28</code> and
  <code>/system/lib[64]/vndk-sp-28</code> that were built with the current
  source as a VNDK snapshot, where <code>28</code> is the VNDK version of
  Android {{ androidPVersionNumber }}. The snapshot also includes the linker
  namespace configuration files <code>/etc/ld.config.28.txt</code>,
  <code>/etc/llndk.libraries.28.txt</code>, and
  <code>/etc/vndksp.libraries.28.txt</code>. The generated snapshot will be used
  with newer Android versions (higher than Android {{ androidPVersionNumber }}).
  </li>
  <li><strong>Installing pre-built VNDK-core and VNDK-SP libraries from a
  snapshot</strong>. In Android {{ androidPVersionNumber }}, a VNDK snapshot has
  a set of pre-built VNDK-core libraries and a set of VNDK-SP libraries, as well
  as linker namespace configuration files. When you provide a list of VNDK
  snapshot versions to be installed, at build time, the system image installs
  the VNDK snapshot libraries to <code>/system/lib[64]/vndk-${VER}</code> and
  the <code>/system/lib[64]/vndk-sp-${VER}</code> directories and linker
  namespace configuration files for those VNDK snapshots to
  <code>/etc</code> directory.</li>
</ul>

<h3 id="vndk-versioning">VNDK versioning</h3>

<p>
  Each Android release has only one VNDK snapshot and the SDK version is used as
  a VNDK version (which means the VNDK version has an integer number, such as 27
  for Android 8.1). The VNDK version is fixed when the Android version is
  released. The VNDK version used by the vendor partition is stored
  automatically in the <code>ro.vndk.version</code> property, which can be read
  on runtime. This version is then used in identifying the vendor VNDK version
  for some libraries and identifying the VNDK snapshot version for namespace
  configuration.
</p>

<h3 id="build-vndk-libs">Building VNDK libraries</h3>

<p>
  The <code>make vndk</code> command builds libraries that have <code>vndk:
  { enabled: true, &hellip; }</code>, including dependencies and namespace
  configuration files. If <code>BOARD_VNDK_VERSION := current</code> is set,
  these libraries are built with the <code>make</code> command.
<p>

<p>
  Because this build does not install the VNDK libraries from the snapshot, the
  installed VNDK libraries are not ABI-stable. However, when an Android version
  is released, the ABI for the current VNDK version is fixed. At this point, any
  ABI breakage is a build error, so patches to the Android version must not
  change the ABI for VNDK libraries.
</p>

</body>
</html>
