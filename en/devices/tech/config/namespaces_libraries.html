<html devsite>
  <head>
    <title>Namespaces for Native Libraries</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->



<p>
Android 7.0 introduced namespaces for native libraries to limit internal API
visibility and resolve situations when apps accidentally end up using platform
libraries instead of their own. See the <a
href="http://android-developers.blogspot.com/2016/06/improving-stability-with-private-cc.html">Improving
Stability with Private C/C++ Symbol Restrictions in Android 7.0</a> Android
Developers blog post for application-specific changes.</p>

<h2 id="architecture">Architecture</h2>

<p>
The change separates system libraries from application libraries and makes it
hard to use internal system libraries by accident (and vice versa).
</p>

<img src="images/namespace-libraries.png" alt="Namespaces for native libraries" id="namespace-libraries" />
<p class="img-caption">
  <strong>Figure 1.</strong> Namespaces for native libraries
</p>

<p>
Namespaces for native libraries prevent apps from using private-platform native
APIs (as was done with OpenSSL). It also removes situations where apps
accidentally end up using platform libraries instead of their own (as witnessed
with <code>libpng</code>).
</p>

<h2 id="adding-additional-native-libraries">Adding additional native
libraries</h2>

<p>
In addition to standard public native libraries, silicon vendors (starting from Android 7.0) and
device manufactures (starting from Android 9) may choose to provide additional native libraries
accessible to apps by putting them under the respective library folders and explicitly listing them
in .txt files.
</p>

<p>The library folders are:</p>
<ul>
  <li><code>/vendor/lib</code> (for 32-bit) and <code>/vendor/lib64</code> (for 64-bit)
    for libraries from silicon vendors</li>
  <li><code>/system/lib</code> (for 32-bit) and <code>/system/lib64</code> (for 64-bit)
    for libraries from device manufacturers</li>
</ul>

<p>The .txt files are:</p>
<ul>
  <li><code>/vendor/etc/public.libraries.txt</code> for libraries from silicon vendors</li>
  <li><code>/system/etc/public.libraries-COMPANYNAME.txt</code> for libraries from device manufacturers,
    where <code>COMPANYNAME</code> refers to a name of the manufacturer (such as
    <code>awesome.company</code>). <code>COMPANYNAME</code> should match with
    <code>[A-Za-z0-9_.-]+</code>; alphanumeric characters, _, . (dot) and -. It is possible to
    have multiple such .txt files in a device if some libraries are from external solution
    providers.
</ul>

<p>
  Native libraries in the <code>system</code> partition that are made public by device manufacturers
  <strong>MUST</strong> be named <code>lib*COMPANYNAME.so</code>, e.g., <code>libFoo.awesome.company.so</code>.
  In other words, <code>libFoo.so</code> without the company name suffix MUST NOT be made public.
  The <code>COMPANYNAME</code> in the library file name MUST match with the <code>COMPANYNAME</code> in the
  txt file name in which the library name is listed.
</p>

<p>
  Native libraries that are part of AOSP MUST NOT be made public (except the standard
  public native libraries which are public by default). Only the additional libraries added by
  silicon vendors or device manufacturers can be made accessible to apps.
</p>

<p>
Starting from Android 8.0, vendor public libraries have the following additional
restrictions and required setups:
</p>

<ol>
  <li>The native library in vendor must be properly labeled so it can be
    accessible to apps. If access is required by any apps (including third
    party apps), the library must be labeled as <code>same_process_hal_file</code>
    in a vendor-specific <code>file_contexts</code> file as follows:
    <pre class="devsite-click-to-copy">/vendor/lib(64)?/libnative.so u:object_r:same_process_hal_file:s0</pre>
    where <code>libnative.so</code> is the name of the native library.
  </li>
  <li>The library, either directly or transitively via its dependencies, must not
    depend on system libraries other than VNDK-SP and LLNDK libraries. The list of
    VNDK-SP and LLNDK libraries can be found at
    <code>development/vndk/tools/definition/tool/datasets/eligible-list-&lt;version&gt;-release.csv</code>.
  </li>
</ol>

<h2 id="updating-app-non-public">Updating apps to not use non-public native libraries</h2>

<p>
This feature is enabled only for applications targeting SDK version 24 or later;
for backward compatibility, see <a
href="http://android-developers.blogspot.com/2016/06/improving-stability-with-private-cc.html">Table
1. What to expect if your app is linking against private native libraries</a>.
The list of Android native libraries accessible to apps (also know as
public native libraries) is listed in CDD section 3.1.1.  Apps targeting 24 or
later and using any non-public libraries should be updated. Please see <a
href="https://developer.android.com/preview/behavior-changes.html#ndk">NDK Apps
Linking to Platform Libraries </a> for more details.
</p>

  </body>
</html>
