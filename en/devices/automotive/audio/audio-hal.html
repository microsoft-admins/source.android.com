<html devsite>
  <head>
    <title>Implementing the Audio HAL</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  {% include "_versions.html" %}
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


<p>
  Automotive audio implementations rely on the standard
  <a href="/devices/audio/implement"> Android Audio HAL</a>, which includes the
  following:
</p>

<ul>
  <li><code><strong>IDevice</code></strong>
  (<code>hardware/interfaces/audio/2.0/IDevice.hal</code>). Creates input and
  output streams, handles master volume and muting, and uses:
  <ul>
    <li><code>createAudioPatch</code> to create external-external patches
    between devices.</li>
    <li><code>IDevice.setAudioPortConfig()</code> to provide volume for each
    physical stream.</li>
  </ul>
  <li><code><strong>IStream</code></strong>
  (<code>hardware/interfaces/audio/2.0/IStream.hal</code>). Along with its In
  and Out variants, manages the actual streaming of audio samples to and from
  the hardware.</li>
</ul>

<h2 id="automotive-device-types">Automotive device types</h2>

<p>
The following device types are relevant for automotive platforms:
</p>

<table>
<thead>
<tr>
<th>Device type</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>AUDIO_DEVICE_OUT_BUS</td>
<td>Primary output from Android (this is how all audio from Android is
delivered to the vehicle). Used as the address for disambiguating streams
for each Context.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_OUT_TELEPHONY_TX</td>
<td>Used for audio routed to the cellular radio for transmission.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_IN_BUS</td>
<td>Used for inputs not otherwise classified.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_IN_FM_TUNER</td>
<td>Used only for broadcast radio input.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_IN_TV_TUNER</td>
<td>May be used for a TV device if present.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_IN_LINE</td>
<td>Used for AUX input jack.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_IN_BLUETOOTH_A2DP</td>
<td>Music received over Bluetooth.</td>
</tr>
<tr>
<td>AUDIO_DEVICE_IN_TELEPHONY_RX</td>
<td>Used for audio received from the cellular radio associated with a phone
call.</td>
</tr>
</tbody>
</table>

<h2 id="route-audio-sources">Routing audio sources</h2>

<p>
  Most audio sources should be captured using <code>AudioRecord</code> or a
  related Android mechanism. The data can then be assigned
  <a href="/devices/audio/attributes">AudioAttributes</a> and played through
  <code>AndroidTrack</code> either by relying on the default Android routing
  logic or by explicitly calling <code>setPreferredDevice()</code> on the
  <code>AudioRecord</code> and/or <code>AudioTrack</code> objects.
<p>

<p>
  For sources with dedicated hardware connections to the external mixer or
  with extremely tight latency requirements, you can use
  <code>createAudioPatch()</code> and <code>releaseAudioPatch()</code> to
  activate and deactivate routes between external devices (without involving
  <code>AudioFlinger</code> in the transport of samples).
</p>

<h2 id="configure-audio-devices">Configuring audio devices</h2>

<p>
  Audio devices visible to Android must be defined in
  <code>system/etc/audio_policy_configuration.xml</code>, which includes the
  following components:
</p>

<ul>
  <li><strong>module name</strong>. Supports "primary" (used for automotive
  use cases), "A2DP", "remote_submix", and "USB". The module name and the
  corresponding audio driver should be compiled to
  <code>audio.primary.$(variant).so</code>.</li>
  <li><strong>devicePorts</strong>. Contains a list of device descriptors for
  all input and output devices (includes permanently attached devices and
  removable devices) that are accessible from this module.
  <ul>
    <li>For each output device, you can define gain control that consists of
      min/value/step/default values in millibel (1 millibel = 1/100 dB = 1/1000
      bel).</li>
      <li>The address attribute on a <code>devicePort</code> can be used to find
      the device, even if there are multiple devices with the same device type
      as <code>AUDIO_DEVICE_OUT_BUS</code>.</li>
  </ul>
  <li><strong>mixPorts</strong>. Contains a list of all output and input streams
  exposed by the audio HAL. Each <code>mixPort</code> can be considered as a
  physical stream to Android <code>AudioService</code>.</li>
  <li><strong>routes</strong>. Defines a list of possible connections between
  input and output devices or between stream and device.</li>
</ul>

<p>
  The following example defines an output device <code>bus0_phone_out</code> in
  which all Android audio streams are mixed by
  <code>mixer_bus0_phone_out</code>. The route takes output stream of
  <code>mixer_bus0_phone_out</code> to <code>device bus0_phone_out</code>.
</p>

<pre class="prettyprint">
&lt;audioPolicyConfiguration version="1.0" xmlns:xi="http://www.w3.org/2001/XInclude"&gt;
    &lt;modules&gt;
        &lt;module name="primary" halVersion="3.0"&gt;
            &lt;attachedDevices&gt;
                &lt;item&gt;bus0_phone_out&lt;/item&gt;
&lt;defaultOutputDevice&gt;bus0_phone_out&lt;/defaultOutputDevice&gt;
            &lt;mixPorts&gt;
                &lt;mixPort name="mixport_bus0_phone_out" 
                         role="source"
                         flags="AUDIO_OUTPUT_FLAG_PRIMARY"&gt;
                    &lt;profile name="" format="AUDIO_FORMAT_PCM_16_BIT"
                            samplingRates="48000"
                            channelMasks="AUDIO_CHANNEL_OUT_STEREO"/&gt;
                &lt;/mixPort&gt;
            &lt;/mixPorts&gt;
            &lt;devicePorts&gt;
                &lt;devicePort tagName="bus0_phone_out" 
                            role="sink"
                            type="AUDIO_DEVICE_OUT_BUS"
                            address="BUS00_PHONE"&gt;
                    &lt;profile name="" format="AUDIO_FORMAT_PCM_16_BIT"
                            samplingRates="48000"
                            channelMasks="AUDIO_CHANNEL_OUT_STEREO"/&gt;
                    &lt;gains&gt;
                        &lt;gain name="" mode="AUDIO_GAIN_MODE_JOINT"
                                minValueMB="-8400" 
                                maxValueMB="4000" 
                                defaultValueMB="0" 
                                stepValueMB="100"/&gt;
                    &lt;/gains&gt;
                &lt;/devicePort&gt;
            &lt;/devicePorts&gt;
            &lt;routes&gt;
                &lt;route type="mix" sink="bus0_phone_out"
                       sources="mixport_bus0_phone_out"/&gt;
            &lt;/routes&gt;
        &lt;/module&gt;
    &lt;/modules&gt;
&lt;/audioPolicyConfiguration&gt;
</pre>

<h2 id="device-ports">Specifying devicePorts</h2>

<p>
  Automotive platforms should specify a <code>devicePort</code> for each
  physical stream that is input to and output from Android. For output,
  <code>devicePorts</code> should be of type <code>AUDIO_DEVICE_OUT_BUS</code>,
  and addressed by integers (i.e., Bus 0, Bus 1, etc.). <code>mixPort</code>s
  should be created in 1:1 relation to the <code>devicePorts</code> and should
  allow specification of the data formats that can be routed to each bus.
</p>

<p>
  Automotive implementations can use multiple input device types, including
  <code>FM_TUNER</code> (reserved for broadcast radio input), <code>MIC</code>
  device for handling microphone input, and <code>TYPE_AUX_LINE</code> for
  representing analog line input. All other input streams are assigned to the
  <code>AUDIO_DEVICE_IN_BUS</code> and discovered by enumerating the devices via
  a <code>AudioManager.getDeviceList()</code> call. Individual sources can be
  differentiated by their <code>AudioDeviceInfo.getProductName()</code>.
  <p>

<p>
  You can also define external devices as ports, then use those ports to
  interact with external hardware with the
  <code>IDevice::createAudioPatch</code> method of the Audio HAL (exposed via a
    new <code>CarAudioManager</code> entry point).
</p>

<p>
  When the BUS-based audio driver is present, you must set the
  <code>audioUseDynamicRouting</code> flag to <code>true</code>:
</p>

<pre class="prettyprint">
&lt;resources&gt;
    &lt;bool name="audioUseDynamicRouting"&gt;true&lt;/bool&gt;
&lt;/resources&gt;
</pre>

<p>
  For details, refer to
  <code>device/generic/car/emulator/audio/overlay/packages/services/Car/service/res/values/config.xml</code>.
</p>

</body>
</html>