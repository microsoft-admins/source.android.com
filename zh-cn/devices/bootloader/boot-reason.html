<html devsite><head>
    <title>规范化启动原因</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>

  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
Android 9 对引导加载程序启动原因规范进行了以下更改。
</p>

<h2 id="about-boot-reasons">关于启动原因</h2>

<p>
引导加载程序使用专用的硬件和内存资源来确定设备重新启动的原因，然后将 <code>androidboot.bootreason=&lt;reason&gt;</code> 添加到用于启动设备的 Android 内核命令行中，以传达这一决定。然后，<code>init</code> 会转换此命令行，使其传播到 Android 属性 <code>bootloader_boot_reason_prop</code> (<code>ro.boot.bootreason</code>) 中。
</p>

<h2 id="about-boot-reason-specifications">关于启动原因规范</h2>

<p>
之前的 Android 版本中指定的启动原因格式如下：不使用空格，全部为小写字母，只有非常少的要求（例如报告 <code>kernel_panic</code>、<code>watchdog</code>、<code>cold</code>/<code>warm</code>/<code>hard</code>），并且允许其他特殊原因。这种宽松的规范导致出现了成百上千个自定义启动原因字符串（有时毫无意义），进而造成了无法管理的情况。到目前最新的 Android 版本发布之前，引导加载程序提交的近乎无法解析或毫无意义的内容急剧增加已经为 <code>bootloader_boot_reason_prop</code> 造成了合规性问题。
</p>

<p>
在开发 Android 9 版本中，Android 团队发现旧的 <code>bootloader_boot_reason_prop</code> 中内容会急剧增加，并且无法在系统运行时重写。因此，要对启动原因规范进行任何改进，都必须与引导加载程序开发者进行互动交流，并对现有系统进行调整。为此，Android 团队采取了以下措施：
</p>

<ul>
  <li>与引导加载程序开发者互动交流，鼓励他们：
  <ul>
    <li>向 <code>bootloader_boot_reason_prop</code> 提供规范、可解析且可识别的原因。</li>
    <li>向 <code>system/core/bootstat/bootstat.cpp</code> <code>kBootReasonMap</code> 列表添加内容。</li>
    </ul>
  </li>
  <li>添加受控且可在系统运行时重写的 <code>system_boot_reason_prop</code> (<code>sys.boot.reason</code>) 源代码。只有少量的系统应用（如 <code>bootstat</code> 和 <code>init</code>）可重写此属性，不过，所有应用都可以通过获得 sepolicy 权限来读取它。</li>
  <li>将启动原因告知用户，让他们等到用户数据装载完毕后再信任系统启动原因属性 <code>system_boot_reason_prop</code> 中的内容。</li>
</ul>

<p>
为什么要等这么久？虽然 <code>bootloader_boot_reason_prop</code> 在启动过程的早期阶段就已可用，但 Android 安全政策根据需要对其进行了屏蔽，因为它表示不准确、不可解析且不合规范的信息。大多数情况下，只有对启动系统有深入了解的开发者才需要访问这些信息。只有在用户数据装载完毕<strong>之后</strong>，才可以通过 <code>system_boot_reason_prop</code> 准确可靠地提取经过优化、可解析且合乎规范的启动原因 API。具体而言：
</p>

  <ul>
    <li>在用户数据装载完毕<strong>之前</strong>，<code>system_boot_reason_prop</code> 将包含 <code>bootloader_boot_reasoon_prop</code> 中的值。</li>
    <li>在用户数据装载完毕<strong>之后</strong>，可以更新 <code>system_boot_reason_prop</code>，以使其符合要求或报告更准确的信息。</li>
  </ul>

<p>
出于上述原因，Android 9 延长了可以正式获取启动原因之前需要等待的时间段，将其从启动时立即准确无误的状态（使用 <code>bootloader_boot_reason_prop</code>）更改为仅在用户数据装载完毕之后才可用（使用 <code>system_boot_reason_prop</code>）。
</p>

<p>
Bootstat 逻辑依赖于信息更丰富且合规的 <code>bootloader_boot_reason_prop</code>。当该属性使用可预测的格式时，能够提高所有受控重新启动和关机情况的准确性，从而优化和扩展 <code>system_boot_reason_prop</code> 的准确性和含义。
</p>

<h2 id="canonical-boot-reason-format">规范化启动原因格式</h2>

<p>
在 Android 9 中，<code>bootloader_boot_reason_prop</code> 的规范化启动原因格式使用以下语法：
</p>

<pre class="prettyprint">&lt;reason&gt;,&lt;subreason&gt;,&lt;detail&gt;…</pre>

<p>
格式设置规则如下：
</p>

<ul>
  <li>小写</li>
  <li>无空格（可使用下划线）</li>
  <li>全部为可打印字符</li>
  <li>以英文逗号分隔的 <code>reason</code>、<code>subreason</code>，以及一个或多个 <code>detail</code>。
  <ul>
    <li>必需的 <code>reason</code>，表示设备为什么必须重新启动或关机且优先级最高的原因。</li>
    <li>选用的 <code>subreason</code>，表示设备为什么必须重新启动或关机的简短摘要（或重新启动设备/将设备关机的人员）。</li>
    <li>一个或多个选用的 <code>detail</code> 值。<code>detail</code> 可以指向某个子系统，以协助确定是哪个具体系统导致了 <code>subreason</code>。您可以指定多个 <code>detail</code> 值，这些值通常应按照重要程度排序。不过，也可以报告多个具有同等重要性的 <code>detail</code> 值。</li>
  </ul>
  </li>
</ul>

<p>
如果 <code>bootloader_boot_reason_prop</code> 为空值，则会被视为非法（因为这会允许其他代理在事后添加启动原因）。
</p>

<h3 id="reason-requirements">原因要求</h3>

<p>
为 <code>reason</code>（第一个跨度，位于终止符或英文逗号之前）指定的值必须是以下集合（分为内核原因、强原因和弱原因）之一：
</p>

<ul>
  <li>内核集：
  <ul>
    <li>"<code>watchdog"</code></li>
    <li><code>"kernel_panic"</code></li>
  </ul>
  </li>
  <li>强集：
  <ul>
    <li><code>"recovery"</code></li>
    <li><code>"bootloader"</code></li>
  </ul>
  </li>
  <li>弱集：
  <ul>
    <li><code>"cold"</code>：通常表示完全重置所有设备，包括内存。</li>
    <li><code>"hard"</code>：通常表示硬件重置了状态，并且 <code>ramoops</code> 应保留持久性内容。</li>
    <li><code>"warm"</code>：通常表示内存和设备保持某种状态，并且 <code>ramoops</code>（请参阅内核中的 <code>pstore</code> 驱动程序）后备存储空间包含持久性内容。</li>
    <li><code>"shutdown"</code></li>
    <li><code>"reboot"</code>：通常意味着 <code>ramoops</code> 状态和硬件状态未知。该值是与 <code>cold</code>、<code>hard</code> 和 <code>warm</code> 一样的通用值，可提供关于设备重置深度的提示。</li>
  </ul>
  </li>
</ul>

<p>
引导加载程序必须提供内核集或弱集 <code>reason</code>，强烈建议引导加载程序提供 <code>subreason</code>（如果可以确定的话）。例如，电源键长按（无论是否有 <code>ramoops</code> 备份）的启动原因为 <code>"reboot,longkey"</code>。
</p>

<p>
第一个跨度 <code>reason</code> 不能是任何 <code>subreason</code> 或 <code>detail</code> 的组成部分。不过，由于用户空间无法产生内核集原因，因此可能会在弱集原因之后重复使用 <code>"watchdog"</code> 以及源代码的详细信息（例如 <code>"reboot,watchdog,service_manager_unresponsive"</code> 或 <code>"reboot,software,watchdog"</code>）。
</p>

<p>
启动原因应该无需专家级内部知识即可解读，并且（或者）应该能让人看懂并提供直观报告。示例：<code>"shutdown,vbxd"</code>（糟糕）、<code>"shutdown,uv"</code>（较好）、<code>"shutdown,undervoltage"</code>（首选）。
</p>

<h3 id="reason-subreason-combinations">“原因-子原因”组合</h3>

<p>
Android 保留了一组 <code>reason</code>-<code>subreason</code> 组合，在正常使用情况下不应过量使用这些组合；不过，如果组合能准确反映相关状况，则可根据具体情况加以使用。保留组合的示例包括：
</p>

<ul>
  <li><code>"reboot,userrequested"</code></li>
  <li><code>"shutdown,userrequested"</code></li>
  <li><code>"Shutdown,thermal"</code>（来自 <code>thermald</code>）</li>
  <li><code>"shutdown,battery"</code></li>
  <li><code>"Shutdown,battery,thermal"</code>（来自 <code>BatteryStatsService</code>）</li>
  <li><code>"reboot,adb"</code></li>
  <li><code>"reboot,shell"</code></li>
  <li><code>"reboot,bootloader"</code></li>
  <li><code>"reboot,recovery"</code></li>
</ul>

<p>
如需更多详细信息，请参阅 <code>system/core/bootstat/bootstat.cpp</code> 中的 <code>kBootReasonMap</code> 以及 Android 源代码库中的关联 git 变更日志记录。
</p>

<h2 id="reporting-boot-reasons">报告启动原因</h2>

<p>
所有启动原因（无论是来自引导加载程序还是记录在规范化启动原因中）都必须记录在 <code>system/core/bootstat/bootstat.cpp</code> 的 <code>kBootReasonMap</code> 部分中。<code>kBootReasonMap</code> 列表包含各种合规原因和不合规的旧版原因。引导加载程序开发者应在此处仅登记新的合规原因（除非产品已发货且无法更改，否则不应登记不合规的原因）。
</p>

<aside class="note">
  <strong>注意</strong>：虽然 <code>system/core/bootstat/bootstat.cpp</code> 包含一个 <code>kBootReasonMap</code> 部分，并且其中列出了大量旧版原因，但这些原因的存在并不意味着 <code>reason</code> 字符串已获准使用。该列表的一个子集内列出了合规原因；随着引导加载程序开发者不断登记更多合规原因并加以说明，这个子集预计将不断增大。
</aside>

<p>
强烈建议使用 <code>system/core/bootstat/bootstat.cpp</code> 中现有的合规条目，如果要使用不合规字符串，要先对其加以限制。请参阅以下指导原则：
</p>

<ul>
  <li><strong>允许</strong>从引导加载程序报告 <code>"kernel_panic"</code>，因为 <code>bootstat</code> 可以检查 <code>ramoops</code> 的 <code>kernel_panic signatures</code>，以便将子原因细化为规范化的 <code>system_boot_reason_prop</code>。</li>
  <li><strong>不允许</strong>从引导加载程序以 <code>kBootReasonMap</code>（如 <code>"panic")</code>）的形式报告不合规的字符串，因为这最终将导致无法细化 <code>reason</code>。</li>
</ul>

<p>
例如，如果 <code>kBootReasonMap</code> 包含 <code>"wdog_bark"</code>，则引导加载程序开发者应采取以下措施：
</p>

<ul>
  <li>更改为 <code>"watchdog,bark"</code>，并将其添加到 <code>kBootReasonMap</code> 中的列表内。</li>
  <li>考虑 <code>"bark"</code> 对于不熟悉该技术的人来说意味着什么，并确定是否存在更有意义的 <code>subreason</code>。</li>
</ul>

<h2 id="verifying-boot-reason-compliance">验证启动原因合规性</h2>

<p>
目前，对于引导加载程序可能提供的所有启动原因，Android 没有提供能够准确触发或检查这些原因的主动 CTS 测试；合作伙伴仍然可以尝试运行被动测试来确定兼容性。
</p>

<p>
因此，要实现引导加载程序合规性，引导加载程序开发者需要自愿遵循上述规则和准则的精神。我们会敦促此类开发者为 AOSP（特别是 <code>system/core/bootstat/bootstat.cpp</code>）做贡献，并将这个机会作为一个讨论启动原因问题的论坛。
</p>

</body></html>