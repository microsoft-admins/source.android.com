<html devsite><head>
    <title>Linhas de código, branches e versões</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  O Android Open Source Project (AOSP) mantém uma pilha de software completa para ser transferida por OEMs e outros implementadores de dispositivos e executada no hardware deles.
  Para manter a qualidade do Android, o Google colaborou com engenheiros, gerentes de produtos, designers de interface do usuário e testadores controle de qualidade em tempo integral, bem como com todas as outras funções necessárias para levar dispositivos modernos ao mercado.
</p>

<p>
  Dessa forma, mantemos diversas linhas de código para separar claramente a versão estável atual do Android da experimental instável. Implementamos a administração de código aberto e a manutenção das linhas de código do Android no ciclo de desenvolvimento de produtos maior.
</p>

<h2 id="aosp-management">Gerenciamento de códigos do AOSP</h2>
<p>
  O gráfico abaixo mostra os conceitos por trás do gerenciamento de códigos e versões do AOSP.
</p>

<img src="/images/code-lines.png" alt="diagrama de linhas de código" id="figure1"/>
<figcaption><strong>Figura 1.</strong> Código e versões do AOSP</figcaption>

<ol>
  <li>
	  A todo momento há uma versão mais recente da plataforma Android. Isso geralmente assume a forma de um branch na árvore.
  </li>
  <li>
	  Os criadores e colaboradores de dispositivos trabalham com a versão mais recente atual, corrigindo bugs, lançando novos dispositivos, testando novos recursos etc.
  </li>
  <li>
	  Ao mesmo tempo, o Google trabalha internamente na próxima versão da plataforma e estrutura Android, de acordo com as necessidades e os objetivos do produto. Desenvolvemos a próxima versão do Android trabalhando com um parceiro de dispositivo em um modelo principal cujas especificações são escolhidas para impelir o Android na direção que acreditamos que ele deve seguir.
	</li>
  <li>
	  Quando a versão n+1 estiver pronta, ela será publicada na árvore de origem pública e se tornará a nova versão mais recente.
  </li>
</ol>

<aside class="note"><strong>Observação</strong>: usamos o termo <em>linhas de código</em> em vez de <em>branches</em> simplesmente porque a qualquer momento pode haver mais de um branch para uma determinada linha de código. Por exemplo, quando uma versão é finalizada, ela pode ou não se tornar um novo branch, dependendo das necessidades atuais.
</aside>

<h2 id="terms-and-caveats">Termos e advertências</h2>

<ul>
  <li>
	  Uma <em>versão</em> corresponde a uma versão formal da plataforma Android, como 1.5, 2.1 e assim por diante. Uma versão da plataforma corresponde à versão no campo <code>SdkVersion</code> dos arquivos <code>AndroidManifest.xml</code> e é definida dentro de <code>frameworks/base/api</code> na árvore de origem.
  </li>
  <li>
	  Um projeto <em>ascendente</em> é um projeto de código aberto a partir do qual a pilha do Android coleta código. Além de projetos como o kernel do Linux e o WebKit, continuamos a migrar alguns projetos Android semiautônomos, como o ART, as ferramentas do Android SDK, o Bionic e assim por diante, para funcionar como projetos ascendentes. Geralmente, esses projetos são desenvolvidos inteiramente na árvore pública. Para alguns projetos ascendentes, o desenvolvimento ocorre com colaboração direta para o próprio projeto ascendente. Para ver mais detalhes, consulte <a href="../contribute/submit-patches.html#upstream-projects">Projetos ascendentes</a>. Em ambos os casos, os instantâneos são periodicamente usados nas versões.
  </li>
  <li>
	  Uma linha de código de versão (que pode, na verdade, consistir em mais de um branch real no git) sempre é considerada o único código-fonte canônico para uma determinada versão da plataforma Android. OEMs e outros grupos que criam dispositivos precisam se basear apenas em um branch de versão.
  </li>
  <li>
	  Linhas de código experimentais são estabelecidas para capturar alterações da comunidade, para que possam ser iteradas visando a estabilidade.
  </li>
  <li>
	  As alterações que se mostrarem estáveis poderão ser usadas em um branch da versão.
    Isso se aplica somente a correções de bugs, melhorias de aplicativo e outras alterações que não afetam as APIs da plataforma.
  </li>
  <li>
	  As alterações são usadas nos branches da versão de projetos ascendentes (incluindo os projetos ascendentes do Android), conforme a necessidade.
  </li>
  <li>
	  A versão n+1 (a próxima versão principal das APIs da plataforma e da estrutura) é desenvolvida internamente pelo Google. Para ver mais detalhes, consulte <a href="#private-codelines">Linhas de código privadas</a>.
  </li>
  <li>
	  As alterações são extraídas de branches experimentais, ascendentes e de versão e levadas para o branch privado do Google conforme a necessidade.
  </li>
  <li>
	  Quando as APIs da plataforma para a próxima versão estiverem estabilizadas e totalmente testadas, o Google finaliza um lançamento da próxima versão da plataforma (especificamente, uma nova <code>SdkVersion</code>). Isso corresponde à linha de código interna que está se tornando um branch da versão pública e à nova linha de código da plataforma atual.
  </li>
  <li>
	  Quando uma nova versão da plataforma é finalizada, uma linha de código experimental correspondente é criada ao mesmo tempo.
  </li>
</ul>

<h2 id="private-codelines">Linhas de código privadas</h2>
<p>
  A estratégia de gerenciamento de origem acima inclui uma linha de código que o Google mantém privada para concentrar a atenção na versão pública atual do Android.
</p>
<p>
  Os OEMs e outros fabricantes de dispositivos querem, naturalmente, fornecer aparelhos com a versão mais recente do Android. Da mesma forma, os desenvolvedores de apps não querem lidar com mais versões de plataforma do que o estritamente necessário. Enquanto isso, o Google mantém a responsabilidade pela direção estratégica do Android como plataforma e produto. Nossa abordagem se concentra em um pequeno número de dispositivos principais para impulsionar recursos e, ao mesmo tempo, garantir as proteções de propriedade intelectual relacionadas ao Android.
</p>
<p>
  Como resultado, o Google frequentemente possui informações confidenciais de terceiros e precisa evitar revelar recursos confidenciais até ter as proteções adequadas. Além disso, existem riscos reais para a plataforma decorrentes do excesso de versões de plataformas existentes ao mesmo tempo. Por esses motivos, estruturamos o projeto de código aberto (incluindo contribuições de terceiros) para que se concentre na versão estável do Android que está pública no momento. O desenvolvimento profundo da próxima versão da plataforma ocorre de forma privada até que ela esteja pronta para se tornar uma versão oficial.
</p>
<p>
  Sabemos que muitos colaboradores discordam dessa abordagem e respeitamos os diferentes pontos de vista. No entanto, essa é a abordagem que consideramos ser a melhor e a que escolhemos implementar para o Android.
</p>

</body></html>