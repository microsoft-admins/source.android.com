<html devsite><head>
    <title>Preparo para a criação</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>As instruções a seguir para criar a árvore de origem do Android se aplicam a todos branches, incluindo o <code>master</code>. A sequência básica dos comandos de criação é a seguinte.</p>

<h2 id="obtaining-proprietary-binaries">Adquirir binários reservados</h2>

<p>O AOSP não pode ser usado apenas a partir do código-fonte puro e requer que outras bibliotecas reservadas relacionadas a hardware sejam executadas, como para aceleração de gráficos de hardware. Consulte as seções abaixo para ver os links de download e <a href="requirements.html#binaries">Binários do dispositivo</a> para ver outros recursos.</p>

<aside class="note">Alguns dispositivos têm esses binários reservados na partição <code>/vendor</code> deles.</aside>

<h3 id="downloading-proprietary-binaries">Fazer o download de binários reservados</h3>

<p>Você pode fazer o download de binários oficiais para os dispositivos compatíveis que executam branches de versões AOSP marcadas <a href="https://developers.google.com/android/drivers" class="external">dos drivers do Google</a>. Esses binários acrescentam acesso a outras funcionalidades de hardware com código-fonte não aberto. Para criar o branch master do AOSP, use a <a href="https://developers.google.com/android/blobs-preview" class="external">Visualização de binários</a>. Ao criar o branch master de um dispositivo, use os binários para a <a href="/setup/start/build-numbers.html">versão numerada mais recente</a> ou com a data mais recente.</p>

<h3 id="extracting-proprietary-binaries">Extrair binários reservados</h3>

<p>Cada conjunto de binários é fornecido como um script de autoextração em um arquivo compactado. Descompacte cada arquivo, execute o script de autoextração incluído na raiz da árvore de origem e confirme que você concorda com os termos do contrato de licença. Os binários e seus makefiles correspondentes serão instalados na hierarquia <code>vendor/</code> da árvore de origem.</p>

<h3 id="cleaning-up">Limpeza</h3>

<p>Para garantir que os binários recém-instalados sejam devidamente considerados após serem extraídos, exclua a saída existente de qualquer versão anterior usando:</p>
<pre class="devsite-terminal devsite-click-to-copy">
make clobber
</pre>

<h2 id="initialize">Configurar o ambiente</h2>
<p>Inicialize o ambiente com o script <code>envsetup.sh</code>. Observe que a substituição de <code>source</code> por <code>.</code> (um único ponto) economiza alguns caracteres, e o formato curto é mais comumente usado na documentação.</p>
<pre class="devsite-terminal devsite-click-to-copy">
source build/envsetup.sh
</pre>
<p>ou</p>
<pre class="devsite-terminal devsite-click-to-copy">
. build/envsetup.sh
</pre>

<h2 id="choose-a-target">Escolher um destino</h2>
<p>Escolha qual destino criar com o <code>lunch</code>. A configuração exata pode ser passada como um argumento. Por exemplo, o comando a seguir se refere a uma versão completa para o emulador, com toda a depuração ativada:</p>
<pre class="devsite-terminal devsite-click-to-copy">
lunch aosp_arm-eng
</pre>
<p>Se executado sem argumentos, o <code>lunch</code> solicitará que você escolha um destino no menu.</p>
<p>Todos os destinos de criação assumem o formato <code>BUILD-BUILDTYPE</code>, onde <code>BUILD</code> é um codinome que se refere à combinação específica de recursos. O <code>BUILDTYPE</code> é um dos seguintes:</p>
<table>
  <thead>
    <tr>
      <th>Tipo da versão</th>
      <th>Uso</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>user</td>
      <td>acesso limitado; adequado para produção</td>
    </tr>
    <tr>
      <td>userdebug</td>
      <td>como user, mas com acesso root e capacidade de depuração; o mais indicado para depuração</td>
    </tr>
    <tr>
      <td>eng</td>
      <td>configuração de desenvolvimento com outras ferramentas de depuração</td>
    </tr>
  </tbody>
</table>

<p>A versão userdebug precisa se comportar da mesma forma que a user, com a capacidade de ativar depuração adicional, que normalmente viola o modelo de segurança da plataforma. Isso faz com que userdebug seja uma boa versão para testes de usuários com maiores recursos de diagnóstico. Ao desenvolver com a versão userdebug, siga as <a href="../develop/new-device.html#userdebug-guidelines">diretrizes de userdebug</a>.</p>

<p>A versão eng prioriza a produtividade de engenharia para engenheiros que trabalham na plataforma. Ela desativa várias otimizações usadas para fornecer uma boa experiência ao usuário. Fora isso, a versão eng se comporta de maneira semelhante às versões user e userdebug, para que os desenvolvedores de dispositivos possam ver como o código se comporta nesses ambientes.</p>

<p>Para ver mais informações sobre como criar e executar em hardwares reais, consulte <a href="running.html">Como executar versões</a>.</p>

<h2 id="build-the-code">Criar o código</h2>

<p>Esta seção é meramente um resumo para garantir que a configuração esteja concluída. Consulte <a href="running.html">Como executar versões</a> para ver instruções detalhadas sobre a criação do Android.</p>

    <p>Crie tudo com <code>make</code>. O GNU <code>make</code> pode gerenciar tarefas paralelas com um argumento <code>-jN</code>, e é comum usar várias tarefas N, que são entre uma e duas vezes o número de threads de hardware no computador que está sendo usado para a criação. Por exemplo, em um computador dual-E5520 (2 CPUs, 4 núcleos por CPU, 2 threads por núcleo), as criações mais rápidas são feitas com comandos entre <code>make -j16</code> e <code>make -j32</code>.</p>

<pre class="devsite-terminal devsite-click-to-copy">
make -j4
</pre>

<h2 id="run-it">Execução</h2>

<p>Você pode executar sua versão em um emulador ou armazená-la em flash em um dispositivo. Observe que você já selecionou o destino da versão com <code>lunch</code>, e é improvável que ela seja executada em um destino diferente daquele para o que foi criada.</p>

<aside class="note"><strong>Observação</strong>: lembre-se de <a href="#obtaining-proprietary-binaries">adquirir os binários reservados</a> ou sua versão não será inicializada com sucesso no hardware de destino. Se adquirir os blobs binários, você precisará descompactá-los, <code>make clobber</code> e recriar.</aside>

<h3 id="flash-a-device">Atualização flash com fastboot</h3>

<p>Para realizar a atualização flash de um dispositivo, você precisará usar o <code>fastboot</code>, que precisa ser incluído no seu caminho após uma criação bem-sucedida. Consulte <a href="running.html#flashing-a-device">Como realizar uma atualização flash em um dispositivo</a> para ver mais instruções.</p>

<h3 id="emulate-an-android-device">Emular um dispositivo Android</h3>

<p>O emulador é adicionado ao seu caminho automaticamente pelo processo de criação. Para executá-lo, digite:</p>

<pre class="devsite-terminal devsite-click-to-copy">
emulator
</pre>

<h2 id="troubleshooting-common-build-errors">Como solucionar erros de criação comuns</h2>

<h3 id="wrong-java-version">Versão incorreta do Java</h3>

<p>Se você está tentando criar uma versão do Android inconsistente com sua versão do Java, <code>make</code> será cancelado com uma mensagem como</p>
<pre>
************************************************************
You are attempting to build with the incorrect version
of java.

Your version is: WRONG_VERSION.
The correct version is: RIGHT_VERSION.

Please follow the machine setup instructions at
    https://source.android.com/source/initializing.html
************************************************************
</pre>

<p>Estas são as prováveis causas e soluções:</p>

<ul>
<li>Falha ao instalar o JDK correto conforme especificado nos <a href="requirements.html#jdk">Requisitos do JDK</a>. Verifique se você seguiu as etapas apresentadas em <a href="building.html#initialize">Configurar o ambiente</a> e <a href="building.html#choose-a-target">Escolher um destino</a>.</li>
<li>Outro JDK instalado anteriormente aparece no seu caminho. Inclua o JDK correto no início do seu caminho ou remova o JDK problemático.</li>
</ul>

<h3 id="python-version-3">Versão 3 do Python</h3>

<p>O repo foi projetado com uma funcionalidade específica do Python 2.x e, infelizmente, é incompatível com o Python 3. Para usar o repo, instale o Python 2.x:</p>

<pre class="devsite-terminal devsite-click-to-copy">
apt-get install python
</pre>

<h3 id="case-insensitive-filesystem">Sistema de arquivos indiferente a maiúsculas</h3>

<p>Se você estiver criando em um sistema de arquivos HFS no Mac OS, poderá encontrar um erro como</p>
<pre>
************************************************************
You are building on a case-insensitive filesystem.
Please move your source tree to a case-sensitive filesystem.
************************************************************
</pre>
<p>Siga as instruções apresentadas em <a href="initializing.html#creating-a-case-sensitive-disk-image">Como criar uma imagem de disco com diferenciação de maiúsculas e minúsculas</a>.</p>

<h3 id="no-usb-permission">Sem permissão para USB</h3>

<p>Na maioria dos sistemas Linux, por padrão, os usuários não privilegiados não podem acessar as portas USB. Se você encontrar um erro de permissão negada, siga as instruções em <a href="initializing.html#configuring-usb-access">Como configurar o acesso por USB</a>.</p>

<p>Se o adb já estiver em execução e não for possível se conectar ao dispositivo depois definir as regras, ele poderá ser eliminado com <code>adb kill-server</code>.
Isso fará com que o adb seja reiniciado com a nova configuração.</p>

</body></html>