<html devsite><head>
    <title>Imagem genérica do sistema (GSI)</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>

  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  Uma imagem genérica do sistema (GSI, na sigla em inglês) é uma imagem do sistema com configurações ajustadas para dispositivos Android. Ela é considerada uma implementação de "Android puro" com código do Android Open Source Project (AOSP) não modificado que qualquer dispositivo com Android 8.1 ou versões posteriores pode executar.
</p>

<p>
  A conformidade com a GSI faz parte do <a href="/compatibility/overview">Programa de compatibilidade do Android</a>. A imagem do sistema de um dispositivo Android é substituída por uma GSI e testada com o <a href="/compatibility/vts/">Teste de fornecedor (VTS, na sigla em inglês)</a> e o <a href="/compatibility/cts/">Teste de compatibilidade (CTS, na sigla em inglês)</a> para garantir que o dispositivo implemente interfaces do fornecedor corretamente com a versão mais recente do Android.
</p>

<aside class="note"><strong>Observação</strong>: este artigo descreve tópicos relacionados à GSI para desenvolvedores de ROM e OEM do Android. Os desenvolvedores de apps Android precisam consultar <a href="https://developer.android.com/topic/generic-system-image/">developer.android.com</a> para ver os detalhes de GSI voltados para eles.
</aside>

<p>
  Para começar a usar GSIs, leia as seções a seguir para ver mais detalhes sobre as <a href="#gsi-configuration-and-variances">configurações de GSI</a> (e as variações permitidas), os <a href="#gsi-types">tipos</a> (GSI Android e GSI legada) e os <a href="#vendor-binaries-and-vndk-dependencies">binários do fornecedor e dependências de VNDK</a>. Quando você estiver pronto para usar uma GSI, <a href="#building-gsis">faça o download e crie a GSI</a> para seu dispositivo de destino e, em seguida, <a href="#flashing-gsis">atualize a GSI com flash</a> para um dispositivo Android.
</p>

<h2 id="gsi-configuration-and-variances">Configuração e variações de GSI</h2>

<p>
  A GSI atual tem a seguinte configuração:
</p>

<ul>
  <li><strong>Treble.</strong> Uma GSI inclui compatibilidade total com as <a href="/devices/architecture/#hidl">alterações de arquitetura baseadas em HIDL</a> (também conhecidas como "Treble") introduzidas no Android 8.0, incluindo compatibilidade com as <a href="/reference/hidl/">interfaces HIDL</a>. Você pode usar uma GSI em qualquer dispositivo Android que use interfaces de fornecedores HIDL. Para ver mais detalhes, consulte <a href="/devices/architecture/#resources">Recursos de arquitetura</a>.</li>
  <li><strong>Verificação de inicialização.</strong> Uma GSI não inclui uma solução de verificação de inicialização (<a href="/security/verifiedboot/">vboot 1.0</a>, <a href="/security/verifiedboot/avb">AVB</a> etc.). Para atualizar com flash uma GSI em um dispositivo Android, será preciso ter um método para desabilitar a verificação de inicialização.</li>
  <li><strong>Variante de versão.</strong> Uma GSI sempre usa uma variante de versão <code>userdebug</code> para possibilitar a execução de VTS e CTS. Depois de substituir a imagem do sistema pela GSI, você pode ativar o acesso root no dispositivo e testar com uma imagem do fornecedor de versão <code>user</code> e uma imagem do sistema de criação <code>userdebug</code>.</li>
  <li><strong>Sistema de arquivos e formato da imagem</strong>. Uma GSI usa um sistema de arquivos ext4 com formato de imagem esparsa.</li>
</ul>

<p>
  A GSI atual inclui as seguintes variações principais:
</p>

<ul>
  <li><strong>Versão</strong>. Compatível com Android 8.0, Android 8.1 e Android 9.</li>
  <li><strong>Arquitetura da CPU</strong>. Compatível com diferentes instruções de CPU (ARM, x86 etc.) e quantidades de bits de CPU (32 ou 64 bits).</li>
  <li><strong>Layout de partição</strong>. Pode usar o layout de partição de <a href="/devices/bootloader/system-as-root">sistema como raiz</a> ou sistema não usado como raiz.</li>
  <li>Compatível com a quantidade de bits da interface Binder.</li>
</ul>

<h2 id="gsi-types">Tipos de GSI</h2>

<p>
  A GSI usada para teste de conformidade é determinada pela versão do Android com que o dispositivo é lançado. O Android 9 é compatível com as seguintes GSIs:
</p>

<table>
  <tbody><tr>
   <th>Nome da GSI</th>
   <th>Descrição</th>
   <th>Nome do produto</th>
  </tr>
  <tr>
   <td>GSI Android</td>
   <td>Para dispositivos lançados com o Android 9</td>
   <td><code>aosp_$arch</code></td>
  </tr>
  <tr>
   <td>GSI legada</td>
   <td>Para dispositivos lançados com o Android 8.0 ou Android 8.1</td>
   <td><code>aosp_$arch_a(b)</code></td>
  </tr>
</tbody></table>

<p>
  Todas as GSIs são criadas a partir da base de código do Android 9.
</p>

<h3 id="changes-in-p-gsis">Alterações na GSI do Android 9</h3>

<p>
  Os dispositivos lançados com o Android 9 precisam usar as GSIs do Android 9 para testes de conformidade, o que inclui as seguintes alterações importantes das GSIs anteriores:
</p>

<ul>
  <li><strong>Combinação de GSI e emulador</strong>. As GSIs são criadas com base nas imagens do sistema de produtos emuladores, como <code>aosp_arm64</code>, <code>aosp_x86</code> etc.</li>
  <li><strong>Sistema como raiz</strong>. Nas versões anteriores do Android, os dispositivos que não eram compatíveis com atualizações A/B podiam ativar a imagem do sistema no diretório <code>/system</code>. No Android 9, a raiz da imagem do sistema é ativada como a raiz do dispositivo.</li>
    <li><strong>interface Binder de 64 bits</strong>. No Android 8.x, as GSIs de 32 bits usavam a interface Binder de 32 bits. O Android 9 não é compatível com a interface Binder de 32 bits, então GSIs de 32 e 64 bits usam a interface Binder de 64 bits.</li>
    <li><strong>Obrigatoriedade do VNDK</strong>. No Android 8.1, o VNDK era opcional. No Android 9, o VNDK é obrigatório, o que significa que <code>BOARD_VNDK_RUNTIME_DISABLE</code> <strong>não</strong> pode ser definido (<code>BOARD_VNDK_RUNTIME_DISABLE :=  # must not be set</code>).</li>
    <li><strong>Propriedade do sistema compatível</strong>. O Android 9 ativa a verificação de acesso para propriedades de sistema compatíveis (<code>PRODUCT_COMPATIBLE_PROPERTY_OVERRIDE := true</code>).</li>
</ul>

<p>
  Para testar os dispositivos que são lançados com o Android 9 com cts-on-gsi, use os <a href="#p-gsi-build-targets">destinos de criação para a GSI do Android 9</a>.
</p>

<h3 id="changes-in-legacy-gsis">Alterações da GSI legada do Android 9</h3>

<p>
  Dispositivos que passam por upgrade para o Android 9 podem usar o nome do produto da GSI legada com sufixo <code>_ab</code> ou <code>_a</code> (por exemplo <code>aosp_arm64_ab</code>, <code>aosp_x86_a</code>) para o teste de conformidade.
  Essa GSI é compatível com os seguintes casos de uso de upgrade:
</p>

<ul>
  <li>Dispositivos com uma implementação da interface de fornecedor do Android 8.1</li>
  <li>Dispositivos atualizados para a implementação da interface do fornecedor do Android 9</li>
</ul>

<p>
  As GSIs legadas são criadas com base na árvore de origem do Android 9, mas contêm as seguintes configurações retrocompatíveis para dispositivos que passaram por upgrade:
</p>

<ul>
  <li><strong>Sistema não usado como raiz</strong>. Dispositivos não compatíveis com o uso do sistema como raiz podem continuar a usar produtos <code>_a</code> (por exemplo, <code>aosp_arm_a</code>).</li>
  <li><strong>Espaço do usuário de 32 bits + interface Binder de 32 bits</strong>. As GSIs de 32 bits podem continuar usando a interface Binder de 32 bits.</li>
  <li><strong>VNDK do 8.1</strong>. Os dispositivos podem usar o VNDK do 8.1 incluso.</li>
  <li><strong>Diretórios de ativação</strong>. Alguns dispositivos legados usam diretórios como indicadores de ativação (por exemplo, <code>/bluetooth</code>, <code>/firmware/radio</code>, <code>/persist</code> etc.).</li>
</ul>

<p>
  Para testar os dispositivos que passam por upgrade para o Android 9 com cts-on-gsi, use os <a href="#legacy-gsi-build-targets">destinos de criação para a GSI legada</a>.
</p>

<aside class="note">
  <strong>Observação</strong>: se um dispositivo anterior ao Android 9 implementar a interface do fornecedor do Android 9 e atender a todos os requisitos introduzidos nessa versão, não use as GSIs legadas, mas sim as GSIs do Android 9 para VTS e cts-on-gsi.
</aside>

<h3 id="changes-to-keymaster-behavior">Alterações no Keymaster do Android 9</h3>

<p>
  Nas versões anteriores do Android, os dispositivos que implementavam o Keymaster 3 ou versões anteriores precisavam verificar se as informações da versão (<code>ro.build.version.release</code> e <code>ro.build.version.security_patch</code>) relatadas pelo sistema em execução correspondiam às informações de versão relatadas pelo carregador de inicialização. Essa informação geralmente vinha do cabeçalho da imagem de inicialização.
</p>

<p>
  No Android 9, esse requisito foi alterado para permitir que os fornecedores inicializem uma GSI. Especificamente, o Keymaster não pode mais executar a verificação, porque as informações de versão relatadas pela GSI podem não corresponder às informadas pelo carregador de inicialização do fornecedor. No caso de dispositivos que implementam o Keymaster 3 ou versões anteriores, os fornecedores precisam modificar a implementação do Keymaster para ignorar a verificação (ou fazer upgrade para o Keymaster 4). Para ver mais detalhes sobre o Keymaster, consulte <a href="/security/keystore/">Armazenamento de chaves protegido por hardware</a>.
</p>

<h2 id="vendor-binaries-and-vndk-dependencies">Binários do fornecedor e dependências do VNDK</h2>

<p>
  Os dispositivos que passam por upgrade para o Android 9 têm diferentes caminhos de upgrade, dependendo da versão dos binários do fornecedor em uso no dispositivo e das configurações relacionadas ao VNDK usadas para criar os dispositivos. A tabela a seguir resume a compatibilidade da GSI legada com dispositivos que passaram por upgrade:
</p>

<table>
  <tbody><tr>
   <th>Caso de uso</th>
   <th>Versão<br />dos binários do<br />fornecedor</th>
   <th><code>BOARD_VNDK_VERSION</code></th>
   <th><code>BOARD_VNDK_RUNTIME_DISABLE</code></th>
   <th>Versão dos binários do sistema<br />da GSI legada</th>
   <th>Compatível com a GSI legada</th>
  </tr>
  <tr>
   <td>0</td>
   <td>8.0</td>
   <td>(qualquer)</td>
   <td>(N/A)</td>
   <td>9</td>
   <td>Não</td>
  </tr>
  <tr>
   <td>1.a</td>
   <td>8.1</td>
   <td>(vazio)</td>
   <td>(qualquer)</td>
   <td>9</td>
   <td>Não</td>
  </tr>
  <tr>
   <td>1.b</td>
   <td>8.1</td>
   <td><code>current</code></td>
   <td><code>true</code></td>
   <td>9</td>
   <td>Não</td>
  </tr>
  <tr>
   <td>2</td>
   <td>8.1</td>
   <td><code>current</code></td>
   <td>(vazio)</td>
   <td>9</td>
   <td>Sim</td>
  </tr>
  <tr>
   <td>3</td>
   <td>9</td>
   <td><code>current</code></td>
   <td><code>true</code></td>
   <td>9</td>
   <td>Sim</td>
  </tr>
  <tr>
   <td>4</td>
   <td>9</td>
   <td><code>current</code></td>
   <td>(vazio)</td>
   <td>9</td>
   <td>Sim</td>
  </tr>
</tbody></table>

<p>
  O caso de uso compatível mais comum é o 2, em que a GSI legada é compatível com dispositivos que executam o Android 8.1 e foram criados com o <code>BOARD_VNDK_VERSION</code>, mas sem o <code>BOARD_VNDK_RUNTIME_DISABLE</code> (ou seja, a aplicação do tempo de execução NÃO foi desativada).
</p>

<p>
  Os dois casos de uso não compatíveis são o 1.a e 1.b, em que a GSI legada NÃO é compatível com dispositivos que executam o Android 8.1 que não foram criados com <code>BOARD_VNDK_VERSION</code> ou com <code>BOARD_VNDK_RUNTIME_DISABLE</code> (ou seja, a aplicação do tempo de execução FOI desativada). Esses dispositivos não são compatíveis, porque seus binários de fornecedor dependem de bibliotecas compartilhadas do Android 8.1 não VNDK, que não estão incluídas em GSIs legadas. Para tornar esses dispositivos compatíveis com a GSI legada, os fornecedores precisam executar um dos seguintes procedimentos:
</p>

<ul>
  <li>Ativar <code>BOARD_VNDK_VERSION</code> sem <code>BOARD_VNDK_RUNTIME_DISABLE</code> (caso de uso 2)
    <br /><br />OU<br /><br /></li>
  <li>Portar/fazer upgrade dos binários do fornecedor para depender de bibliotecas compartilhadas do Android 9 (casos de uso 3 e 4).</li>
</ul>

<h2 id="building-gsis">Como criar GSIs</h2>

<p>
  A partir do Android 9, cada versão do sistema tem um branch de GSI chamado <code><var>DESSERT</var>-gsi</code> no AOSP (por exemplo, <code>pie-gsi</code> é o branch da GSI no Android 9). Os branches de GSI incluem o conteúdo do Android com todos os <a href="/security/bulletin/">patches de segurança</a> e <a href="#contributing-to-gsis">patches de GSI</a> aplicados.
</p>

<p>
  Para criar uma GSI, configure a árvore de origem do Android <a href="/setup/build/downloading">fazendo o download</a> de um branch de GSI e <a href="/setup/build/building#choose-a-target">escolhendo um destino de criação da GSI</a>. Use as tabelas de destino de criação abaixo para determinar a versão correta da GSI para seu dispositivo. Após a conclusão da criação, a GSI torna-se a imagem do sistema (por exemplo, <code>system.img</code>) e aparece na pasta de saída <code>out/target/product/<strong>generic_arm64_ab</strong></code>. A criação também gera <code>vbmeta.img</code>, que você pode usar para desativar a verificação de inicialização nos dispositivos com o artigo <a href="/security/verifiedboot/avb">Inicialização verificada do Android</a>.
</p><p>

</p><p>
  Por exemplo, para gerar o destino de criação <code>aosp_arm64_ab-userdebug</code> da GSI legada no branch de GSI <code>pie-gsi</code>, execute os seguintes comandos:
</p>

<pre class="prettyprint">
$ repo init -u https://android.googlesource.com/platform/manifest -b pie-gsi
$ repo sync -cq
$ source build/envsetup.sh
$ lunch aosp_arm64_ab-userdebug
$ make -j4
</pre>

<h3 id="p-gsi-build-targets">Destinos de criação de GSI do Android 9</h3>

<p>
  Os seguintes destinos de criação de GSI são voltados para dispositivos lançados com o Android 9. Devido à redução nas variações entre as arquiteturas, o Android 9 inclui apenas quatro produtos GSI.
</p>

<table>
  <tbody><tr>
   <th>Nome da GSI</th>
   <th>Arquitetura da CPU</th>
   <th>Quantidade de bits da interface Binder</th>
   <th>Sistema como raiz</th>
   <th>Nome do produto</th>
  </tr>
  <tr>
   <td><code>aosp_arm</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm64-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_64-userdebug</code></td>
  </tr>
</tbody></table>

<h3 id="legacy-gsi-build-targets">Destinos de criação de GSI legada do Android 9</h3>

<p>
  Os seguintes destinos de criação de GSI legada são voltados para dispositivos que passam por upgrade para o Android 9. Nomes de GSI legadas incluem o sufixo <code>_ab</code> ou <code>_a</code> para distingui-los dos nomes da GSI do Android 9.
</p>

<table>
  <tbody><tr>
   <th>Nome da GSI</th>
   <th>Arquitetura da CPU</th>
   <th>Quantidade de bits da interface Binder</th>
   <th>Sistema como raiz</th>
   <th>Nome do produto</th>
  </tr>
  <tr>
   <td><code>aosp_arm_a</code></td>
   <td><code>ARM</code></td>
   <td><code>32</code></td>
   <td><code>N</code></td>
   <td><code>aosp_arm_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm_ab</code></td>
   <td><code>ARM</code></td>
   <td><code>32</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>aosp_arm_64b_ab</code></td>
   <td><code>ARM</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm_64b_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64_a</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td><code>aosp_arm64_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_arm64_ab</code></td>
   <td><code>ARM64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_arm64_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_a</code></td>
   <td><code>x86</code></td>
   <td><code>32</code></td>
   <td><code>N</code></td>
   <td><code>aosp_x86_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_ab</code></td>
   <td><code>x86</code></td>
   <td><code>32</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_ab-userdebug</code></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>**NA</code></td>
   <td><code>x86</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64_a</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>N</code></td>
   <td><code>aosp_x86_64_a-userdebug</code></td>
  </tr>
  <tr>
   <td><code>aosp_x86_64_ab</code></td>
   <td><code>x86-64</code></td>
   <td><code>64</code></td>
   <td><code>Y</code></td>
   <td><code>aosp_x86_64_ab-userdebug</code></td>
  </tr>
</tbody></table>
<em>** Pode ser adicionado mediante solicitação</em>

<aside class="aside">
  <strong>Observação</strong>: esses destinos de criação provavelmente serão removidos em uma versão futura do Android.
</aside>

<h2 id="flashing-gsis">Requisitos de atualização de GSIs com flash</h2>

<p>
  Os dispositivos Android podem ter designs diferentes, então não é possível ter um comando ou conjunto de instruções único para atualizar uma GSI com flash para um dispositivo específico. Consulte o fabricante do dispositivo Android para receber as instruções explícitas de atualização com flash ou use as seguintes etapas gerais como diretrizes:
</p>

<ol>
  <li>Verifique se o dispositivo conta com:
    <ul>
      <li>compatibilidade com interfaces HIDL-HAL;</li>
      <li>um método para desbloquear dispositivos (para que eles possam ser atualizados com flash usando <code>fastboot</code>);</li>
      <li>um método para desativar a verificação da inicialização (por exemplo, <a href="/security/verifiedboot/">vboot 1.0</a>, <a href="/security/verifiedboot/avb">AVB</a> etc.);</li>
      <li>desbloqueio do dispositivo para torná-lo atualizável com flash por meio de <code>fastboot</code>. Para garantir que você tenha a versão mais recente do <code>fastboot</code>, crie-a com base na árvore de origem do Android.</li>
    </ul>
  </li>
  <li>Desative a verificação de inicialização.</li>
  <li>Limpe a partição atual do sistema e, em seguida, atualize a GSI com flash para a partição.</li>
  <li>Exclua permanentemente os dados do usuário e limpe outras partições necessárias (por exemplo, metadados).</li>
  <li>Reinicialize o dispositivo.</li>
</ol>

<p>
  Por exemplo, para atualizar uma GSI com flash para qualquer dispositivo Pixel:
</p>

<ol>
  <li><a href="/setup/build/running#booting-into-fastboot-mode">Inicialize no modo do carregador de inicialização</a> e <a href="/setup/build/running#unlocking-the-bootloader">desbloqueie o carregador de inicialização</a>.</li>
  <li>Desative a verificação de inicialização do Android (AVB, na sigla em inglês) atualizando com flash o <code>vbmeta.img</code>:
<pre class="prettyprint">$ fastboot flash vbmeta vbmeta.img</pre></li>
  <li>Limpe e atualize a GSI com flash para a partição do sistema:
<pre class="prettyprint">
$ fastboot erase system
$ fastboot flash system system.img
</pre></li>
  <li>Exclua permanentemente os dados do usuário e limpe outras partições necessárias.
<pre class="prettyprint">$ fastboot -w</pre></li>
  <li>Reinicialize:
<pre class="prettyprint">$ fastboot reboot</pre></li>
</ol>

<h2 id="contributing-to-gsis">Como contribuir com GSIs</h2>

<p>
  O Android aceita de braços abertos suas contribuições para o desenvolvimento de GSI. Você pode participar e ajudar na melhoria da GSI da seguinte forma:
</p>

<ul>
  <li><strong>Criando um patch de GSI</strong>. Como o <code><var>DESSERT</var>-gsi</code> <strong>não</strong> é um branch de desenvolvimento e aceita apenas seleções do branch master do AOSP, para enviar um patch de GSI, é preciso:
    <ol>
      <li>enviar o patch para o branch master do <a href="https://android-review.googlesource.com" class="external">AOSP</a>;</li>
      <li>selecionar o patch para <code><var>DESSERT</var>-gsi</code>;</li>
      <li>informar um bug para que a seleção seja analisada.</li>
    </ol>
  </li>
  <li><strong>Informando bugs da GSI</strong> ou fazendo outras sugestões. Leia as instruções em <a href="/setup/contribute/report-bugs#platform">Como informar bugs</a> e procure ou registre bugs de GSI (procure por "Imagem genérica do sistema" na tabela "Plataforma").</li>
</ul>

</body></html>