<html devsite><head>
    <title>Como compilar com o Jack</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<aside class="warning">
  <strong>Aviso</strong>: a partir deste <a href="https://android-developers.googleblog.com/2017/03/future-of-java-8-language-feature.html" class="external">anúncio de 14 de março de 2017</a>, o uso da cadeia de ferramentas Jack foi suspenso. O Jack era a cadeia de ferramentas padrão de criação do Android para as versões 6.0 a 8.1 do sistema.
</aside>

<p>Ele era uma cadeia de ferramentas do Android que compilava fonte Java em bytecode dex do Android. Você não precisa fazer nada diferente para usar o Jack, apenas usar seus comandos makefile padrão para compilar a árvore ou o projeto. O Android 8.1 é a última versão que usa o Jack.</p>

<h2 id="overview">Sobre o Jack</h2>
<p>O Jack funciona da seguinte maneira:</p>

<img src="../images/jack_overview.png" alt="Visão geral do Jack"/>
<figcaption><strong>Figura 1.</strong> Visão geral do Jack</figcaption>

<h3 id="jack_library">Formato da biblioteca do Jack</h3>

<p>O Jack tem o próprio formato de arquivo .jack que contém o código dex pré-compilado para a biblioteca, possibilitando uma compilação mais rápida (pré-dex).</p>

<img src="../images/jack_library.png" alt="Conteúdo do arquivo da biblioteca do Jack"/>
<figcaption><strong>Figura 2.</strong> Conteúdo do arquivo da biblioteca do Jack</figcaption>

<h3 id="jill">Jill</h3>

<p>A ferramenta Jill converte as bibliotecas .jar existentes para o novo formato de biblioteca, como mostrado abaixo.</p>

<img src="../images/jack_jill.png" alt="Como importar bibliotecas .jar com o Jill"/>
<figcaption><strong>Figura 3.</strong> Fluxo de trabalho para importar uma biblioteca .jar existente</figcaption>

<h2 id="using_jack">Servidor de compilação do Jack</h2>

<aside class="note"><strong>Observação</strong>: as instruções a seguir se aplicam apenas ao uso do Jack no Android 6.x. Para ver instruções sobre como usar o Jack no Android 7.x e 8.x, consulte a <a href="https://android.googlesource.com/platform/prebuilts/sdk/+/master/tools/README-jack-server.md" class="external">documentação do servidor Jack</a>.</aside>

<p>A primeira vez que o Jack é usado, ele abre um servidor de compilação Jack local no seu computador,  que:</p>

<ul>
<li>traz um aumento de velocidade intrínseco, porque evita a abertura de um novo host JRE JVM, o carregamento do código do Jack, a inicialização do Jack e o aquecimento do JIT a cada compilação. Ele também oferece tempos de compilação muito bons durante pequenas compilações (por exemplo, no modo incremental);</li>
<li>é uma solução de curto prazo para controlar o número de compilações Jack paralelas. Ele evita sobrecarregar seu computador (problema de memória ou de disco) porque limita o número de compilações paralelas.</li>
</ul>

<p>O servidor Jack se desativa após um tempo ocioso sem nenhuma compilação.
Ele usa duas portas TCP na interface do localhost e não está disponível externamente. Todos os parâmetros (número de compilações paralelas, tempo limite, número de portas etc.) podem ser modificados editando o arquivo <code>$HOME/.jack</code>.</p>

<h3 id="home_jack_file">Arquivo $HOME/.jack</h3>

<p>O arquivo <code>$HOME/.jack</code> contém as seguintes configurações para as variáveis do servidor Jack em uma sintaxe bash completa:</p>

<ul>
<li><code>SERVER=true</code>. Ativa o recurso de servidor do Jack.</li>
<li><code>SERVER_PORT_SERVICE=8072</code>. Define o número da porta TCP do servidor para fins de compilação.</li>
<li><code>SERVER_PORT_ADMIN=8073</code>. Define o número da porta TCP do servidor para fins administrativos.</li>
<li><code>SERVER_COUNT=1</code>. Não utilizado.
</li><li><code>SERVER_NB_COMPILE=4</code>. Define o número máximo de compilações paralelas permitidas.</li>
<li><code>SERVER_TIMEOUT=60</code>. Define o número de segundos de ociosidade que o servidor precisa aguardar sem nenhuma compilação antes de se desativar.</li>
<li><code>SERVER_LOG=${SERVER_LOG:=$SERVER_DIR/jack-$SERVER_PORT_SERVICE.log}</code>.
Define o arquivo em que os logs do servidor são gravados. Por padrão, essa variável pode ser sobrecarregada por uma variável de ambiente.</li>
<li><code>JACK_VM_COMMAND=${JACK_VM_COMMAND:=java}</code>. Define o comando padrão usado para abrir uma JVM no host. Por padrão, essa variável pode ser sobrecarregada pela variável de ambiente.</li>
</ul>

<h3 id="jack_troubleshooting">Solução de problemas de compilação do Jack</h3>

<table>
<tbody><tr>
<th>Problema</th>
<th>Ação</th>
</tr>
<tr>
<td>Seu computador deixa de responder durante a compilação ou você percebe que as compilações do Jack falham com "Erro de falta de memória"</td>
<td>É possível melhorar a situação reduzindo o número de compilações do Jack simultâneas editando <code>$HOME/.jack</code> e alterando <code>SERVER_NB_COMPILE</code> para um valor mais baixo.</td>
</tr>
<tr>
<td>As compilações estão falhando com "Não é possível iniciar o servidor em segundo plano"</td>
<td>A causa mais provável é que as portas TCP já estão sendo usadas no seu computador. Altere as portas editando <code>$HOME/.jack</code> (as variáveis <code>SERVER_PORT_SERVICE</code> e <code>SERVER_PORT_ADMIN</code>). Para resolver a situação, desative o servidor de compilação do Jack editando <code>$HOME/.jack</code> e alterando <code>SERVER</code> para falso. Infelizmente, isso diminuirá significativamente a velocidade da sua compilação e poderá obrigar você a abrir <code>make -j</code> com controle de carga (opção <code>-l</code> de <code>make</code>).</td>
</tr>
<tr>
<td>A compilação é interrompida sem qualquer progresso</td>
<td>Para resolver a situação, elimine o servidor em segundo plano do Jack usando <code>jack-admin kill-server</code>) e remova os diretórios temporários contidos em <code>jack-$USER</code> do seu diretório temporário (<code>/tmp</code> ou <code>$TMPDIR</code>).</td>
</tr>
</tbody></table>

<h3 id="jack_log">Como encontrar o log do Jack</h3>
<p>Se você executar um comando <code>make</code> com um destino dist, o log do Jack estará em <code>$ANDROID_BUILD_TOP/out/dist/logs/jack-server.log</code>.
Caso contrário, você pode encontrar o log executando <code>jack-admin server-log</code>.
Em caso de falhas do Jack reproduzíveis, veja um log mais detalhado definindo a seguinte variável:</p>

<pre class="devsite-terminal devsite-click-to-copy">
export ANDROID_JACK_EXTRA_ARGS="--verbose debug --sanity-checks on -D sched.runner=single-threaded"
</pre>

<p>Use comandos makefile padrão para compilar a árvore (ou seu projeto) e anexar a saída padrão e o erro. Para remover os logs de criação detalhados, execute:</p>

<pre class="devsite-terminal devsite-click-to-copy">
unset ANDROID_JACK_EXTRA_ARGS
</pre>

<h3 id="jack_limitations">Limitações do Jack</h3>

<ul>
<li>Por padrão, o servidor do Jack contém apenas um usuário e pode ser usado apenas por um usuário em um computador. Para aceitar mais usuários, selecione diferentes números de porta para cada um deles e ajuste o <code>SERVER_NB_COMPILE</code> corretamente. Também é possível desativar o servidor do Jack configurando <code>SERVER=false</code> em <code>$HOME/.jack</code>.</li>
<li>A compilação do CTS é lenta devido à integração atual do <code>vm-tests-tf</code>.
</li><li>As ferramentas de manipulação de bytecode (como o JaCoCo) não são compatíveis.</li>
</ul>

<h2 id="using_jack_features">Como usar o Jack</h2>

<p>O Jack é compatível com a linguagem de programação Java 1.7 e integra os outros recursos descritos abaixo.</p>

<h3 id="predexing">Pré-dexação</h3>

<p>Ao gerar um arquivo de biblioteca do Jack, o <code>.dex</code> da biblioteca é gerado e armazenado dentro do arquivo de biblioteca <code>.jack</code> como um pré-dex.
Durante a compilação, o Jack reutiliza o pré-dex de cada biblioteca. Todas as bibliotecas são pré-dexadas:</p>

<img src="../images/jack_predex.png" alt="Bibliotecas do Jack com pré-dex"/>
<figcaption><strong>Figura 4.</strong> Bibliotecas do Jack com pré-dex</figcaption>

<p>O Jack não reutiliza o pré-dex da biblioteca se for usado redução, ofuscação ou reempacotamento na compilação.</p>

<h3 id="incremental_compilation">Compilação incremental</h3>

<p>Compilação incremental significa que somente os componentes modificados desde a última compilação (e as dependências deles) são recompilados. A compilação incremental pode ser significativamente mais rápida do que uma compilação completa quando as alterações se limitam a um conjunto de componentes.</p>

<p>A compilação incremental não é ativada por padrão e é desativada automaticamente quando a redução, o ofuscamento, o reempacotamento ou o legado de multidex está habilitado. Para ativar compilações incrementais, inclua a linha a seguir no arquivo <code>Android.mk</code> do projeto que você quer criar de forma incremental:</p>

<pre class="devsite-click-to-copy">LOCAL_JACK_ENABLED := incremental</pre>

<aside class="note"><strong>Observação</strong>: se algumas dependências não forem criadas na primeira vez que você fizer seu projeto com o Jack, use <code>mma</code> para criá-las. Depois disso, você poderá usar o comando de criação padrão.</aside>

<h3 id="shrinking_and_obfuscation">Redução e ofuscação</h3>

<p>O Jack usa arquivos de configuração para evitar a redução e a ofuscação.</p>

<p>Entre as opções comuns estão as seguintes:</p>

<ul>
  <li> <code>@</code>
  </li><li> <code>-include</code>
  </li><li> <code>-basedirectory</code>
  </li><li> <code>-injars</code>
  </li><li> <code>-outjars // only 1 output jar supported</code>
  </li><li> <code>-libraryjars</code>
  </li><li> <code>-keep</code>
  </li><li> <code>-keepclassmembers</code>
  </li><li> <code>-keepclasseswithmembers</code>
  </li><li> <code>-keepnames</code>
  </li><li> <code>-keepclassmembernames</code>
  </li><li> <code>-keepclasseswithmembernames</code>
  </li><li> <code>-printseeds</code>
</li></ul>

<p>Entre as opções de redução está a seguinte:</p>

<ul>
  <li><code>-dontshrink</code>
</li></ul>

<p>Entre as opções de ofuscação estão as seguintes:</p>

<ul>
  <li> <code>-dontobfuscate</code>
  </li><li> <code>-printmapping</code>
  </li><li> <code>-applymapping</code>
  </li><li> <code>-obfuscationdictionary</code>
  </li><li> <code>-classobfuscationdictionary</code>
  </li><li> <code>-packageobfuscationdictionary</code>
  </li><li> <code>-useuniqueclassmembernames</code>
  </li><li> <code>-dontusemixedcaseclassnames</code>
  </li><li> <code>-keeppackagenames</code>
  </li><li> <code>-flattenpackagehierarchy</code>
  </li><li> <code>-repackageclasses</code>
  </li><li> <code>-keepattributes</code>
  </li><li> <code>-adaptclassstrings</code>
</li></ul>

<p>Entre as opções ignoradas estão as seguintes:</p>

<ul>
  <li> <code>-dontoptimize // Jack does not optimize</code>
  </li><li> <code>-dontpreverify // Jack does not preverify</code>
  </li><li> <code>-skipnonpubliclibraryclasses</code>
  </li><li> <code>-dontskipnonpubliclibraryclasses</code>
  </li><li> <code>-dontskipnonpubliclibraryclassmembers</code>
  </li><li> <code>-keepdirectories</code>
  </li><li> <code>-target</code>
  </li><li> <code>-forceprocessing</code>
  </li><li> <code>-printusage</code>
  </li><li> <code>-whyareyoukeeping</code>
  </li><li> <code>-optimizations</code>
  </li><li> <code>-optimizationpasses</code>
  </li><li> <code>-assumenosideeffects</code>
  </li><li> <code>-allowaccessmodification</code>
  </li><li> <code>-mergeinterfacesaggressively</code>
  </li><li> <code>-overloadaggressively</code>
  </li><li> <code>-microedition</code>
  </li><li> <code>-verbose</code>
  </li><li> <code>-dontnote</code>
  </li><li> <code>-dontwarn</code>
  </li><li> <code>-ignorewarnings</code>
  </li><li> <code>-printconfiguration</code>
  </li><li> <code>-dump</code>
</li></ul>

<aside class="note"><strong>Observação</strong>: outras opções gerarão um erro.</aside>

<h3 id="repackaging">Reempacotamento</h3>

<p>O Jack usa arquivos de configuração jarjar para realizar o reempacotamento. Embora o Jack seja compatível com os tipos de regra "rule", ele não é compatível com os tipos de regra "zap" ou "keep".</p>

<h3 id="multidex_support">Compatibilidade com multidex</h3>

<p>O Jack oferece compatibilidade nativa e legada com multidex. Como os arquivos dex estão limitados a 65 mil métodos, os apps com mais do que isso precisam ser divididos em vários arquivos dex. Para ver mais detalhes, consulte <a href="http://developer.android.com/tools/building/multidex.html" class="external">Como criar apps com mais de 65 mil métodos</a>.</p>

</body></html>