<html devsite><head>
    <title>Visão geral</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  Trabalhar com código Android requer o uso do <strong>Git</strong> (um sistema de controle de versão de código aberto) e do <strong>Repo</strong> (uma ferramenta de gestão de repositório criada pelo Google e executada sobre o Git).
</p>

<h2 id="git">Git</h2>

<p>
  O Git foi projetado para lidar com grandes projetos distribuídos em vários repositórios. O Android usa o Git para operações locais, como branches, confirmações, diferenças e edições locais. Um dos desafios da configuração do projeto Android foi descobrir como oferecer o melhor apoio à comunidade externa, desde a comunidade de amadores até grandes OEMs que fabricam dispositivos de consumo para o mercado de massa. Queríamos componentes interessantes que tivessem uma vida própria fora do Android e que fossem substituíveis. Primeiro, escolhemos um sistema de controle de revisão distribuído e, em seguida, nós o limitamos ao Git.
</p>

<p>
  Para ver mais detalhes sobre o Git, consulte a <a href="https://git-scm.com/documentation" class="external">documentação do Git</a> (em inglês).
</p>

<h2 id="repo">Repo</h2>
<p>
  O Repo unifica os repositórios do Git quando necessário, realiza uploads para o <a href="https://android-review.googlesource.com/">sistema de controle de revisão Gerrit</a> e automatiza partes do fluxo de trabalho de desenvolvimento do Android. O objetivo não é que o Repo substitua Git, mas apenas facilite o trabalho com o Git no contexto do Android. O comando Repo é um script Python executável que pode ser colocado em qualquer lugar do seu caminho. Ao trabalhar com arquivos de origem do Android, você usa o Repo para operações em toda a rede. Por exemplo, com um único comando Repo, é possível fazer o download de arquivos de vários repositórios no seu diretório de trabalho local.
</p>

<p>
  Na maioria das situações, é possível usar o Git em vez do Repo ou combinar os comandos Repo e Git para formar comandos complexos. No entanto, usar o Repo para operações básicas em toda a rede facilitará muito seu trabalho. Para ver mais detalhes sobre o Repo, consulte o artigo <a href="/setup/develop/repo">Referência do comando Repo</a>.
</p>

<h2 id="other-tools">Outras ferramentas</h2>

<p>
  Uma das outras ferramentas usadas é o <a href="https://gerrit-review.googlesource.com/Documentation/" class="external">Gerrit</a>, um sistema de análise de código baseado na Web para projetos que usam o Git. O Gerrit incentiva o uso mais centralizado do Git, permitindo que todos os usuários autorizados enviem alterações, que são automaticamente mescladas se forem aprovadas na análise do código. Além disso, o Gerrit facilita a análise, exibindo as alterações lado a lado no navegador e permitindo comentários in-line.
</p>

<p>
  Por fim, o <a href="http://developer.android.com/tools/studio/index.html" class="external">Android Studio</a> é o ambiente de desenvolvimento integrado oficial para o desenvolvimento de aplicativos Android.
</p>

<h2 id="workflow">Fluxo de trabalho</h2>

<p>
  O desenvolvimento do Android envolve o seguinte fluxo de trabalho básico:
</p>

<ol>
  <li>Iniciar um novo branch de tópico usando <code>repo start</code>.
  </li>
  <li>Editar os arquivos.
  </li>
  <li>Preparar as alterações usando <code>git add</code>.
  </li>
  <li>Confirmar as alterações usando <code>git commit</code>.
  </li>
  <li>Fazer upload das alterações para o servidor de revisão usando <code>repo upload</code>.
  </li>
</ol>

<h2 id="common-tasks">Tarefas comuns</h2>

<p>
  Trabalhar com o Git e o Repo nos repositórios de código do Android envolve a execução das seguintes tarefas comuns:
</p>

<table>
  <tbody><tr>
    <th>Comando</th>
    <th>Descrição</th>
  </tr>
  <tr>
    <td><code>repo init</code></td>
    <td>Inicializa um novo cliente.</td>
  </tr>
  <tr>
    <td><code>repo sync</code></td>
    <td>Sincroniza o cliente nos repositórios.</td>
  </tr>
  <tr>
    <td><code>repo start</code></td>
    <td>Inicia um novo branch.</td>
  </tr>
  <tr>
    <td><code>repo status</code></td>
    <td>Mostra o status do branch atual.</td>
  </tr>
  <tr>
    <td><code>repo upload</code></td>
    <td>Faz upload das alterações para o servidor de revisão.</td>
  </tr>
  <tr>
    <td><code>git add</code></td>
    <td>Prepara os arquivos.</td>
  </tr>
  <tr>
    <td><code>git commit</code></td>
    <td>Confirma os arquivos preparados.</td>
  </tr>
  <tr>
    <td><code>git branch</code></td>
    <td>Mostra os branches atuais.</td>
  </tr>
  <tr>
    <td><code>git branch [branch]</code></td>
    <td>Cria um novo branch de tópico.</td>
  </tr>
  <tr>
    <td><code>git checkout [branch]</code></td>
    <td>Muda o HEAD para o branch especificado.</td>
  </tr>
  <tr>
    <td><code>git merge [branch]</code></td>
    <td>Mescla o [branch] com o branch atual.</td>
  </tr>
  <tr>
    <td><code>git diff</code></td>
    <td>Mostra a diferença entre alterações não preparadas.</td>
  </tr>
  <tr>
    <td><code>git diff --cached</code></td>
    <td>Mostra a diferença entre alterações preparadas.</td>
  </tr>
  <tr>
    <td><code>git log</code></td>
    <td>Mostra o histórico do branch atual.</td>
  </tr>
  <tr>
    <td><code>git log m/[codeline]..</code></td>
    <td>Mostra confirmações que não foram enviadas.</td>
  </tr>
</tbody></table>

<p>
  Para ver informações sobre o uso do Repo para fazer o download da origem, consulte <a href="/setup/build/downloading">Como fazer o download da origem</a> e <a href="/setup/develop/repo">Referência do comando Repo</a>.
</p>

<h3 id="synchronizing-clients">Como sincronizar clientes</h3>

<p>
  Para sincronizar os arquivos para todos os projetos disponíveis:
</p>
<pre class="devsite-terminal devsite-click-to-copy">repo sync</pre>

<p>
  Para sincronizar os arquivos para os projetos selecionados:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo sync <var>PROJECT0 PROJECT1 ... PROJECTN</var>
</pre>

<h3 id="creating-topic-branches">Como criar branches de tópicos</h3>

<p>
  Inicie um branch de tópico no seu ambiente de trabalho local sempre que começar uma alteração, como quando você começar a trabalhar em um bug ou em um novo recurso. Um branch de tópico <strong>não</strong> é uma cópia dos arquivos originais, mas sim um ponteiro para uma confirmação específica, o que torna a criação de branches locais e a alternância entre eles uma operação leve. Usando branches, é possível isolar um aspecto do seu trabalho dos outros. Para ver um artigo interessante sobre o uso de branches de tópicos, consulte <a href="http://www.kernel.org/pub/software/scm/git/docs/howto/separating-topic-branches.txt" class="external">Como separar branches de tópicos</a> (link em inglês).
</p>

<p>
  Para iniciar um branch de tópico usando o Repo, navegue até o projeto e execute:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo start <var>BRANCH_NAME</var> .
</pre>

<p>
  O ponto final (.) representa o projeto no diretório de trabalho atual.
</p>

<p>
  Para verificar se o novo branch foi criado:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo status .
</pre>

<h3 id="using-topic-branches">Como usar branches de tópicos</h3>

<p>Para atribuir o branch a um projeto específico:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo start <var>BRANCH_NAME PROJECT_NAME</var>
</pre>

<p>Para ver uma lista de todos os projetos, consulte <a href="https://android.googlesource.com/" class="external">android.googlesource.com</a>.
Se você já navegou para o diretório do projeto, use um ponto apenas para representar o projeto atual.
</p>

<p>
  Para mudar para outro branch no seu ambiente de trabalho local:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git checkout <var>BRANCH_NAME</var>
</pre>

<p>
  Para ver uma lista de branches existentes:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git branch
</pre>

<p>ou</p>

<pre class="devsite-terminal devsite-click-to-copy">
repo branches
</pre>

<p>
  Os dois comandos retornam a lista de branches existentes com o nome do branch atual precedido por um asterisco (*).
</p>

<aside class="note"><strong>Observação</strong>: um bug pode fazer com que <code>repo
sync</code> redefina o branch de tópico local. Se <code>git branch</code> mostrar * (sem branch) depois que você executar o <code>repo sync</code>, execute <code>git checkout</code> novamente.</aside>

<h3 id="staging-files">Como preparar arquivos</h3>

<p>
  Por padrão, o Git percebe, mas não monitora, as alterações feitas em um projeto.
  Para dizer ao Git para preservar suas alterações, você precisa marcar ou <em>preparar</em> essas mudanças para inclusão em uma confirmação.
</p>

<p>
  Para preparar alterações:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git add
</pre>

<p>
  Esse comando aceita argumentos para arquivos ou diretórios dentro do diretório do projeto. Apesar do nome, <code>git add</code> não tem como única função adicionar arquivos ao repositório git, mas também preparar modificações e exclusões de arquivos.
</p>

<h3 id="viewing-client-status">Como ver o status do cliente</h3>

<p>
  Para listar o estado dos arquivos:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo status
</pre>

<p>
  Para ver as edições não confirmadas (edições locais que <strong>não</strong> estão marcadas para confirmação):
</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo diff
</pre>

<p>
  Para ver as edições confirmadas (as edições localizadas <strong>marcadas</strong> para confirmação), verifique se você está no diretório do projeto e, em seguida, execute <code>git
  diff</code> com o argumento <code>cached</code>:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">cd <var>~/WORKING_DIRECTORY/PROJECT</var></code>
<code class="devsite-terminal">git diff --cached</code>
</pre>

<h3 id="committing-changes">Como confirmar alterações</h3>

<p>
  Uma <em>confirmação</em> é a unidade básica do controle de revisão no Git e consiste em um instantâneo da estrutura de diretório e do conteúdo do arquivo para todo o projeto. Para criar uma confirmação no Git:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
git commit
</pre>

<p>
  Quando for solicitada uma mensagem de confirmação, forneça uma mensagem breve, mas útil, para as alterações enviadas ao AOSP. Se você não adicionar uma mensagem de confirmação, ela será cancelada.
</p>

<h3 id="uploading-changes-to-gerrit">Como fazer upload das alterações para o Gerrit</h3>

<p>
  Atualize para a revisão mais recente e, em seguida, faça upload da alteração:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">repo sync</code>
<code class="devsite-terminal">repo upload</code>
</pre>

<p>
  Esse comando retorna uma lista das alterações que você confirmou e solicita que você selecione os branches que serão enviados para o servidor de revisão. Se houver apenas um branch, você verá uma solicitação <code>y/n</code> simples.
</p>

<h3 id="resolving-sync-conflicts">Como resolver conflitos de sincronização</h3>

<p>
  Se o comando <code>repo sync</code> retornar conflitos de sincronização:
</p>

<ol>
  <li>Veja os arquivos cuja mesclagem foi cancelada (código de status = U).</li>
  <li>Edite as regiões de conflito conforme necessário.</li>
  <li>Mude para o diretório do projeto relevante. Adicione e confirme os arquivos afetados e realoque as alterações:
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">git add .</code>
<code class="devsite-terminal">git commit</code>
<code class="devsite-terminal">git rebase --continue</code>
</pre>
  </li>
  <li>Após a conclusão da realocação, inicie toda a sincronização novamente:
<pre class="devsite-terminal devsite-click-to-copy">
repo sync <var>PROJECT0 PROJECT1 ... PROJECTN</var>
</pre>
  </li>
</ol>

<h3 id="cleaning-up-client-files">Como fazer a limpeza de clientes</h3>
<p>
  Depois de mesclar as alterações no Gerrit, atualize seu diretório de trabalho local e use <code>repo prune</code> para remover com segurança os branches de tópico desatualizados:
</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">repo sync</code>
<code class="devsite-terminal">repo prune</code>
</pre>

<h3 id="deleting-clients">Como excluir clientes</h3>
<p>
  Como todas as informações de estado são armazenadas no seu cliente, você só precisa excluir o diretório do seu sistema de arquivos:
</p>
<pre class="devsite-terminal devsite-click-to-copy">
rm -rf <var>WORKING_DIRECTORY</var>
</pre>

<p>
  A exclusão de um cliente <em>exclui permanentemente</em> todas as alterações que você ainda não enviou para análise.
</p>

</body></html>