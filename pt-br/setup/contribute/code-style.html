<html devsite><head>
    <title>Estilo de código Java do AOSP para Colaboradores</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
  Os estilos de código abaixo são regras rígidas para a contribuição de código Java para o Android Open Source Project (AOSP).  As contribuições para a plataforma Android que não seguirem essas regras geralmente <em>não serão aceitas</em>. Entendemos que nem todo código existente segue essas regras, mas esperamos que todos os novos códigos estejam em conformidade com elas.
</p>

<aside class="note">
  <strong>Observação</strong>: estas regras destinam-se à plataforma Android e não são obrigatórias para desenvolvedores de apps Android. Os desenvolvedores de apps podem seguir o padrão que escolherem, como o <a href="https://google.github.io/styleguide/javaguide.html" class="external">Guia de estilo do Google para Java</a> (em inglês).
</aside>

<h2 id="java-language-rules">Regras de linguagem Java</h2>

  <p>
    O Android segue as convenções de codificação padrão do Java com as outras regras descritas abaixo.
  </p>

  <h3 id="dont-ignore-exceptions">Não ignorar exceções</h3>

    <p>
      Pode ser tentador escrever um código que ignore completamente uma exceção, como:
    </p>

<pre class="prettyprint">
  void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) { }
  }
</pre>

    <p>
      Não faça isso. Embora você possa pensar que seu código nunca encontrará essa condição de erro ou que não é importante corrigi-la, ignorar as exceções acima cria minas no código que podem ser acionadas por outra pessoa algum dia. Você precisa corrigir todas as exceções no seu código de forma consistente. O tratamento específico varia de acordo com o caso.
    </p>

    <p class="inline-block">
      "<em>Sempre que alguém tem uma cláusula catch vazia, essa pessoa deve ficar com muito medo. Definitivamente há momentos em que essa é a coisa certa a se fazer, mas você precisa pelo menos pensar sobre isso. No Java, não há como fugir do sentimento de medo</em>" — <a href="http://www.artima.com/intv/solid4.html" class="external">James Gosling</a>
    </p>

    <p>Alternativas aceitáveis (em ordem de preferência):</p>

    <ul>
      <li>Leve a exceção até o autor da chamada do seu método.
<pre class="prettyprint">
  void setServerPort(String value) throws NumberFormatException {
      serverPort = Integer.parseInt(value);
  }
</pre>
      </li>
      <li>
        Gere uma nova exceção apropriada ao seu nível de abstração.
<pre class="prettyprint">
  void setServerPort(String value) throws ConfigurationException {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        throw new ConfigurationException("Port " + value + " is not valid.");
    }
  }
</pre>
      </li>
      <li>
        Lide com o erro da maneira correta e substitua um valor apropriado no bloco <code>catch {}</code>.
<pre class="prettyprint">
  /** Set port. If value is not a valid number, 80 is substituted. */

  void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        serverPort = 80;  // default port for server
    }
  }
</pre>
      </li>
      <li>
        Capture a exceção e gere uma nova <code>RuntimeException</code>.
        Isso é perigoso, portanto, faça isso apenas se tiver certeza de que, se esse erro ocorrer, a ação mais adequada é causar uma falha.

<pre class="prettyprint">
  /** Set port. If value is not a valid number, die. */

  void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        throw new RuntimeException("port " + value " is invalid, ", e);
    }
  }
</pre>
        <aside class="note">
          <strong>Observação</strong>: a exceção original é transmitida para o construtor da RuntimeException. Se for necessário compilar seu código no Java 1.3, omita a exceção que é a causa.
        </aside>
      </li>
      <li>
        Como último recurso, se você tiver certeza de que ignorar a exceção é apropriado, ignore-a. No entanto, você também precisará comentar uma boa justificava:
<pre class="prettyprint">
/** If value is not a valid number, original port number is used. */

void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) {
        // Method is documented to just ignore invalid user input.
        // serverPort will just be unchanged.
    }
}
</pre>
      </li>
    </ul>

  <h3 id="dont-catch-generic-exception">Não capturar exceção genérica</h3>

    <p>
      Também pode ser tentador ceder à preguiça ao capturar exceções e fazer algo assim:
    </p>

<pre class="prettyprint">
  try {
      someComplicatedIOFunction();        // may throw IOException
      someComplicatedParsingFunction();   // may throw ParsingException
      someComplicatedSecurityFunction();  // may throw SecurityException
      // phew, made it all the way
  } catch (Exception e) {                 // I'll just catch all exceptions
      handleError();                      // with one generic handler!
  }
</pre>

    <p>
      Não faça isso. Em quase todos os casos, é inadequado capturar a exceção genérica ou Throwable (de preferência, não o Throwable, porque ele inclui exceções de erro). Isso é muito perigoso, porque significa que exceções não esperadas (incluindo RuntimeExceptions, como ClassCastException) são capturadas no tratamento de erros no nível do aplicativo. Isso obscurece as propriedades de tratamento de falhas do seu código, o que significa que se alguém adicionar um novo tipo de exceção no código que você está chamando, o compilador não ajudará você a perceber que precisa lidar com o erro de maneira diferente. Na maioria dos casos, não é recomendado processar diferentes tipos de exceção da mesma maneira.
    </p>

    <p>
      A rara exceção a essa regra é o código de teste e o código de nível superior, em que o recomendado é capturar todos os tipos de erro (para evitar que eles apareçam em uma IU ou para manter uma tarefa de lote em execução). Nesses casos, você pode capturar a exceção genérica (ou Throwable) e processar o erro adequadamente.
      Pense com muito cuidado antes de fazer isso e coloque comentários explicando o motivo de essa ação ser segura nesse lugar.
    </p>

    <p>Alternativas para capturar a exceção genérica:</p>

    <ul>
      <li>
        Capture cada exceção separadamente como parte de um bloco multi-catch, por exemplo:
<pre class="prettyprint">
try {
    ...
} catch (ClassNotFoundException | NoSuchMethodException e) {
    ...
}</pre>
      </li>
      <li>
        Refatore seu código para ter um tratamento de erros mais refinado, com vários blocos try. Separe o IO da análise, processe os erros separadamente em cada caso.
      </li>
      <li>
        Gere a exceção novamente. Muitas vezes, não é necessário capturar a exceção nesse nível, basta deixar o método gerá-la.
      </li>
    </ul>

    <p>
      Lembre-se: as exceções são suas amigas. Quando o compilador reclamar que você não está capturando uma exceção, não fique bravo. Sorria, porque ele acaba de facilitar a captura de problemas de tempo de execução no seu código.
    </p>

  <h3 id="dont-use-finalizers">Não utilizar finalizadores</h3>

    <p>
      Os finalizadores são uma maneira de ter um pedaço de código executado quando um objeto é coletado da lixeira. Embora eles possam ser úteis para fazer a limpeza (principalmente de recursos externos), não há garantias sobre quando um finalizador será chamado (ou mesmo de que ele será chamado).
    </p>

    <p>
      O Android não utiliza finalizadores. Na maioria dos casos, é possível fazer o trabalho de um finalizador com bom processamento de exceções. Se você precisar muito, defina um método close() (ou semelhante) e documente exatamente quando esse método precisa ser chamado (consulte InputStream para ver um exemplo). Nesse caso, é recomendável, mas não obrigatório, imprimir uma breve mensagem de registro do finalizador, contanto que isso não sobrecarregue os registros.
    </p>

  <h3 id="fully-qualify-imports">Qualificar totalmente as importações</h3>

    <p>
      Quando você quiser usar a classe Bar do pacote foo, há duas maneiras possíveis de importá-la:
    </p>

    <ul>
      <li><code>import foo.*;</code>
        <p>Reduz potencialmente o número de declarações de importação.</p>
      </li>
      <li><code>import foo.Bar;</code>
        <p>
          Deixa óbvio quais classes são realmente usadas e o código fica mais legível para os administradores.
        </p>
      </li>
    </ul>

    <p>
      Use <code>import foo.Bar;</code> para importar todo o código Android. Uma exceção explícita é gerada para bibliotecas padrão Java (<code>java.util.
      </code>, <code>java.io.*</code> etc.) e o código de teste de unidade (<code>junit.framework.*</code>).
    </p>

<h2 id="java-library-rules">Regras da biblioteca Java</h2>

  <p>
    Existem convenções para usar bibliotecas e ferramentas Java do Android. Em alguns casos, a convenção mudou significativamente, e é possível que códigos mais antigos usem um padrão ou biblioteca obsoletos. Ao trabalhar com esses códigos, você pode continuar com o estilo existente. Ao criar novos componentes, no entanto, nunca use bibliotecas obsoletas.
  </p>

<h2 id="java-style-rules">Regras de estilo Java</h2>

  <h3 id="use-javadoc-standard-comments">Usar comentários padrão do Javadoc</h3>

    <p>
      Cada arquivo precisa ter uma declaração de direitos autorais no topo, seguida por declarações package e import (cada bloco separado por uma linha em branco) e, por fim, a declaração de classe ou interface. Nos comentários do Javadoc, descreva o que a classe ou interface faz.
    </p>
<pre class="prettyprint">
/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.foo;

import android.os.Blah;
import android.view.Yada;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Does X and Y and provides an abstraction for Z.
 */

public class Foo {
    ...
}
</pre>

    <p>
      Cada classe e método público não trivial que você escreve <em>precisa</em> conter um comentário de Javadoc com pelo menos uma frase descrevendo o que a classe ou o método faz. Essa frase precisa começar com um verbo descritivo em terceira pessoa.
    </p>

    <p><strong>Exemplos</strong></p>

<pre class="prettyprint">
/** Returns the correctly rounded positive square root of a double value. */

static double sqrt(double a) {
    ...
}
</pre>

    <p>ou</p>

<pre class="prettyprint">
/**
 * Constructs a new String by converting the specified array of
 * bytes using the platform's default character encoding.
 */
public String(byte[] bytes) {
    ...
}
</pre>

    <p>
      Não é necessário escrever um Javadoc para métodos triviais get e set, como <code>setFoo()</code> se todos os seus Javadoc disserem que se trata de "sets Foo". Se o método fizer algo mais complexo (como aplicar uma restrição ou se tiver um efeito colateral importante), você precisará documentá-lo. Se o significado da propriedade "Foo" não for óbvio, você precisará documentá-lo.
    </p>

    <p>
      O Javadoc pode ser benéfico para todo método que você escrever, público ou não.
      Os métodos públicos fazem parte de uma API e, portanto, exigem o Javadoc. Atualmente, o Android não aplica um estilo específico para escrever comentários em Javadoc, mas você precisa seguir as instruções contidas em <a href="http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html" class="external">Como escrever comentários em documentos na ferramenta Javadoc</a> (em inglês).
    </p>

  <h3 id="write-short-methods">Escrever métodos curtos</h3>

    <p>
      Quando possível, mantenha os métodos curtos e focados. Sabemos que métodos longos às vezes são apropriados, então nenhum limite rígido é imposto para o comprimento do método. Se um método exceder 40 linhas, considere quebrá-lo sem prejudicar a estrutura do programa.
    </p>

  <h3 id="define-fields-in-standard-places">
    Definir campos em lugares padrão
  </h3>

    <p>
      Defina os campos no topo do arquivo ou imediatamente antes dos métodos que os usam.
    </p>

  <h3 id="limit-variable-scope">Limitar o escopo da variável</h3>

    <p>
      Mantenha o escopo das variáveis locais no nível mínimo. Ao fazer isso, você aumenta a capacidade de leitura e manutenção do seu código e reduz a probabilidade de erro. Cada variável precisa ser declarada no bloco mais interno que inclua todos os usos da variável.
    </p>

    <p>
      Variáveis locais precisam ser declaradas no ponto em que são usadas pela primeira vez.
      Quase todas as declarações de variáveis locais precisam conter um inicializador.
      Se você ainda não tiver informações suficientes para inicializar uma variável de maneira coerente, adie a declaração até que as tenha.
    </p>

    <p>
      A exceção são as declarações try-catch. Se uma variável for inicializada com o valor de retorno de um método que gera uma exceção verificada, ela precisará ser inicializada dentro de um bloco try. Se o valor precisar ser usado fora do bloco try, ele terá que ser declarado antes do bloco try, onde ainda não pode ser inicializado de maneira coerente:
    </p>

<pre class="prettyprint">
// Instantiate class cl, which represents some sort of Set

Set s = null;
try {
    s = (Set) cl.newInstance();
} catch(IllegalAccessException e) {
    throw new IllegalArgumentException(cl + " not accessible");
} catch(InstantiationException e) {
    throw new IllegalArgumentException(cl + " not instantiable");
}

// Exercise the set
s.addAll(Arrays.asList(args));
</pre>

    <p>
      No entanto, até mesmo esse caso pode ser evitado pelo encapsulamento do bloco try-catch em um método:
    </p>

<pre class="prettyprint">
Set createSet(Class cl) {
    // Instantiate class cl, which represents some sort of Set
    try {
        return (Set) cl.newInstance();
    } catch(IllegalAccessException e) {
        throw new IllegalArgumentException(cl + " not accessible");
    } catch(InstantiationException e) {
        throw new IllegalArgumentException(cl + " not instantiable");
    }
}

...

// Exercise the set
Set s = createSet(cl);
s.addAll(Arrays.asList(args));
</pre>

    <p>
      As variáveis de loop precisam ser declaradas na própria instrução, a menos que exista uma razão convincente para fazer o contrário:
    </p>

<pre class="prettyprint">
for (int i = 0; i &lt; n; i++) {
    doSomething(i);
}
</pre>

    <p>e</p>

<pre class="prettyprint">
for (Iterator i = c.iterator(); i.hasNext(); ) {
    doSomethingElse(i.next());
}
</pre>

  <h3 id="order-import-statements">Ordenar declarações de importação</h3>

    <p>A ordem das declarações de importação é esta:</p>

    <ol>
      <li>
        importações do Android
      </li>
      <li>
        importações de terceiros (<code>com</code>, <code>junit</code>, <code>net</code>, <code>org</code>)
      </li>
      <li>
        <code>java</code> e <code>javax</code>
      </li>
    </ol>

    <p>Para corresponder exatamente às configurações do ambiente de desenvolvimento integrado, as importações precisam estar:</p>

    <ul>
      <li>
        em ordem alfabética dentro de cada agrupamento, com letras maiúsculas antes de letras minúsculas (por exemplo, Z antes de a);
      </li>
      <li>
        separadas por uma linha em branco entre cada agrupamento principal (<code>android</code>, <code>com</code>, <code>junit</code>, <code>net</code>, <code>org</code>, <code>java</code>, <code>javax</code>).
      </li>
    </ul>

    <p>
      Originalmente, não havia requisito de estilo na ordenação, o que significa que os ambientes de desenvolvimento integrado estavam sempre mudando de ordem ou que os desenvolvedores desses ambientes precisavam desativar os recursos de gerenciamento de importação automática e fazer a manutenção manual das importações. Isso foi considerado algo ruim. Quando o estilo Java era solicitado, os estilos preferidos variavam muito, e o Android precisava simplesmente "escolher uma ordem e ser consistente". Por isso, escolhemos um estilo, atualizamos o guia de estilo e fizemos com que os ambientes de desenvolvimento integrado o obedecessem. Esperamos que, quando os usuários do ambiente de desenvolvimento integrado trabalharem no código, as importações em todos os pacotes corresponderão a esse padrão sem esforço extra de engenharia.
    </p>

    <p>Esse estilo foi escolhido de tal forma que:</p>

    <ul>
      <li>
        as importações que as pessoas querem ver primeiro estejam no topo (<code>android</code>);
        </li>
      <li>
        as importações que as pessoas querem ver por último estejam na parte inferior (<code>java</code>);
      </li>
      <li>
        os humanos possam acompanhar facilmente o estilo;
      </li>
      <li>
        os ambientes de desenvolvimento integrado possam acompanhar o estilo.
      </li>
    </ul>

    <p>
      Coloque as importações estáticas acima de todas as outras importações ordenadas da mesma forma que as importações regulares.
    </p>

  <h3 id="use-spaces-for-indentation">Usar espaços para recuo</h3>

    <p>
      Usamos quatro (4) recuos de espaço para blocos e nunca usamos guias. Em caso de dúvida, seja consistente com o código circundante.
    </p>

    <p>
      Usamos oito (8) recuos de espaço para uniões de linha, incluindo chamadas de função e atribuições.
    </p>

    <p><span class="compare-better">Recomendado</span></p>

<pre class="prettyprint">
Instrument i =
        someLongExpression(that, wouldNotFit, on, one, line);
</pre>

    <p><span class="compare-worse">Não recomendado</span></p>

<pre class="prettyprint">
Instrument i =
    someLongExpression(that, wouldNotFit, on, one, line);
</pre>

  <h3 id="follow-field-naming-conventions">Seguir convenções de nomenclatura de campo</h3>

    <ul>
      <li>
      Os nomes de campo não estáticos e não públicos começam com m.
    </li>
    <li>
      Nomes de campos estáticos começam com s.
    </li>
    <li>
      Outros campos começam com uma letra minúscula.
    </li>
    <li>
      Os campos estáticos públicos finais (constantes) são EM_MAIÚSCULA_E_COM_SUBLINHADOS.
    </li>
  </ul>

  <p>Exemplo:</p>

<pre class="prettyprint">
public class MyClass {
    public static final int SOME_CONSTANT = 42;
    public int publicField;
    private static MyClass sSingleton;
    int mPackagePrivate;
    private int mPrivate;
    protected int mProtected;
}
</pre>

  <h3 id="use-standard-brace-style">Usar estilo padrão para chaves</h3>

    <p>
      As chaves não ficam na própria linha, elas ficam na mesma linha que o código anterior a elas:
    </p>

<pre class="prettyprint">
class MyClass {
    int func() {
        if (something) {
            // ...
        } else if (somethingElse) {
            // ...
        } else {
            // ...
        }
    }
}
</pre>

    <p>
      É obrigatório que as declarações para uma condicional estejam entre chaves. Exceção: se toda a condicional (a condição e o corpo) couber em uma linha, você poderá colocar tudo em uma só linha (não obrigatório). Por exemplo, isto é aceitável:
    </p>

<pre class="prettyprint">
if (condition) {
    body();
}
</pre>

    <p>E isto é aceitável:</p>

<pre class="prettyprint">if (condition) body();</pre>

    <p>Mas isto não é aceitável:</p>

<pre class="prettyprint">
if (condition)
    body();  // bad!
</pre>

  <h3 id="limit-line-length">Limite de comprimento de linha</h3>

    <p>
      Cada linha de texto no seu código precisa ter no máximo 100 caracteres.
      Embora muita discussão tenha cercado essa regra, a decisão de que 100 caracteres é o máximo permanece, <em>com as seguintes exceções</em>:
    </p>

    <ul>
      <li>
        Se uma linha de comentário contiver um comando de exemplo ou um URL literal com mais de 100 caracteres, essa linha poderá ter mais de 100 caracteres para facilitar a operação de recortar e colar.
      </li>
      <li>
        As linhas de importação podem ultrapassar o limite, porque os humanos raramente as veem (isso também simplifica o desenvolvimento de ferramentas).
      </li>
    </ul>

  <h3 id="use-standard-java-annotations">Usar anotações Java padrão</h3>

    <p>
      Anotações precisam preceder outros modificadores para o mesmo elemento de linguagem. Anotações de marcadores simples (por exemplo, @Override) podem ser listadas na mesma linha que o elemento de linguagem. Se houver várias anotações ou anotações parametrizadas, elas precisam ser listadas uma por linha em ordem alfabética.
    </p>

    <p>
      As práticas padrão do Android para as três anotações predefinidas em Java são as seguintes:
    </p>

    <ul>
      <li>
        <code>@Deprecated</code>: a anotação @Deprecated precisa ser utilizada sempre que o uso do elemento anotado não for recomendado. Se você usar a anotação @Deprecated, também precisará ter uma tag @deprecated do Javadoc e precisará nomear uma implementação alternativa. Além disso, lembre-se de que um método @Deprecated <em>ainda deve funcionar</em>.
        Se você vir um código antigo que tenha uma tag @deprecated do Javadoc, adicione a anotação @Deprecated.
      </li>
      <li>
        <code>@Override</code>: a anotação @Override precisa ser utilizada sempre que um método substituir a declaração ou implementação de uma superclasse. Por exemplo, se você usar a tag @inheritdocs do Javadoc e a derivar de uma classe (não de uma interface), você também precisará anotar que o método @Overrides (modifica) o método da classe pai.
      </li>
      <li>
        <code>@SuppressWarnings</code>: o uso da anotação @SuppressWarnings é indicado apenas em circunstâncias em que é impossível eliminar um aviso. Se um aviso passar nesse teste de "impossível eliminar", a anotação @SuppressWarnings <em>precisa</em> ser usada, para assegurar que todos os avisos reflitam problemas reais no código.

        <p>
          Quando uma anotação @SuppressWarnings for necessária, ela precisará ser prefixada com um comentário TODO que explique a condição "impossível eliminar". Isso identificará normalmente uma classe ofensiva que possui uma interface inadequada. Exemplo:
        </p>

<pre class="prettyprint">
// TODO: The third-party class com.third.useful.Utility.rotate() needs generics
@SuppressWarnings("generic-cast")
List&lt;String&gt; blix = Utility.rotate(blax);
</pre>

        <p>
          Quando uma anotação @SuppressWarnings for necessária, o código precisará ser refatorado para isolar os elementos do software em que a anotação se aplica.
        </p>
      </li>
    </ul>

  <h3 id="treat-acronyms-as-words">Tratar acrônimos como palavras</h3>

    <p>
      Trate acrônimos e abreviações como palavras na nomeação de variáveis, métodos e classes para tornar os nomes mais legíveis:
    </p>

    <table>
      <thead>
        <tr>
          <th>Boa</th>
          <th>Ruim</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>XmlHttpRequest</td>
          <td>XMLHTTPRequest</td>
        </tr>
        <tr>
          <td>getCustomerId</td>
          <td>getCustomerID</td>
        </tr>
        <tr>
          <td>class Html</td>
          <td>class HTML</td>
        </tr>
        <tr>
          <td>String url</td>
          <td>String URL</td>
        </tr>
        <tr>
          <td>long id</td>
          <td>long ID</td>
        </tr>
      </tbody>
    </table>

    <p>
      Como as bases de código do JDK e do Android são muito inconsistentes em relação aos acrônimos, é praticamente impossível ser consistente com o código circundante. Portanto, sempre trate acrônimos como palavras.
    </p>

  <h3 id="use-todo-comments">Usar comentários TODO</h3>

    <p>
      Use comentários TODO para o código que é temporário, uma solução de curto prazo ou suficientemente boa, mas não perfeita. Os TODOs precisam incluir a string TODO com todas as letras em caixa alta, seguidas por dois pontos:
    </p>

<pre class="prettyprint">
// TODO: Remove this code after the UrlTable2 has been checked in.
</pre>

    <p>e</p>

<pre class="prettyprint">
// TODO: Change this to use a flag instead of a constant.
</pre>

    <p>
      Se seu TODO estiver no formato "Em uma data futura, faça algo", inclua uma data muito específica ("Correções até novembro de 2005") ou um evento muito específico ("Remova este código depois que todos os mixers de produção entenderem o protocolo V7").
    </p>

  <h3 id="log-sparingly">Usar registros com moderação</h3>

    <p>
      Embora a geração de registros seja necessária, ela tem um impacto significativamente negativo no desempenho e perde a utilidade rapidamente se não for mantida razoavelmente concisa. As instalações de geração de registros oferecem cinco níveis diferentes:
    </p>

    <ul>
      <li>
        <code>ERROR</code>: use quando algo fatal acontecer, ou seja, algo com consequências visíveis ao usuário e não recuperável sem a exclusão explícita de alguns dados, desinstalação de aplicativos, exclusão permanente de partições de dados ou reprogramação em flash do dispositivo (ou pior).
        Esse nível é sempre registrado. Os problemas que justificam alguma geração de registro no nível ERROR são geralmente bons candidatos a serem relatados a um servidor de coleta de estatísticas.
      </li>
      <li>
        <code>WARNING</code>: use quando algo sério e inesperado acontecer, ou seja, algo que tenha consequências visíveis para o usuário, mas que possa ser recuperado sem perda de dados pela execução de alguma ação explícita, que variar entre aguardar um app, reiniciá-lo completamente, fazer o download de uma nova versão de um app ou reinicializar o dispositivo. Esse nível é sempre registrado. Os problemas que justificam alguma geração de registro no nível WARNING também podem ser considerados para geração de relatório para um servidor de coleta de estatísticas.
      </li>
      <li>
        <code>INFORMATIVE:</code> use para anotar que algo interessante para a maioria das pessoas aconteceu, ou seja, quando for detectada uma situação que provavelmente terá um impacto generalizado, embora não seja necessariamente um erro. Essa condição só deve ser registrada por um módulo que acredita razoavelmente ser o de maior autoridade no domínio (para evitar a geração de registros duplicados por componentes não autoritativos). Esse nível é sempre registrado.
      </li>
      <li>
        <code>DEBUG</code>: use para anotar outras informações sobre o que está acontecendo no dispositivo e que poderiam ser relevantes para a investigação e a depuração de comportamentos inesperados. Registre apenas o que é necessário para coletar informações suficientes sobre o que está acontecendo com o componente. Se os registros de depuração estiverem dominando o registro, é provável que você esteja usando a geração de registros detalhados.

        <p>
          Este nível será registrado, mesmo em compilações de versão, e é necessário que seja cercado por um bloco <code>if (LOCAL_LOG)</code> ou <code>if
          LOCAL_LOGD)</code>, em que <code>LOCAL_LOG[D]</code> esteja definido na classe ou subcomponente para que possa haver possibilidade de desativar todo esse tipo de geração de registros. Portanto, não é permitida nenhuma lógica ativa em um bloco <code>if (LOCAL_LOG)</code>. Toda compilação de string para o registro também precisa ser colocada dentro do bloco <code>if (LOCAL_LOG)</code>. A chamada de geração de registros não pode ser refatorada em uma chamada de método se isso fizer com que a compilação de string ocorra fora do bloco <code>if (LOCAL_LOG)</code>.
        </p>

        <p>
          Alguns códigos ainda contêm <code>if (localLOGV)</code>. Isso também é considerado aceitável, embora o nome não seja padrão.
        </p>
      </li>
      <li>
        <code>VERBOSE</code>: use para todo o restante. Esse nível só será registrado em versões de depuração e precisa ser cercado por um bloco <code>if (LOCAL_LOGV)</code> (ou equivalente) para que possa ser compilado por padrão. Todas as compilações de string serão removidas das compilações de versão e precisam aparecer dentro do bloco <code>if (LOCAL_LOGV)</code>.
      </li>
    </ul>

    <h4="log-sparingly-notes">Observações

      <ul>
        <li>
          Dentro de um determinado módulo, que não seja o nível VERBOSE, é necessário que um erro só seja reportado uma vez, se possível. Dentro de uma única cadeia de chamadas de função em um módulo, é necessário que apenas a função mais interna retorne o erro e que os autores de chamada no mesmo módulo só adicionem alguma geração de registro se isso ajudar significativamente a isolar o problema.
        </li>
        <li>
          Em uma cadeia de módulos, que não seja o nível VERBOSE, quando um módulo de nível inferior detectar dados inválidos provenientes de um módulo de nível superior, o módulo de nível inferior precisa registrar essa situação apenas no registro DEBUG e somente se a geração de registros fornecer informações que não estejam disponíveis para o autor da chamada. Especificamente, não é necessário registrar situações em que uma exceção é gerada (a exceção precisa conter todas as informações relevantes) ou em que as únicas informações registradas estão contidas em um código de erro. Isso é especialmente importante na interação entre a biblioteca e os aplicativos, e as condições causadas por aplicativos de terceiros que são adequadamente processados pela biblioteca não podem acionar a geração de registros em um nível mais alto do que o DEBUG. As únicas situações que podem acionar a geração de registros no nível INFORMATIVE ou superior são aquelas em que um módulo ou aplicativo detecta um erro no próprio nível ou proveniente de um nível inferior.
        </li>
        <li>
          Quando for possível que uma condição que normalmente justifica alguma geração de registros ocorra muitas vezes, é recomendável implementar algum mecanismo de limitação de taxa para evitar a sobrecarga dos registros com muitas cópias duplicadas das mesmas informações (ou que sejam muito semelhantes).
        </li>
        <li>
          Perdas de conectividade de rede são consideradas comuns, totalmente esperadas, e não precisam ser registradas sem justificativa. Uma perda de conectividade de rede que tenha consequências em um aplicativo precisa ser registrada no nível DEBUG ou VERBOSE (dependendo se as consequências são sérias e inesperadas o suficiente para serem registradas em uma compilação de versão).
        </li>
        <li>
          Um sistema de arquivos completo em um sistema de arquivos que seja acessível para ou em nome de aplicativos de terceiros não precisa ser registrado em um nível superior a INFORMATIVE.
        </li>
        <li>
          Dados inválidos provenientes de qualquer fonte não confiável (incluindo qualquer arquivo em armazenamento compartilhado ou dados provenientes de qualquer conexão de rede) são considerados esperados e não podem acionar a geração de registros em um nível superior a DEBUG quando forem detectados como inválidos (e, quando existir, o registro precisa ser o mais limitado possível).
        </li>
        <li>
          Tenha em mente que o operador <code>+</code>, quando usado em strings, cria implicitamente um <code>StringBuilder</code> com o tamanho de buffer padrão (16 caracteres) e potencialmente outros objetos de string temporários. Ou seja, a criação explícita de StringBuilders não é mais cara do que a dependência do operador "+" padrão (e, na verdade, pode ser muito mais eficiente). Tenha em mente que o código que chama <code>Log.v()</code> é compilado e executado em compilações de versão, incluindo a compilação de strings, mesmo se os registros não estiverem sendo lidos.
        </li>
        <li>
          Todo registro que seja gerado para ser lido por outras pessoas e esteja disponível em compilações de versão precisa ser conciso sem ser ilegível e precisa ser razoavelmente compreensível. Isso inclui todos os registros no nível DEBUG.
        </li>
        <li>
          Quando possível, a geração de registros precisa ser mantida em uma única linha, se isso fizer sentido. Comprimentos de linha de até 80 ou 100 caracteres são perfeitamente aceitáveis, enquanto comprimentos maiores que 130 ou 160 caracteres, incluindo o comprimento da tag, precisam ser evitados sempre que possível.
        </li>
        <li>
          O registro de sucessos nunca pode ser usado em níveis mais altos que VERBOSE.
        </li>
        <li>
          O registro temporário usado para diagnosticar um problema que é difícil de reproduzir precisa ser mantido no nível DEBUG ou VERBOSE e ser delimitado por blocos que permitam desabilitá-lo totalmente no tempo de compilação.
        </li>
        <li>
          Tenha cuidado com falhas de segurança no registro. Informações particulares precisam ser evitadas. É definitivamente necessário que informações sobre conteúdo protegido sejam evitadas. Isso é especialmente importante ao escrever o código da biblioteca, já que não é fácil saber com antecedência o que é ou não uma informação particular ou um conteúdo protegido.
        </li>
        <li>
          <code>System.out.println()</code> (ou <code>printf()</code> para código nativo) nunca pode ser usado. System.out e System.err são redirecionados para /dev/null, então suas instruções de impressão não terão efeitos visíveis. No entanto, toda compilação de string que acontecer para essas chamadas ainda será executada.
        </li>
        <li>
          <em>A regra de ouro da geração de registros é que os seus não podem enviar desnecessariamente outros registros para fora do buffer, assim como outros registros não podem enviar o seus.</em>
        </li>
      </ul>

  <h3 id="be-consistent">Ser consistente</h3>

    <p>
      Nosso conselho final: SEJA CONSISTENTE. Se você estiver editando um código, reserve alguns minutos para examinar o código circundante e determinar o estilo dele. Se o código usa espaços em torno das cláusulas if, faça o mesmo. Se os comentários do código têm pequenas caixas de estrelas ao redor deles, faça o mesmo com seus comentários.
    </p>

    <p>
      O objetivo de ter diretrizes de estilo é ter um vocabulário comum de codificação, para que as pessoas possam se concentrar no que você está dizendo, e não em como você está dizendo. Apresentamos aqui as regras de estilo globais para que as pessoas conheçam o vocabulário, mas o estilo local também é importante. Se o código adicionado a um arquivo for drasticamente diferente do existente, isso atrapalhará o ritmo dos leitores. Tente evitar isso.
    </p>

<h2 id="javatests-style-rules">Regras de estilo do Javatests</h2>

  <p>
    Siga as convenções de nomenclatura do método de teste e use um caractere sublinhado para separar o que está sendo testado do caso específico que está sendo testado. Esse estilo faz com que seja mais fácil ver exatamente os casos que estão sendo testados. Exemplo:
  </p>

<pre class="prettyprint">
testMethod_specificCase1 testMethod_specificCase2

void testIsDistinguishable_protanopia() {
    ColorMatcher colorMatcher = new ColorMatcher(PROTANOPIA)
    assertFalse(colorMatcher.isDistinguishable(Color.RED, Color.BLACK))
    assertTrue(colorMatcher.isDistinguishable(Color.X, Color.Y))
}
</pre>

</h4="log-sparingly-notes"></body></html>