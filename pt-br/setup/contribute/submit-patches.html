<html devsite><head>
    <title>Como enviar patches</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>Esta página descreve o processo completo de envio de um patch para o AOSP, incluindo revisão e controle de alterações com o <a href="https://android-review.googlesource.com/">Gerrit</a>.</p>
<h3 id="prerequisites">Pré-requisitos</h3>
<ul>
<li>
<p>Antes de seguir as instruções fornecidas nesta página, será preciso <a href="../build/initializing.html">inicializar seu ambiente de criação</a>, <a href="../build/downloading.html">fazer o download da origem</a>, <a href="https://android.googlesource.com/new-password">criar uma senha</a> e seguir as instruções da página do gerador de senhas.</p>
</li>
<li>
<p>Para ver mais detalhes sobre o Repo e o Git, consulte <a href="../develop/index.html">Desenvolvimento</a>.</p>
</li>
<li>
<p>Para ver informações sobre as diferentes funções que você pode desempenhar na comunidade do Android Open Source, consulte <a href="../start/roles.html">Funções do projeto</a>.</p>
</li>
<li>
<p>Se você pretende contribuir com código para a plataforma Android, leia as <a href="../start/licenses.html">informações de licenciamento do AOSP</a>.</p>
</li>
<li>
<p>Observe que as alterações em alguns dos projetos ascendentes usados pelo Android precisam ser feitas diretamente nesses projetos, conforme descrito em <a href="#upstream-projects">Projetos ascendentes</a>.</p>
</li>
</ul>

<h2 id="for-contributors">Para colaboradores</h2>

<h3 id="authenticate-with-the-server">Autenticar com o servidor</h3>
<p>Antes de fazer upload para o Gerrit, você precisa <a href="https://android.googlesource.com/new-password">definir uma senha</a> que o identifique junto ao servidor. Siga as instruções na página do gerador de senhas. É necessário fazer isso apenas uma vez. Consulte <a href="../build/downloading.html#using-authentication">Como usar a autenticação</a> para ver mais detalhes.</p>
<h3 id="start-a-repo-branch">Iniciar um branch do repo</h3>
<p>Para cada alteração que você pretende fazer, inicie um novo branch dentro do repositório git relevante:</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo start <var>NAME</var> .
</pre>
<p>É possível iniciar vários branches independentes ao mesmo tempo e no mesmo repositório. O branch NAME é local em relação ao seu espaço de trabalho e não será incluído no Gerrit ou na árvore de origem final.</p>
<h3 id="make-your-change">Fazer a alteração</h3>
<p>Depois de modificar os arquivos de origem (e validá-los) confirme as alterações no seu repositório local:</p>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">git add -A</code>
<code class="devsite-terminal">git commit -s</code>
</pre>
<p>Forneça uma descrição detalhada da alteração na sua mensagem de confirmação. Essa descrição será enviada para o repositório AOSP público, então siga nossas diretrizes para escrever as descrições da lista de alterações: </p>
<ul>

<li>
<p>Comece com um resumo de apenas uma linha (máximo de 50 caracteres), seguido por uma linha em branco.
Esse formato é usado pelo git e Gerrit para várias exibições.</p>
</li>

<li>
<p>A partir da terceira linha, insira uma descrição mais longa, que precisa ser limitada ao tamanho máximo de 72 caracteres. Essa descrição precisa se concentrar no problema que a alteração resolve e em como ela o resolve. A segunda parte é, de certa forma, opcional na implementação de novos recursos, embora seja desejável.</p>
</li>
<li>
<p>Inclua uma breve observação das suposições ou informações básicas que possam ser importantes quando outro colaborador trabalhar nesse recurso no ano seguinte.</p>
</li>
</ul>

<p>Este é um exemplo de uma mensagem de confirmação:</p>
<pre class="devsite-click-to-copy">short description on first line

more detailed description of your patch,
which is likely to take up multiple lines.
</pre>

<p>Uma código de alteração exclusivo, seu nome e e-mail fornecidos durante o <code>repo
init</code> serão automaticamente adicionados à sua mensagem de confirmação. </p>

<h3 id="upload-to-gerrit">Upload para Gerrit</h3>
<p>Depois de ter confirmado a alteração do seu histórico pessoal, envie-a para o Gerrit com</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo upload
</pre>
<p>Se você iniciou vários branches no mesmo repositório, terá que selecionar qual será enviado por upload.</p>
<p>Após um upload bem-sucedido, o repo fornecerá o URL de uma nova página no <a href="https://android-review.googlesource.com/">Gerrit</a>. Acesse esse link para ver seu patch no servidor de análise, adicionar comentários ou solicitar revisores específicos para seu patch.</p>
<h3 id="uploading-a-replacement-patch">Como fazer upload de um patch substituto</h3>
<p>Suponha que um revisor analisou seu patch e solicitou uma pequena modificação. Você pode alterar sua confirmação no git, o que resultará em um novo patch no Gerrit com o mesmo código de alteração que o original.</p>
<aside class="note"><b>Observação</b>: se você tiver feito outras confirmações desde o upload desse patch, será preciso mover manualmente seu HEAD do git.</aside>
<pre class="devsite-click-to-copy">
<code class="devsite-terminal">git add -A</code>
<code class="devsite-terminal">git commit --amend</code>
</pre>
<p>Quando você fizer upload do patch corrigido, ele substituirá o original no Gerrit e no histórico do git local.</p>

<h3 id="resolving-sync-conflicts">Como resolver conflitos de sincronização</h3>
<p>Se outros patches enviados para a árvore de origem entrarem em conflito com o seu, você precisará realocar seu patch sobre o novo HEAD do repositório de origem. A maneira mais fácil de fazer isso é executar</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo sync
</pre>
<p>Esse comando busca primeiro as atualizações do servidor de origem e, em seguida, tenta realocar seu HEAD automaticamente no novo HEAD remoto.</p>
<p>Se a realocação automática não for bem sucedida, será necessário realizar uma realocação manual.</p>
<pre class="devsite-terminal devsite-click-to-copy">
repo rebase
</pre>
<p>Usar o <code>git mergetool</code> pode ajudar você a lidar com o conflito de realocação. Depois de ter mesclado os arquivos conflitantes,</p>
<pre class="devsite-terminal devsite-click-to-copy">
git rebase --continue
</pre>
<p>Depois que a realocação automática ou manual for concluída, execute <code>repo
upload</code> para enviar o patch realocado.</p>

<h3 id="after-a-submission-is-approved">Após a aprovação de um envio</h3>
<p>Depois que um envio passa pelo processo de análise e verificação, o Gerrit mescla automaticamente a alteração no repositório público. Outros usuários poderão executar <code>repo sync</code> para transferir a atualização para o cliente local deles.</p>

<h2 id="upstream-projects">Projetos ascendentes</h2>
<p>O Android usa diversos outros projetos de código aberto, como o kernel do Linux e o WebKit, conforme descrito em <a href="/setup/code-lines.html">Linhas de código, branches e versões</a>. Para a maioria dos projetos em <code>external/</code>, as alterações precisam ser feitas de forma ascendente e, em seguida, os administradores do Android são informados da nova versão ascendente que contém essas alterações. Também pode ser útil fazer upload de patches que nos levem a rastrear uma nova versão ascendente, embora essas possam ser alterações difíceis de realizar se o projeto for amplamente usado no Android, como a maioria das grandes alterações mencionadas abaixo, onde a tendência é que o upgrade seja feito a cada versão.</p>
<p>Um caso especial interessante é o bionic. Grande parte do código dele vem do BSD, por isso, a menos que a alteração seja realizada no código que é novo no bionic, é preferível ter uma correção ascendente para então extrair um novo arquivo completo do BSD apropriado. Infelizmente, há uma grande mistura de BSDs diferentes no momento, mas esperamos resolver isso no futuro e chegar a um ponto em que seja possível rastrear projetos ascendentes de forma mais atenta.</p>
<h3 id="icu4c">ICU4C</h3>
<p>Todas as alterações do projeto ICU4C em <code>external/icu4c</code> precisam ser realizadas de forma ascendente em <a href="http://site.icu-project.org/">icu-project.org/</a>.
Consulte <a href="http://site.icu-project.org/bugs">Como enviar solicitações de recursos e bugs de ICU</a> (em inglês) para ver mais informações.</p>

<h3 id="llvmclangcompiler-rt">LLVM/Clang/Compiler-rt</h3>
<p>Todas as alterações em projetos relacionados a LLVM (<code>external/clang</code>, <code>external/compiler-rt</code>, <code>external/llvm</code>) precisam ser feitas de forma ascendente em <a href="http://llvm.org/">llvm.org/</a>.</p>

<h3 id="mksh">mksh</h3>
<p>Todas as alterações no projeto do MirBSD Korn Shell em <code>external/mksh</code> precisam ser feitas de forma ascendente enviando um e-mail para miros-mksh no domínio mirbsd.org (não é necessário ter uma assinatura para realizar o envio) ou no <a href="https://launchpad.net/mksh">Launchpad</a>.
</p>
<h3 id="openssl">OpenSSL</h3>
<p>Todas as alterações no projeto OpenSSL em <code>external/openssl</code> precisam ser realizadas de forma ascendente em <a href="http://www.openssl.org">openssl.org</a>.</p>
<h3 id="v8">V8</h3>
<p>Todas as alterações no projeto V8 em <code>external/v8</code> precisam ser enviadas de forma ascendente em <a href="https://code.google.com/p/v8">code.google.com/p/v8</a>. Consulte <a href="https://code.google.com/p/v8/wiki/Contributing">Como contribuir com o V8</a> (em inglês) para ver mais detalhes.</p>
<h3 id="webkit">WebKit</h3>
<p>Todas as alterações no projeto WebKit em <code>external/webkit</code> precisam ser realizadas de forma ascendente em <a href="http://www.webkit.org">webkit.org</a>. O processo começa com o registro de um bug do WebKit.
Esse bug usará o <code>Android</code> para os campos <code>Platform</code> e <code>OS</code> somente se o bug for específico do Android. É muito mais provável que os bugs recebam atenção dos revisores quando uma correção proposta for adicionada e os testes forem incluídos. Consulte <a href="http://webkit.org/coding/contributing.html">Como contribuir com código para o WebKit</a> (em inglês) para ver mais detalhes.</p>
<h3 id="zlib">zlib</h3>
<p>Todas as alterações do projeto zlib em <code>external/zlib</code> precisam ser feitas de forma ascendente em <a href="http://zlib.net">zlib.net</a>.</p>

</body></html>