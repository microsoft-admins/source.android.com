<html devsite><head>
    <title>O ciclo de um bug</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>O Android Open Source Project mantém um rastreador de problemas público em que você pode relatar bugs e solicitar recursos para a pilha do software Android principal.
Para mais detalhes sobre esse rastreador de problemas, consulte a página <a href="report-bugs.html">Como informar bugs</a>.
Reportar bugs é algo ótimo, mas o que acontece com um relatório de bug depois que você o registra? Esta página descreve o ciclo de um bug.</p>

<p class="note">O rastreador de problemas do Android Open Source Project (AOSP) destina-se apenas a bugs e solicitações de recursos relacionados à pilha de software Android principal e é uma ferramenta técnica para a comunidade do Open Source.</p>

<p>Este não é um fórum de suporte ao cliente. Para ver informações de suporte, consulte as Centrais de Ajuda do <a href="https://support.google.com/nexus">Nexus</a> e do <a href="https://support.google.com/pixelphone">Pixel</a>.
O suporte para outros dispositivos é fornecido pelos fabricantes deles ou pelas operadoras que os vendem.</p>

<p>O suporte para apps do Google ocorre por meio do <a href="http://support.google.com/">site de suporte do Google</a>. O suporte para apps de terceiros é fornecido pelo desenvolvedor de cada app, por exemplo, por meio dos dados de contato fornecidos no Google Play.</p>

<p>Este é o ciclo de um bug resumido:</p>
<ol>
<li>Um bug é registrado, apresentando o estado "Novo".</li>
<li>Um administrador do AOSP realiza as revisões e triagens dos bugs periodicamente. Eles são divididos em um de quatro <em>grupos</em>: Novo, Aberto, Sem ação ou Resolvido.</li>
<li>Cada grupo inclui vários estados que fornecem mais detalhes sobre o destino do problema.</li>
<li>Bugs marcados como "Resolvidos" serão posteriormente incluídos em uma versão futura do software Android.</li>
</ol>

<h2 id="bucket-details">Detalhes dos grupos</h2>
<p>
Usamos o campo <strong>Status</strong> no Rastreador de problemas para especificar o status de um problema no processo de resolução. Isso funciona da mesma forma com as definições especificadas na <a href="https://developers.google.com/issue-tracker/concepts/issues#status">documentação do Rastreador de problemas</a>.
</p>
<h3 id="new-issues">Problemas novos</h3>
<p>
Problemas novos incluem relatórios de bugs que ainda não estão recebendo nenhuma ação. Os dois estados são:
</p>
<ul>
 <li><strong>Novo</strong>: o relatório do bug ainda não passou por triagem (isto é, não foi analisado por um administrador do AOSP).</li>
 <li><strong>Novo + Hotlist:NeedsInfo</strong>: o relatório do bug ainda não tem informações suficientes para ser solucionado. A pessoa que relatou o bug precisa fornecer mais detalhes para que ele possa ser submetido à triagem. Se passar tempo suficiente e nenhuma nova informação for recebida, o bug poderá ser fechado por padrão, como um dos estados Sem ação.</li>
</ul>
<h3 id="open-issues">Problemas abertos</h3>
<p>
Este grupo contém bugs que precisam que algo seja feito, mas que ainda não foram resolvidos, aguardando uma alteração no código-fonte.
</p>
<ul>
 <li><strong>Atribuído</strong>: o relatório do bug foi reconhecido como um relatório devidamente detalhado de um problema legítimo, e o bug foi atribuído a um colaborador específico para avaliação e análise.</li>
 <li><strong>Aceito</strong>: o responsável reconheceu o problema e começou a trabalhar nele.</li>
</ul>
<p>
Normalmente, o estado inicial de um bug é <strong>Atribuído</strong> e permanece assim até que alguém tenha a intenção de resolvê-lo, quando passa para o estado <strong>Aceito</strong>. No entanto, observe que isso não é uma garantia, e não é incomum que os bugs passem de <strong>Atribuído</strong> para um dos estados Resolvidos.
</p>
<p>
Em geral, se um bug estiver em um desses estados Abertos, isso significa que a equipe do AOSP o reconheceu como um problema legítimo, e uma contribuição de alta qualidade para corrigir esse bug provavelmente será aceita. No entanto, é impossível garantir uma correção a tempo de uma versão específica.
</p>
<h3 id="no-action-issues">Problemas sem ação</h3>
<p>
Este grupo contém erros para os quais nenhuma ação é considerada necessária.
</p>
<ul>
 <li><strong>Sem correção (não reproduzível)</strong>: um colaborador do AOSP tentou reproduzir o comportamento descrito, mas não conseguiu. Isso às vezes significa que o bug é legítimo, mas simplesmente raro ou difícil de reproduzir, ou que não havia informações suficientes para corrigir o problema.</li>
 <li><strong>Sem correção (comportamento esperado)</strong>: um administrador do AOSP determinou que o comportamento descrito não é um bug, mas sim o comportamento esperado. Esse estado também é comumente chamado de <em>funcionando como deveria</em> (WAI, na sigla em inglês). Para solicitações de recursos, um administrador do AOSP determinou que a solicitação não será implementada no Android.</li>
 <li><strong>Sem correção (obsoleto)</strong>: o problema não é mais relevante devido a alterações no produto.</li>
 <li><strong>Sem correção (inviável)</strong>: as alterações necessárias para resolver o problema não são viáveis. Esse status também é usado para problemas informados que não podem ser tratados no AOSP, geralmente por serem relacionados a um dispositivo personalizado ou um app externo. Outra opção é que o informante tenha confundido o rastreador com um fórum de ajuda.</li>
 <li><strong>Cópia</strong>: já havia um relatório idêntico no rastreador de problemas. Todas as ações serão informadas no outro relatório.</li>
</ul>
<h3 id="resolved-issues">Problemas resolvidos</h3>
<p>
Este grupo contém erros em que alguma ação foi realizada e que agora são considerados como resolvidos.
</p>
<ul>
 <li><strong>Corrigido (verificado)</strong>: o bug foi corrigido e está incluído em uma versão formal. Quando esse estado é definido, tentamos também estabelecer uma propriedade indicando em qual versão ele foi corrigido.</li>
 <li><strong>Corrigido</strong>: o bug foi corrigido (ou o recurso foi implementado) em uma árvore de origem, mas ainda não foi incluído em uma versão formal.</li>
</ul>
<h2 id="other-stuff">Outras questões</h2>
<p>
Os estados e o ciclo acima são como geralmente tentamos rastrear o software.
No entanto, o Android contém muitos softwares e tem uma quantidade proporcionalmente elevada de bugs. Por isso, às vezes os bugs não passam por todos os estados em uma progressão formal. Tentamos manter o sistema atualizado, mas a tendência é fazer isso em "limpezas de bugs" periódicas, em que analisamos o banco de dados e fazemos atualizações.</p>

</body></html>